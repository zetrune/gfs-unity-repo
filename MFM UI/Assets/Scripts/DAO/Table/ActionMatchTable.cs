﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.DAO.Table
{
    public class ActionMatchTable
    {
        public int Id { get; set; }
        public int ActionId { get; set; }
        public int MatchId { get; set; }
    }
}
