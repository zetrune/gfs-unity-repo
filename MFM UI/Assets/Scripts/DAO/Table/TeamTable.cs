﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.DAO.Table
{
    public class TeamTable
    {
        public int Id { get; set; }
        public String Name { get; set; }
        public int  IsProtected { get; set; }
        public int GlobalStatsId { get; set; }
        public int LocalStatsId { get; set; }
        public int VisitorStatsId { get; set; }
        public int Status { get; set; }
        public int Type { get; set; }
        public int PrimaryR { get; set; }
        public int PrimaryG { get; set; }
        public int PrimaryB { get; set; }
        public int SecondaryR { get; set; }
        public int SecondaryG { get; set; }
        public int SecondaryB { get; set; }
    }
}
