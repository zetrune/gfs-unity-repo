﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.DAO.Table
{
    public class ChampionshipTable
    {
        public int Id { get; set;  }
        public String Name { get; set; }
        public String FlagName { get; set; }
    }
}
