﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.DAO.Table
{
    public class ActionTable
    {
        public int Id { get; set; }
        public int Side { get; set; }
        public int Time { get; set; }
        public int Bonus { get; set; }
        public int Defense1 { get; set; }
        public int Defense2 { get; set; }
        public int Attack1 { get; set; }
        public int Attack2 { get; set; }
    }
}
