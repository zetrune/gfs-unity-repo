﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.DAO.Table
{
    public class PlayerTable
    {
        public int Id { get; set; }
        public int GamePoint { get; set; }
        public int TeamId { get; set; }
        public int BoardTrust { get; set; }
    }
}
