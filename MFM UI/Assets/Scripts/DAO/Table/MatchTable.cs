﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.DAO.Table
{
    public class MatchTable
    {
        public int Id { get; set; }
        public int FirstHalfIsPlayed { get; set; }
        public int IsPlayed { get; set; }
        public int LocalScore { get; set; }
        public int VisitorScore { get; set; }
        public int Time { get; set; }
        public int AvailableBonus { get; set; }
        public int UsedBonus { get; set; }
        public int LocalTeamId { get; set; }
        public int VisitorTeamId { get; set; }
    }
}
