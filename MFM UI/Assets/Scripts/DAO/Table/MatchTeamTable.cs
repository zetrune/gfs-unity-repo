﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.DAO.Table
{
    public class MatchTeamTable
    {
        public int Id { get; set; }
        public int MatchId { get; set; }
        public int TeamId { get; set; }
    }
}
