﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.DAO.Table
{
    public class MatchJourneyTable
    {
        public int Id { get; set; }
        public int MatchId { get; set; }
        public int JourneyId { get; set; }
    }
}
