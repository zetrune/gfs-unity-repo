﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.DAO.Table
{
    public class TeamStatsTable
    {
        public int Id { get; set; }
        public int CurrentRank { get; set; }
        public int PreviousRank { get; set; }
        public int NbVictory { get; set; }
        public int NbDraw { get; set; }
        public int NbLose { get; set; }
        public int NbMatchs { get; set; }
        public int NbGoalScored { get; set; }
        public int NbGoalConceded { get; set; }
        public int GoalAverage { get; set; }
        public int NbPoints { get; set; }
    }
}
