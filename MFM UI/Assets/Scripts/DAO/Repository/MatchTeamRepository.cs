﻿using Assets.Scripts.DAO.Table;
using Assets.Scripts.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.DAO.Repository
{
    public interface IMatchTeamRepository : IRepository<MatchTeamTable>
    {

    }

    public class MatchTeamRepository : RepositoryBase<MatchTeamTable>, IMatchTeamRepository
    {
        public MatchTeamRepository(IDbManager parDbManager)
            : base(parDbManager)
        {

        }
    }
}
