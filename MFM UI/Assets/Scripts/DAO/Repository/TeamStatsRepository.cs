﻿using Assets.Scripts.DAO.Table;
using Assets.Scripts.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.DAO.Repository
{
    public interface ITeamStatsRepository : IRepository<TeamStatsTable>
    {

    }

    public class TeamStatsRepository : RepositoryBase<TeamStatsTable>, ITeamStatsRepository
    {
        public TeamStatsRepository(IDbManager parDbManager)
            : base(parDbManager)
        {

        }
    }
}
