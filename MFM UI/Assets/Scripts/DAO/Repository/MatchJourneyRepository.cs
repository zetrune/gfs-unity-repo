﻿using Assets.Scripts.DAO.Table;
using Assets.Scripts.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.DAO.Repository
{
    public interface IMatchJourneyRepository : IRepository<MatchJourneyTable>
    {

    }

    public class MatchJourneyRepository : RepositoryBase<MatchJourneyTable>, IMatchJourneyRepository
    {
        public MatchJourneyRepository(IDbManager parDbManager)
            : base(parDbManager)
        {

        }
    }
}
