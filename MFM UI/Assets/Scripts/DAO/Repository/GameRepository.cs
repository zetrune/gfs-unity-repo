﻿using Assets.Scripts.DAO.Table;
using Assets.Scripts.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.DAO.Repository
{
    public interface IGameRepository : IRepository<GameTable>
    {
        GameTable Get();
    }

    public class GameRepository : RepositoryBase<GameTable>, IGameRepository
    {

        public GameRepository(IDbManager parDbManager)
            : base(parDbManager)
        {
        }

        public GameTable Get()
        {
            var locType = typeof(GameTable);
            var locBuilder = new StringBuilder();

            locBuilder.Append("SELECT ");

            var locProps = locType.GetProperties();
            for (int i = 0; i < locProps.Length; i++)
            {
                locBuilder.Append(locProps[i].Name);

                if (i < locProps.Length - 1)
                {
                    locBuilder.Append(",");
                }
            }

            locBuilder.Append(" FROM ");
            locBuilder.Append(locType.Name);

            var locCmd = _dbManager.CreateDbCommand();
            locCmd.CommandText = locBuilder.ToString();
            var locReader = locCmd.ExecuteReader();

            List<GameTable> result = new List<GameTable>();

            while (locReader.Read())
            {

                var locEntity = new GameTable();
                for (int i = 0; i < locProps.Length; i++)
                {
                    locProps[i].SetValue(locEntity, Convert.ChangeType(locReader.GetValue(i), locProps[i].PropertyType), null);
                }

                result.Add(locEntity);
            }

            return result.FirstOrDefault();
        }
    }
}
