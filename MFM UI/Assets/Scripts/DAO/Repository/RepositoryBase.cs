﻿using Assets.Scripts.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Assets.Scripts.DAO.Repository
{
    public abstract class RepositoryBase<T> where T : class, new()
    {
        protected readonly IDbManager _dbManager;

        public RepositoryBase(IDbManager parDbManager)
        {
            _dbManager = parDbManager;
        }

        public virtual void Add(T parEntity)
        {
            var locType = parEntity.GetType();
            var locBuilder = new StringBuilder();

            locBuilder.Append("INSERT INTO ");
            locBuilder.Append(locType.Name);

            var locProps = locType.GetProperties();

            if (locProps.Length > 1)
            {
                locBuilder.Append(" (");

                for (int i = 0; i < locProps.Length; i++)
                {
                    if (locProps[i].Name == "Id")
                    {
                        continue;
                    }

                    locBuilder.Append(locProps[i].Name);

                    if (i < locProps.Length - 1)
                    {
                        locBuilder.Append(",");
                    }
                }

                locBuilder.Append(") VALUES (");

                for (int i = 0; i < locProps.Length; i++)
                {
                    if (locProps[i].Name == "Id")
                    {
                        continue;
                    }

                    locBuilder.Append("'");
                    locBuilder.Append(locProps[i].GetValue(parEntity, null));
                    locBuilder.Append("'");

                    if (i < locProps.Length - 1)
                    {
                        locBuilder.Append(",");
                    }
                }

                locBuilder.Append(")");
            }
            else
            {
                locBuilder.Append(" DEFAULT VALUES");
            }
            


            var locCmd = _dbManager.CreateDbCommand();
            locCmd.CommandText = locBuilder.ToString();
            locCmd.ExecuteReader();

            locCmd = _dbManager.CreateDbCommand();
            locCmd.CommandText = @"select last_insert_rowid()";
            long lastId = (long)locCmd.ExecuteScalar();

            var prop = locProps.Where(p => p.Name == "Id").FirstOrDefault();
            prop.SetValue(parEntity, Convert.ChangeType(lastId, prop.PropertyType), null);
        }

        public virtual void Delete(int parId)
        {
            var locType = typeof(T);
            var locBuilder = new StringBuilder();

            locBuilder.Append("DELETE FROM ");
            locBuilder.Append(locType.Name);
            
            locBuilder.Append(" WHERE Id = '");
            locBuilder.Append(parId);
            locBuilder.Append("'");

            var locCmd = _dbManager.CreateDbCommand();
            locCmd.CommandText = locBuilder.ToString();
            locCmd.ExecuteReader();
        }

        public virtual T GetById(int parId)
        {
            var result = GetByProperty("Id", parId.ToString());

            if (result != null && result.Count > 0)
            {
                return result.First();
            }

            return null;
        }

        public virtual List<T> GetByProperty(string parPropertyName, string value)
        {
            var locType = typeof(T);
            var locBuilder = new StringBuilder();

            locBuilder.Append("SELECT ");

            var locProps = locType.GetProperties();
            for (int i = 0; i < locProps.Length; i++)
            {
                locBuilder.Append(locProps[i].Name);

                if (i < locProps.Length - 1)
                {
                    locBuilder.Append(",");
                }
            }

            locBuilder.Append(" FROM ");
            locBuilder.Append(locType.Name);
            locBuilder.Append(" WHERE " + parPropertyName + " = '");
            locBuilder.Append(value);
            locBuilder.Append("'");

            var locCmd = _dbManager.CreateDbCommand();
            locCmd.CommandText = locBuilder.ToString();
            var locReader = locCmd.ExecuteReader();

            List<T> result = new List<T>();

            while (locReader.Read())
            {

                var locEntity = new T();
                for (int i = 0; i < locProps.Length; i++)
                {
                    locProps[i].SetValue(locEntity, Convert.ChangeType(locReader.GetValue(i), locProps[i].PropertyType), null);
                }

                result.Add(locEntity);
            }

            return result;
        }

        public virtual void Update(T parEntity)
        {
            var locType = parEntity.GetType();
            var locBuilder = new StringBuilder();

            locBuilder.Append("UPDATE ");
            locBuilder.Append(locType.Name);
            locBuilder.Append(" SET ");

            var locProps = locType.GetProperties();

            if (locProps.Length > 1)
            {
                for (int i = 0; i < locProps.Length; i++)
                {
                    if (locProps[i].Name == "Id")
                    {
                        continue;
                    }

                    locBuilder.Append(locProps[i].Name);
                    locBuilder.Append(" = '");
                    locBuilder.Append(locProps[i].GetValue(parEntity, null));
                    locBuilder.Append("'");

                    if (i < locProps.Length - 1)
                    {
                        locBuilder.Append(",");
                    }
                }

                locBuilder.Append(" WHERE Id = '");
                locBuilder.Append(locType.GetProperty("Id").GetValue(parEntity, null));
                locBuilder.Append("'");

                var locCmd = _dbManager.CreateDbCommand();
                locCmd.CommandText = locBuilder.ToString();
                locCmd.ExecuteReader();
            }
        }
    }
}
