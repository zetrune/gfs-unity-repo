﻿using Assets.Scripts.DAO.Table;
using System;
using System.Collections.Generic;

namespace Assets.Scripts.DAO.Repository
{
    public interface IRepository<T> where T : class
    {
        void Add(T parEntity);
        void Delete(int parId);
        T GetById(int parId);
        List<T> GetByProperty(string parPropertyName, string value);
        void Update(T parEntity);
    }
}