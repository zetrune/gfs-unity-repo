﻿using Assets.Scripts.DAO.Table;
using Assets.Scripts.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.DAO.Repository
{
    public interface ITeamRepository : IRepository<TeamTable>
    {

    }

    public class TeamRepository : RepositoryBase<TeamTable>, ITeamRepository
    {

        public TeamRepository(IDbManager parDbManager)
            : base(parDbManager)
        {
        }
    }
}
