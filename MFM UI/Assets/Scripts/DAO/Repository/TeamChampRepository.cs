﻿using Assets.Scripts.DAO.Table;
using Assets.Scripts.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.DAO.Repository
{
    public interface ITeamChampRepository : IRepository<TeamChampTable>
    {

    }

    public class TeamChampRepository : RepositoryBase<TeamChampTable>, ITeamChampRepository
    {
        public TeamChampRepository(IDbManager parDbManager)
            : base(parDbManager)
        {

        }
    }
}
