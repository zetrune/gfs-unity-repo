﻿using Assets.Scripts.DAO.Table;
using Assets.Scripts.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.DAO.Repository
{
    public interface IJourneyRepository : IRepository<JourneyTable>
    {

    }

    public class JourneyRepository : RepositoryBase<JourneyTable>, IJourneyRepository
    {

        public JourneyRepository(IDbManager parDbManager)
            : base(parDbManager)
        {
        }
    }
}
