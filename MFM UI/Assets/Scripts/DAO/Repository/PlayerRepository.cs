﻿using Assets.Scripts.DAO.Table;
using Assets.Scripts.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.DAO.Repository
{
    public interface IPlayerRepository : IRepository<PlayerTable>
    {

    }

    public class PlayerRepository : RepositoryBase<PlayerTable>, IPlayerRepository
    {

        public PlayerRepository(IDbManager parDbManager)
            : base(parDbManager)
        {
        }
    }
}
