﻿using Assets.Scripts.DAO.Table;
using Assets.Scripts.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.DAO.Repository
{
    public interface IActionRepository : IRepository<ActionTable>
    {

    }

    public class ActionRepository : RepositoryBase<ActionTable>, IActionRepository
    {

        public ActionRepository(IDbManager parDbManager)
            : base (parDbManager)
        {
        }
    }
}
