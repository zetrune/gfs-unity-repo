﻿using Assets.Scripts.DAO.Table;
using Assets.Scripts.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.DAO.Repository
{
    public interface IChampionshipRepository : IRepository<ChampionshipTable>
    {

    }

    public class ChampionshipRepository : RepositoryBase<ChampionshipTable>, IChampionshipRepository
    {

        public ChampionshipRepository(IDbManager parDbManager)
            : base(parDbManager)
        {
        }
    }
}
