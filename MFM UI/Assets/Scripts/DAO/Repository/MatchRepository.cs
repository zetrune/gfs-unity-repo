﻿using Assets.Scripts.DAO.Table;
using Assets.Scripts.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.DAO.Repository
{
    public interface IMatchRepository : IRepository<MatchTable>
    {

    }

    public class MatchRepository : RepositoryBase<MatchTable>, IMatchRepository
    {

        public MatchRepository(IDbManager parDbManager)
            : base(parDbManager)
        {
        }
    }
}
