﻿using Assets.Scripts.DAO.Table;
using Assets.Scripts.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.DAO.Repository
{
    public interface IPlayerMatchRepository : IRepository<PlayerMatchTable>
    {

    }

    public class PlayerMatchRepository : RepositoryBase<PlayerMatchTable>, IPlayerMatchRepository
    {
        public PlayerMatchRepository(IDbManager parDbManager)
            : base(parDbManager)
        {

        }
    }
}
