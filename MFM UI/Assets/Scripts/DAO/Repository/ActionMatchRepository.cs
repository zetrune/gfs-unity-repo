﻿using Assets.Scripts.DAO.Table;
using Assets.Scripts.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.DAO.Repository
{

    public interface IActionMatchRepository : IRepository<ActionMatchTable>
    {

    }

    public class ActionMatchRepository : RepositoryBase<ActionMatchTable>, IActionMatchRepository
    {
        public ActionMatchRepository(IDbManager parDbManager)
            : base (parDbManager)
        {

        }
    }
}
