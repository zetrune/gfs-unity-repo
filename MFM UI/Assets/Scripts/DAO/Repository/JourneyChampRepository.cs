﻿using Assets.Scripts.DAO.Table;
using Assets.Scripts.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.DAO.Repository
{
    public interface IJourneyChampRepository : IRepository<JourneyChampTable>
    {

    }

    public class JourneyChampRepository : RepositoryBase<JourneyChampTable>, IJourneyChampRepository
    {
        public JourneyChampRepository(IDbManager parDbManager)
            : base(parDbManager)
        {

        }
    }
}
