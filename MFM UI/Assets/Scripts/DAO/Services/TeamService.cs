﻿using Assets.Scripts.DAO.Repository;
using Assets.Scripts.DAO.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.DAO.Services
{
    public interface ITeamService
    {
        void AddTeam(Team parTeam);
        void UpdateTeam(Team parTeam);
        Team GetTeam(int parTeamId);
        void RemoveTeam(int parTeamId);
    }

    public class TeamService : ITeamService
    {
        private readonly ITeamRepository _teamRepo;
        private readonly ITeamChampRepository _teamChampRepo;
        private readonly ITeamStatsRepository _teamStatsRepo;

        public TeamService(ITeamRepository parTeamRepo, ITeamChampRepository parTeamChampRepo, ITeamStatsRepository parTeamStatsRepo)
        {
            _teamRepo = parTeamRepo;
            _teamChampRepo = parTeamChampRepo;
            _teamStatsRepo = parTeamStatsRepo;
        }

        public void AddTeam(Team parTeam)
        {
            if (parTeam == null || parTeam._id != 0)
            {
                return;
            }

            parTeam._teamGlobalStats._id = AddTeamStatsModule(parTeam._teamGlobalStats);
            parTeam._teamLocalStats._id = AddTeamStatsModule(parTeam._teamLocalStats);
            parTeam._teamVisitorStats._id = AddTeamStatsModule(parTeam._teamVisitorStats);

            TeamTable locTeamTable = MapTeamToTable(parTeam);

            _teamRepo.Add(locTeamTable);
            parTeam._id = locTeamTable.Id;
        }

        public void UpdateTeam(Team parTeam)
        {
            if (parTeam == null || parTeam._id == 0)
            {
                return;
            }

            UpdateTeamStatsModule(parTeam._teamGlobalStats);
            UpdateTeamStatsModule(parTeam._teamLocalStats);
            UpdateTeamStatsModule(parTeam._teamVisitorStats);

            TeamTable locTeamTable = MapTeamToTable(parTeam);

            _teamRepo.Update(locTeamTable);
        }

        private int AddTeamStatsModule(TeamStatsModule parTeamStatsModule)
        {
            TeamStatsTable locTeamStatsTable = MapTeamStatsModuleToTable(parTeamStatsModule);

            _teamStatsRepo.Add(locTeamStatsTable);

            return locTeamStatsTable.Id;
        }

        private TeamStatsTable MapTeamStatsModuleToTable(TeamStatsModule parTeamStatsModule)
        {
            TeamStatsTable locTeamStatsTable = new TeamStatsTable();

            locTeamStatsTable.Id = parTeamStatsModule._id;
            locTeamStatsTable.CurrentRank = parTeamStatsModule._currentRank;
            locTeamStatsTable.PreviousRank = parTeamStatsModule._previousRank;
            locTeamStatsTable.NbVictory = parTeamStatsModule._nbVictory;
            locTeamStatsTable.NbDraw = parTeamStatsModule._nbDraw;
            locTeamStatsTable.NbLose = parTeamStatsModule._nbLose;
            locTeamStatsTable.NbMatchs = parTeamStatsModule._nbMatchs;
            locTeamStatsTable.NbGoalScored = parTeamStatsModule._nbGoalScored;
            locTeamStatsTable.NbGoalConceded = parTeamStatsModule._nbGoalConceded;
            locTeamStatsTable.GoalAverage = parTeamStatsModule._goalAverage;
            locTeamStatsTable.NbPoints = parTeamStatsModule._nbPoints;

            return locTeamStatsTable;
        }

        public Team GetTeam(int parTeamId)
        {
            var locTeamTable = _teamRepo.GetById(parTeamId);

            Team result = null;

            if (locTeamTable != null)
            {
                result = MapTableToTeam(locTeamTable);

                result._teamGlobalStats = GetTeamStatsModule(locTeamTable.GlobalStatsId);
                result._teamLocalStats = GetTeamStatsModule(locTeamTable.LocalStatsId);
                result._teamVisitorStats = GetTeamStatsModule(locTeamTable.VisitorStatsId);
            }

            return result;
        }

        private TeamStatsModule GetTeamStatsModule(int parTeamStatsId)
        {
            var locTeamStatsTable = _teamStatsRepo.GetById(parTeamStatsId);

            TeamStatsModule locTeamStatsModule = null;

            if (locTeamStatsTable != null)
            {
                locTeamStatsModule = MapTeamStatsTableToModule(locTeamStatsTable);
            }

            return locTeamStatsModule;
        }

        private TeamStatsModule MapTeamStatsTableToModule(TeamStatsTable parTeamStatsTable)
        {
            TeamStatsModule locTeamStatsModule = new TeamStatsModule();

            locTeamStatsModule._id = parTeamStatsTable.Id;
            locTeamStatsModule._currentRank = parTeamStatsTable.CurrentRank;
            locTeamStatsModule._previousRank = parTeamStatsTable.PreviousRank;
            locTeamStatsModule._nbVictory = parTeamStatsTable.NbVictory;
            locTeamStatsModule._nbDraw = parTeamStatsTable.NbDraw;
            locTeamStatsModule._nbLose = parTeamStatsTable.NbLose;
            locTeamStatsModule._nbMatchs = parTeamStatsTable.NbMatchs;
            locTeamStatsModule._nbGoalScored = parTeamStatsTable.NbGoalScored;
            locTeamStatsModule._nbGoalConceded = parTeamStatsTable.NbGoalConceded;
            locTeamStatsModule._goalAverage = parTeamStatsTable.GoalAverage;
            locTeamStatsModule._nbPoints = parTeamStatsTable.NbPoints;

            locTeamStatsModule.updateRateStats();

            return locTeamStatsModule;
        }

        public void RemoveTeam(int parTeamId)
        {
            var locTeamTable = _teamRepo.GetById(parTeamId);

            if (locTeamTable == null)
            {
                return;
            }

            _teamStatsRepo.Delete(locTeamTable.GlobalStatsId);
            _teamStatsRepo.Delete(locTeamTable.LocalStatsId);
            _teamStatsRepo.Delete(locTeamTable.VisitorStatsId);

            _teamRepo.Delete(parTeamId);

            var locTabTeamChamp = _teamChampRepo.GetByProperty("TeamId", parTeamId.ToString());

            foreach (var locTeamChamp in locTabTeamChamp)
            {
                _teamChampRepo.Delete(locTeamChamp.Id);
            }

        }

        private void UpdateTeamStatsModule(TeamStatsModule parTeamStatsModule)
        {
            TeamStatsTable locTeamStatsTable = MapTeamStatsModuleToTable(parTeamStatsModule);

            _teamStatsRepo.Update(locTeamStatsTable);
        }

        private static TeamTable MapTeamToTable(Team parTeam)
        {
            return new TeamTable
            {
                Id = parTeam._id,
                Name = parTeam._teamName,
                IsProtected = parTeam._isProtected ? 1 : 0,
                GlobalStatsId = parTeam._teamGlobalStats._id,
                LocalStatsId = parTeam._teamLocalStats._id,
                VisitorStatsId = parTeam._teamVisitorStats._id,
                Status = (int)parTeam._teamStatus._status,
                Type = (int)parTeam._teamSkills._teamType,
                PrimaryR = parTeam._teamPrimaryColor.r,
                PrimaryG = parTeam._teamPrimaryColor.g,
                PrimaryB = parTeam._teamPrimaryColor.b,
                SecondaryR = parTeam._teamSecondaryColor.r,
                SecondaryG = parTeam._teamSecondaryColor.g,
                SecondaryB = parTeam._teamSecondaryColor.b,
            };
        }

        private Team MapTableToTeam(TeamTable parTeamTable)
        {
            var locTeam = new Team();

            locTeam._id = parTeamTable.Id;
            locTeam._teamName = parTeamTable.Name;
            locTeam._isProtected = parTeamTable.IsProtected == 1 ? true : false;
            locTeam._teamStatus = new TeamStatusModule((TeamStatus)parTeamTable.Status);
            locTeam._teamSkills = new TeamSkillsModule((TeamType)parTeamTable.Type);
            locTeam._teamPrimaryColor = new Color32((byte)parTeamTable.PrimaryR, (byte)parTeamTable.PrimaryG, (byte)parTeamTable.PrimaryB, 255);
            locTeam._teamSecondaryColor = new Color32((byte)parTeamTable.SecondaryR, (byte)parTeamTable.SecondaryG, (byte)parTeamTable.SecondaryB, 255);
            return locTeam;
        }
    }
}
