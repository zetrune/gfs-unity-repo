﻿using Assets.Scripts.DAO.Repository;
using Assets.Scripts.DAO.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.DAO.Services
{
    public interface IMatchService
    {
        void AddMatch(Match parMatch);
        void UpdateMatch(Match parMatch);
        Match GetMatch(int parMatchId);
        void RemoveMatch(int parMatchId);
    }

    public class MatchService : IMatchService
    {
        private readonly IMatchRepository _matchRepo;
        private readonly IActionRepository _actionRepo;
        private readonly IActionMatchRepository _actionMatchRepo;     

        public MatchService(IMatchRepository parMatchRepo, IActionRepository parActionRepo, IActionMatchRepository parActionMatchRepo)
        {
            _matchRepo = parMatchRepo;
            _actionRepo = parActionRepo;
            _actionMatchRepo = parActionMatchRepo;            
        }

        public void AddMatch(Match parMatch)
        {
            if (parMatch == null || parMatch._id != 0)
            {
                return;
            }

            MatchTable locMatchTable = MapMatchToTable(parMatch);

            _matchRepo.Add(locMatchTable);
            parMatch._id = locMatchTable.Id;
            AddMatchActions(parMatch);
        }

        public void UpdateMatch(Match parMatch)
        {
            if (parMatch == null || parMatch._id == 0)
            {
                return;
            }

            MatchTable locMatchTable = MapMatchToTable(parMatch);

            _matchRepo.Update(locMatchTable);
            AddMatchActions(parMatch);
        }

        private static MatchTable MapMatchToTable(Match parMatch)
        {
            MatchTable locMatchTable = new MatchTable();

            locMatchTable.Id = parMatch._id;
            locMatchTable.FirstHalfIsPlayed = parMatch._firstHalfIsPlayed ? 1 : 0;
            locMatchTable.IsPlayed = parMatch._isPlayed ? 1 : 0;
            locMatchTable.LocalScore = parMatch._localScore;
            locMatchTable.VisitorScore = parMatch._visitorScore;
            locMatchTable.Time = parMatch._time;
            locMatchTable.AvailableBonus = parMatch._availableBonus;
            locMatchTable.UsedBonus = parMatch._usedBonus;
            locMatchTable.LocalTeamId = parMatch._localTeam;
            locMatchTable.VisitorTeamId = parMatch._visitorTeam;
            return locMatchTable;
        }

        private void AddMatchActions(Match parMatch)
        {
            foreach (var locAction in parMatch._actions)
            {
                var locActionTable = AddAction(locAction);
                if (locActionTable != null)
                {
                    locAction.Id = locActionTable.Id;
                    AddActionMatch(parMatch._id, locAction.Id);
                }
            }
        }

        private void AddActionMatch(int parMatchId, int parActionId)
        {
            var locTabActionMatch = _actionMatchRepo.GetByProperty("MatchId", parMatchId.ToString());

            bool toBeAdded = true;
            foreach (var locActionMatch in locTabActionMatch)
            {
                if (locActionMatch.ActionId == parActionId)
                {
                    toBeAdded = false;
                    break;
                }
            }

            if (toBeAdded)
            {
                var locActionMatch = new ActionMatchTable();

                locActionMatch.MatchId = parMatchId;
                locActionMatch.ActionId = parActionId;

                _actionMatchRepo.Add(locActionMatch);
            }
        }

        private ActionTable AddAction(Action parAction)
        {
            if (parAction == null || parAction.Id != 0)
            {
                return null;
            }

            ActionTable locActionTable = MapActionToTable(parAction);

            _actionRepo.Add(locActionTable);

            return locActionTable;
        }

        private static ActionTable MapActionToTable(Action parAction)
        {
            return new ActionTable
            {
                Id = parAction.Id,
                Side = (int)parAction.Side,
                Time = parAction.Time,
                Bonus = (int)parAction.Bonus,
                Defense1 = (int)parAction.Defense1,
                Defense2 = (int)parAction.Defense2,
                Attack1 = (int)parAction.Attack1,
                Attack2 = (int)parAction.Attack2
            };
        }

        public Match GetMatch(int parMatchId)
        {
            var locMatchTable = _matchRepo.GetById(parMatchId);

            Match result = null;

            if (locMatchTable != null)
            {
                result = MapTableToMatch(locMatchTable);

                result._actions = GetMatchAction(result);
            }

            return result;
        }

        private List<Action> GetMatchAction(Match parMatch)
        {
            var locTabActionMatch = _actionMatchRepo.GetByProperty("MatchId", parMatch._id.ToString());

            var result = new List<Action>();

            foreach (var locActionMatch in locTabActionMatch)
            {

                var locActionTable = _actionRepo.GetById(locActionMatch.ActionId);

                if (locActionTable != null)
                {
                    result.Add(MapTableToAction(locActionTable));
                }
                
            }

            return result;
        }

        public void RemoveMatch(int parMatchId)
        {

            _matchRepo.Delete(parMatchId);

            var locTabActionMatch = _actionMatchRepo.GetByProperty("MatchId", parMatchId.ToString());
        
            foreach (var locActionMatch in locTabActionMatch)
            {
                _actionRepo.Delete(locActionMatch.ActionId);
                _actionMatchRepo.Delete(locActionMatch.Id);
            }
        }

        private Action MapTableToAction(ActionTable parActionTable)
        {
            var locAction = new Action();

            locAction.Id = parActionTable.Id;
            locAction.Side = (ActionSide)parActionTable.Side;
            locAction.Time = parActionTable.Time;
            locAction.Bonus = (ActionBonus)parActionTable.Bonus;
            locAction.Defense1 = (ActionState)parActionTable.Defense1;
            locAction.Defense2 = (ActionState)parActionTable.Defense2;
            locAction.Attack1 = (ActionState)parActionTable.Attack1;
            locAction.Attack2 = (ActionState)parActionTable.Attack2;

            return locAction;
        }

        private Match MapTableToMatch(MatchTable parMatchTable)
        {
            Match locMatch = new Match(parMatchTable.LocalTeamId, parMatchTable.VisitorTeamId);

            locMatch._id = parMatchTable.Id;
            locMatch._firstHalfIsPlayed = parMatchTable.FirstHalfIsPlayed == 1 ? true : false;
            locMatch._isPlayed = parMatchTable.IsPlayed == 1 ? true : false;
            locMatch._localScore = parMatchTable.LocalScore;
            locMatch._visitorScore = parMatchTable.VisitorScore;
            locMatch._time = parMatchTable.Time;
            locMatch._availableBonus = parMatchTable.AvailableBonus;
            locMatch._usedBonus = parMatchTable.UsedBonus;         

            return locMatch;
        }
    }
}
