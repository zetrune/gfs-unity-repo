﻿using Assets.Scripts.DAO.Repository;
using Assets.Scripts.DAO.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.DAO.Services
{

    public interface IPlayerService
    {
        void AddPlayer(Player parPlayer);
        void UpdatePlayer(Player parPlayer);
        Player GetPlayer(int parPlayerId);
        void RemovePlayer(int parPlayerId);
    }

    public class PlayerService : IPlayerService
    {
        private readonly IPlayerRepository _playerRepo;
        private readonly IPlayerMatchRepository _playerMatchRepo;
        private readonly IMatchService _matchService;

        public PlayerService(IPlayerRepository parPlayerRepo, IPlayerMatchRepository parPlayerMatchRepo, IMatchService parMatchService) {
            _playerRepo = parPlayerRepo;
            _playerMatchRepo = parPlayerMatchRepo;
            _matchService = parMatchService;
        }    

        public void AddPlayer(Player parPlayer)
        {
            if (parPlayer == null || parPlayer._id != 0)
            {
                return;
            }

            PlayerTable locPlayerTable = MapPlayerToTable(parPlayer);

            _playerRepo.Add(locPlayerTable);
            parPlayer._id = locPlayerTable.Id;

            AddPlayerMatchs(parPlayer);
        }

        public void UpdatePlayer(Player parPlayer)
        {
            if (parPlayer == null || parPlayer._id == 0)
            {
                return;
            }

            PlayerTable locPlayerTable = MapPlayerToTable(parPlayer);

            _playerRepo.Update(locPlayerTable);

            UpdatePlayerHistory(parPlayer);
        }

        private void AddPlayerMatchs(Player parPlayer)
        {
            foreach (var locMatch in parPlayer._historyModule._matchHistory)
            {
                PlayerMatchTable locPlayerMatchTable = new PlayerMatchTable();
                locPlayerMatchTable.MatchId = locMatch._id;
                locPlayerMatchTable.PlayerId = parPlayer._id;

                _playerMatchRepo.Add(locPlayerMatchTable);
            }
        }

        private PlayerTable MapPlayerToTable(Player parPlayer)
        {
            PlayerTable locPlayerTable = new PlayerTable();
            locPlayerTable.Id = parPlayer._id;
            locPlayerTable.TeamId = parPlayer._team;
            locPlayerTable.GamePoint = parPlayer._gamePoints;
            locPlayerTable.BoardTrust = parPlayer._boardTrust;
            return locPlayerTable;
        }

        public Player GetPlayer(int parPlayerId)
        {
            var locPlayerTable = _playerRepo.GetById(parPlayerId);

            Player player = null;

            if (locPlayerTable != null)
            {
                player = MapTableToPlayer(locPlayerTable);
                player._historyModule = new PlayerHistoryModule();
                player._historyModule._matchHistory = GetPlayerMatchs(parPlayerId);
                player._team = locPlayerTable.TeamId;
            }

            return player;
        }

        private List<Match> GetPlayerMatchs(int parPlayerId)
        {
            var locTabPlayerMatchs = _playerMatchRepo.GetByProperty("PlayerId", parPlayerId.ToString());

            var result = new List<Match>();

            foreach (var locPlayerMatch in locTabPlayerMatchs)
            {
                var locMatch = _matchService.GetMatch(locPlayerMatch.MatchId);

                if (locMatch != null)
                {
                    result.Add(locMatch);
                }
            }

            return result;
        }

        private Player MapTableToPlayer(PlayerTable locPlayerTable)
        {
            Player locPlayer = new Player();
            locPlayer._id = locPlayerTable.Id;
            locPlayer._gamePoints = locPlayerTable.GamePoint;
            locPlayer._boardTrust = locPlayerTable.BoardTrust;
            return locPlayer;
        }

        public void RemovePlayer(int parPlayerId)
        {
            _playerRepo.Delete(parPlayerId);

            
        }

        private void UpdatePlayerHistory(Player parPlayer)
        {
            var locTabMatch = parPlayer._historyModule._matchHistory;

            foreach (var locMatch in locTabMatch)
            {
                var locMatchTable = _playerMatchRepo.GetByProperty("MatchId", locMatch._id.ToString()).FirstOrDefault();

                if (locMatchTable == null)
                {
                    PlayerMatchTable locPlayerMatchTable = new PlayerMatchTable();
                    locPlayerMatchTable.MatchId = locMatch._id;
                    locPlayerMatchTable.PlayerId = parPlayer._id;

                    _playerMatchRepo.Add(locPlayerMatchTable);
                }
            }
        }
    }
}
