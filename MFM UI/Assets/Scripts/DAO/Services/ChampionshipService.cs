﻿using Assets.Scripts.DAO.Repository;
using Assets.Scripts.DAO.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.DAO.Services
{

    public interface IChampionshipService
    {
        void AddChampionship(Championship parChampionship);
        void UpdateChampionship(Championship parChampionship);
        Championship GetChampionship(int parChampionshipId);
        void RemoveChampionship(int parChampionshipId);

        void AddTeamToChampionship(Championship parChampionship, Team parTeam);
        void RemoveTeamFromChampionship(Championship parChampionship, Team parTeam);
    }

    public class ChampionshipService : IChampionshipService
    {
        private readonly IChampionshipRepository _champRepo;
        private readonly IJourneyChampRepository _journeyChampRepo;
        private readonly IJourneyService _journeyService;
        private readonly ITeamChampRepository _teamChampRepo;
        private readonly ITeamService _teamService;

        public ChampionshipService(IChampionshipRepository parChampRepo, IJourneyService parJourneyService, IJourneyChampRepository parJourneyChampRepo, ITeamChampRepository parTeamChampRepo, ITeamService parTeamService)
        {
            _champRepo = parChampRepo;
            _journeyService = parJourneyService;
            _journeyChampRepo = parJourneyChampRepo;
            _teamChampRepo = parTeamChampRepo;
            _teamService = parTeamService;
        }

        public void AddChampionship(Championship parChampionship)
        {
            if (parChampionship == null || parChampionship._id != 0)
            {
                return;
            }

            ChampionshipTable locChampTable = MapChampToTable(parChampionship);

            _champRepo.Add(locChampTable);
            parChampionship._id = locChampTable.Id;

            foreach (var locTeam in parChampionship._tabFirstLeagueTeams)
            {
                _teamService.AddTeam(locTeam);
                AddTeamChamp(parChampionship._id, locTeam._id);
            }

            AddChampJourneys(parChampionship);
        }

        public void UpdateChampionship(Championship parChampionship)
        {
            if (parChampionship == null || parChampionship._id == 0)
            {
                return;
            }

            ChampionshipTable locChampTable = MapChampToTable(parChampionship);

            _champRepo.Update(locChampTable);

            foreach (var locTeam in parChampionship._tabFirstLeagueTeams)
            {
                _teamService.UpdateTeam(locTeam);
            }

            _journeyService.UpdateJourney(parChampionship._tabJourneysFirstLeague[GameManager._instance.Game.JourneyIndex]);
        }

        private void AddTeamChamp(int parChampId, int parTeamId)
        {
            var locTabTeamChamp = _teamChampRepo.GetByProperty("ChampionshipId", parChampId.ToString());

            bool toBeAdded = true;
            foreach (var locTeamChamp in locTabTeamChamp)
            {
                if (locTeamChamp.TeamId == parTeamId)
                {
                    toBeAdded = false;
                    break;
                }
            }

            if (toBeAdded)
            {
                var locTeamChamp = new TeamChampTable();

                locTeamChamp.ChampionshipId = parChampId;
                locTeamChamp.TeamId = parTeamId;

                _teamChampRepo.Add(locTeamChamp);
            }
        }

        private void AddChampJourneys(Championship parChampionship)
        {
            foreach (var locJourney in parChampionship._tabJourneysFirstLeague)
            {
                JourneyTable locJourneyTable = _journeyService.AddJourney(locJourney);
                if (locJourneyTable != null)
                {
                    locJourney._id = locJourneyTable.Id;
                    AddJourneyChamp(parChampionship._id, locJourney._id);
                }
            }
        }

        private void AddJourneyChamp(int parChampId, int parJourneyId)
        {
            var locTabJourneyChamp = _journeyChampRepo.GetByProperty("ChampionshipId", parChampId.ToString());

            bool toBeAdded = true;
            foreach (var locJourneyChamp in locTabJourneyChamp)
            {
                if (locJourneyChamp.JourneyId == parJourneyId)
                {
                    toBeAdded = false;
                    break;
                }
            }

            if (toBeAdded)
            {
                var locJourneyChamp = new JourneyChampTable();

                locJourneyChamp.ChampionshipId = parChampId;
                locJourneyChamp.JourneyId = parJourneyId;

                _journeyChampRepo.Add(locJourneyChamp);
            }
        }


        private ChampionshipTable MapChampToTable(Championship parChampionship)
        {
            ChampionshipTable locChampionshipTable = new ChampionshipTable();

            locChampionshipTable.Id = parChampionship._id;
            locChampionshipTable.Name = parChampionship._name;
            locChampionshipTable.FlagName = parChampionship._flagName;

            return locChampionshipTable;
        }

        public Championship GetChampionship(int parChampionshipId)
        {
            var locChampTable = _champRepo.GetById(parChampionshipId);

            Championship championship = null;

            if (locChampTable != null)
            {
                championship = MapTableToChamp(locChampTable, GetChampTeam(parChampionshipId));
                championship._tabJourneysFirstLeague = GetChampJourney(championship);
            }

            return championship;
        }

        private List<Team> GetChampTeam(int parChampionshipId)
        {
            var locTabTeam = new List<Team>();

            var locTabChampTeam = _teamChampRepo.GetByProperty("ChampionshipId", parChampionshipId.ToString());

            foreach (var locChampTeam in locTabChampTeam)
            {
                var locTeam = _teamService.GetTeam(locChampTeam.TeamId);

                if (locTeam != null)
                {
                    locTabTeam.Add(locTeam);
                }
            }

            return locTabTeam;
        }

        private Championship MapTableToChamp(ChampionshipTable parChampTable, List<Team> parTabTeam)
        {
            Championship locChampionship = new Championship(parChampTable.Name, parChampTable.FlagName, parTabTeam);
            locChampionship._id = parChampTable.Id;

            return locChampionship;
        }

        private List<Journey> GetChampJourney(Championship parChampionship)
        {
            var locTabJourneyChamp = _journeyChampRepo.GetByProperty("ChampionshipId", parChampionship._id.ToString());

            var result = new List<Journey>();

            foreach (var locJourneyChamp in locTabJourneyChamp)
            {
                var locJourney = _journeyService.GetJourney(locJourneyChamp.JourneyId);

                if (locJourney != null)
                {
                    result.Add(locJourney);
                }
            }

            return result;
        }

        public void RemoveChampionship(int parChampionshipId)
        {
            _champRepo.Delete(parChampionshipId);

            var locTabJourneyChamp = _journeyChampRepo.GetByProperty("ChampionshipId", parChampionshipId.ToString());

            foreach (var locJourneyChamp in locTabJourneyChamp)
            {
                _journeyService.RemoveJourney(locJourneyChamp.JourneyId);
                _journeyChampRepo.Delete(locJourneyChamp.Id);
            }

            var locTabTeamChamp = _teamChampRepo.GetByProperty("ChampionshipId", parChampionshipId.ToString());

            foreach (var locTeamChamp in locTabTeamChamp)
            {
                _teamChampRepo.Delete(locTeamChamp.Id);
            }
        }

        public void AddTeamToChampionship(Championship parChampionship, Team parTeam)
        {
            var locTabTeamChamp = _teamChampRepo.GetByProperty("ChampionshipId", parChampionship._id.ToString());

            bool toBeAdded = true;
            foreach (var locTeamChamp in locTabTeamChamp)
            {
                if (locTeamChamp.TeamId == parTeam._id)
                {
                    toBeAdded = false;
                    break;
                }
            }

            if (toBeAdded)
            {
                var locTeamChamp = new TeamChampTable();

                locTeamChamp.ChampionshipId = parChampionship._id;
                locTeamChamp.TeamId = parTeam._id;

                _teamChampRepo.Add(locTeamChamp);
            }
        }

        public void RemoveTeamFromChampionship(Championship parChampionship, Team parTeam)
        {
            var locTabTeamChamp = _teamChampRepo.GetByProperty("ChampionshipId", parChampionship._id.ToString());

            foreach (var locTeamChamp in locTabTeamChamp)
            {
                if (locTeamChamp.TeamId ==  parTeam._id)
                {
                    _teamChampRepo.Delete(locTeamChamp.Id);
                }
            }
        }
    }
}
