﻿using Assets.Scripts.DAO.Repository;
using Assets.Scripts.DAO.Table;
using Assets.Scripts.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.DAO.Services
{

    public interface IGameService
    {
        void AddGame(Game parGame);
        void UpdateGame(Game parGame);
        Game GetGame();
    }

    public class GameService : IGameService
    {
        private readonly IGameRepository _gameRepo;
        private readonly IPlayerService _playerService;
        private readonly IChampionshipService _champService;

        public GameService(IGameRepository parGameRepo, IPlayerService parPlayerService, IChampionshipService parChampService)
        {
            _gameRepo = parGameRepo;
            _playerService = parPlayerService;
            _champService = parChampService;
        }

        public void AddGame(Game parGame)
        {
            if (parGame == null || parGame.Id != 0 
                || parGame.Championship == null || parGame.Player == null)
            {
                return;
            }


            _champService.AddChampionship(parGame.Championship);
            _playerService.AddPlayer(parGame.Player);

            GameTable locGameTable = MapGameToTable(parGame);

            _gameRepo.Add(locGameTable);
            parGame.Id = locGameTable.Id;
        }

        public void UpdateGame(Game parGame)
        {
            if (parGame == null || parGame.Id == 0
               || parGame.Championship == null || parGame.Player == null)
            {
                return;
            }

            _champService.UpdateChampionship(parGame.Championship);
            _playerService.UpdatePlayer(parGame.Player);

            GameTable locGameTable = MapGameToTable(parGame);

            _gameRepo.Update(locGameTable);
        }

        private GameTable MapGameToTable(Game parGame)
        {
            GameTable locGameTable = new GameTable();
            locGameTable.Id = parGame.Id;
            locGameTable.PlayerId = parGame.Player._id;
            locGameTable.ChampionshipId = parGame.Championship._id;
            locGameTable.JourneyIndex = parGame.JourneyIndex;
            locGameTable.Season = parGame.Season;

            return locGameTable;
        }

        public Game GetGame()
        {
            var locGameTable = _gameRepo.Get();

            Game locGame = null;

            if (locGameTable != null)
            {
                locGame = new Game();
                locGame.Id = locGameTable.Id;
                locGame.Player = _playerService.GetPlayer(locGameTable.PlayerId);
                locGame.Championship = _champService.GetChampionship(locGameTable.ChampionshipId);
                locGame.JourneyIndex = locGameTable.JourneyIndex;
                locGame.Season = locGameTable.Season;

            }

            return locGame;
        }
    }
}
