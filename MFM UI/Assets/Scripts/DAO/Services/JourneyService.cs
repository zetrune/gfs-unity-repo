﻿using Assets.Scripts.DAO.Repository;
using Assets.Scripts.DAO.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.DAO.Services
{

    public interface IJourneyService
    {
        JourneyTable AddJourney(Journey parJourney);
        void UpdateJourney(Journey parJourney);
        Journey GetJourney(int parJourneyId);
        void RemoveJourney(int parJourneyId);
    }

    public class JourneyService : IJourneyService
    {
        private readonly IJourneyRepository _journeyRepo;
        private readonly IMatchService _matchService;
        private readonly IMatchJourneyRepository _matchJourneyRepo;

        public JourneyService(IJourneyRepository parJourneyRepo, IMatchService parMatchService, IMatchJourneyRepository parMatchJourneyRepo)
        {
            _journeyRepo = parJourneyRepo;
            _matchService = parMatchService;
            _matchJourneyRepo = parMatchJourneyRepo;
        }

        public JourneyTable AddJourney(Journey parJourney)
        {
            if (parJourney == null || parJourney._id != 0)
            {
                return null;
            }

            JourneyTable locJourneyTable = MapJourneyToTable(parJourney);

            _journeyRepo.Add(locJourneyTable);

            foreach (var locMatch in parJourney._tabMatchs)
            {
                _matchService.AddMatch(locMatch);

                MatchJourneyTable locMatchJourneyTable = new MatchJourneyTable();
                locMatchJourneyTable.JourneyId = locJourneyTable.Id;
                locMatchJourneyTable.MatchId = locMatch._id;

                _matchJourneyRepo.Add(locMatchJourneyTable);
            }

            return locJourneyTable;
        }

        public void UpdateJourney(Journey parJourney)
        {
            if (parJourney == null || parJourney._id == 0)
            {
                return;
            }

            JourneyTable locJourneyTable = MapJourneyToTable(parJourney);

            _journeyRepo.Update(locJourneyTable);

            foreach (var locMatch in parJourney._tabMatchs)
            {
                _matchService.UpdateMatch(locMatch);
            }
        }

        private JourneyTable MapJourneyToTable(Journey parJourney)
        {
            JourneyTable locJourney = new JourneyTable();
            locJourney.Id = parJourney._id;

            return locJourney;
        }

        public Journey GetJourney(int parJourneyId)
        {
            var locJourneyTable = _journeyRepo.GetById(parJourneyId);

            Journey result = null;

            if (locJourneyTable != null)
            {
                result = MapTableToJourney(locJourneyTable);
                result._tabMatchs = GetJourneyMatchs(result);
            }

            return result;
        }

        private List<Match> GetJourneyMatchs(Journey parJourney)
        {
            var locTabMatchJourney = _matchJourneyRepo.GetByProperty("JourneyId", parJourney._id.ToString());

            var result = new List<Match>();

            foreach (var locMatchJourney in locTabMatchJourney)
            {
                var locMatch = _matchService.GetMatch(locMatchJourney.MatchId);
                
                if (locMatch != null)
                {
                    result.Add(locMatch);
                }
            }

            return result;
        }

        private Journey MapTableToJourney(JourneyTable locJourneyTable)
        {
            Journey locJourney = new Journey();
            locJourney._id = locJourneyTable.Id;
            return locJourney;
        }

        public void RemoveJourney(int parJourneyId)
        {
            _journeyRepo.Delete(parJourneyId);

            var locTabMatchJourney = _matchJourneyRepo.GetByProperty("JourneyId", parJourneyId.ToString());

            foreach (var locMatchJourney in locTabMatchJourney)
            {
                _matchService.RemoveMatch(locMatchJourney.MatchId);
                _matchJourneyRepo.Delete(locMatchJourney.Id);
            }
        }
    }
}
