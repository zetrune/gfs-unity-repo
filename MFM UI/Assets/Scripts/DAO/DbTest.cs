﻿using System;
using UnityEngine;
using System.Collections;
using Assets.Scripts.DAO.Table;
using Assets.Scripts.DAO.Repository;
using Assets.Scripts.Managers;
using System.Linq;

namespace Assets.Scripts.DAO
{
    

    public class DbTest : MonoBehaviour
    {
        void Start()
        {
            var match = new MatchTable
            {
                FirstHalfIsPlayed = 1,
                IsPlayed = 0,
                LocalScore = 2,
                VisitorScore = 1,
                Time = 10,
                AvailableBonus = 3,
                UsedBonus = 2,
                LocalTeamId = 12345,
                VisitorTeamId = 67890
            };

            var dbMgr = new DbManager();
            dbMgr.OpenConnection();
            var repo = new MatchRepository(dbMgr);

            repo.Add(match);

            Debug.Log("Added: " + match.Id);

            var listDbAction = repo.GetByProperty("Time", 42.ToString());

            if (listDbAction != null && listDbAction.Count > 0)
            {
                var dbAction = listDbAction.First();

                Debug.Log("Success: " + dbAction.Id + " :: " + dbAction.Time);

                dbAction.Time = 87;

                repo.Update(dbAction);
                //repo.Delete(dbAction);
            }
            else
            {
                Debug.Log("Error !");
            }

            dbMgr.CloseConnection();
        }
    }

}
