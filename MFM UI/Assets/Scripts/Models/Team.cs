﻿using System;
using UnityEngine;
using System.Collections.Generic;
using Random = UnityEngine.Random;

public class Team {

    /**
	 * Variables pour une equipe
	 */
    public int _id;
	public String _teamName;                                            // Nom de l'equipe
    public bool _isProtected;                                           // Protection for historical favorite teams that can't have a status less than outsiders
	public TeamStatsModule _teamGlobalStats = new TeamStatsModule(); 	// Module permettant de conserver les statistiques d'une equipe
	public TeamStatsModule _teamLocalStats = new TeamStatsModule(); 	// Module permettant de conserver les statistiques d'une equipe a domicile
	public TeamStatsModule _teamVisitorStats = new TeamStatsModule(); 	// Module permettant de conserver les statistiques d'une equipe a l'exterieur
	public TeamStatusModule _teamStatus; 								// Module permettant de gerer le statut de l'equipe
	public TeamSkillsModule _teamSkills;								// Module permettant de gerer les competences d'une equipe
    public Color32  _teamPrimaryColor;
    public Color32 _teamSecondaryColor;

    /**
	 * Constructeur
	 * @param parTeamName Le nom de l'equipe
	 * @param parTeamStatus Le statut de l'equipe
	 */
    public Team(String parTeamName, TeamStatusModule parTeamStatus, TeamSkillsModule parTeamSkills,
	            Color32 parTeamPrimaryColor, Color32 parTeamSecondaryColor){
		_teamName = parTeamName;
		_teamStatus = parTeamStatus;
		_teamSkills = parTeamSkills;
		if (_teamStatus._status == TeamStatus.FAV) {
			_isProtected = true;
		} else {
			_isProtected = false;
		}
		_teamPrimaryColor = parTeamPrimaryColor;
		_teamSecondaryColor = parTeamSecondaryColor;
	}

    public Team()
    {

    }
	
	/**
	 * Function to set the tactics of the team regarding is the match is local and by comparing the status and the type of the opponent
	 * @param parOpponentTeam The opponent team 
	 * @param parMatchIsLocal True if the match is local false otherwise
	 */
	public MatchResult getPronostic(Team parOpponentTeam, bool parMatchIsLocal) {
		Tactic locTactic = getMatchTactic(parOpponentTeam, parMatchIsLocal);
		if (locTactic == Tactic.OFF) {
			return MatchResult.VICTORY;
		} else if (locTactic == Tactic.NEUTRAL) {
			return MatchResult.DRAW;
		} else {
			return MatchResult.LOSE;
		}
	}
	
	public int getFutureStatusObjective() {
		Trend locTrend = getStatusTrend();
		int locCurStatusIndex = _teamStatus._statusIndex;

		if (((locCurStatusIndex == 3) && (locTrend == Trend.UP))
		    || ((locCurStatusIndex == 0) && (locTrend == Trend.DOWN))) {
			return _teamStatus.getBoardObjective();
		}
		
		if (locTrend == Trend.UP) {
			return ConstManager.TAB_TEAM_STATUS_BOARD_OBJECTIVE[locCurStatusIndex + 1];
		} else if (locTrend == Trend.DOWN) {
			return ConstManager.TAB_TEAM_STATUS_BOARD_OBJECTIVE[locCurStatusIndex - 1];
		} else {
			return _teamStatus.getBoardObjective();
		}
	}
	
	public void updateTeamStatus() {
		Trend locTmpTrend = getStatusTrend();
		
		if (locTmpTrend == Trend.UP) {
			_teamStatus.upgradeTeamGlobalStatus();
		} else if (locTmpTrend == Trend.DOWN) {
			_teamStatus.downgradeTeamGlobalStatus();
		}
	}
	
	public void updateBoardTrust() {
		Trend locTmpTrend = getStatusTrend();
		
		if (locTmpTrend == Trend.UP) {
			PlayerManager.getInstance().upgradeBoardTrust(GameManager._instance.Game.Player);
		} else if (!isObjectiveAchieve()) {
			PlayerManager.getInstance().downgradeBoardTrust(GameManager._instance.Game.Player);
		}
	}
	
	/**
	 * Fonction qui permet d'obtenir la tendance du statut d'une equipe en fonction de son classement
	 */
	public Trend getStatusTrend() {
		if (_teamGlobalStats._currentRank < _teamStatus.getHighestRank()) { // Le classement de l'equipe est superieur a la position la plus haute possible pour son statut
			return Trend.UP;
		} else {
			if (_teamGlobalStats._currentRank > _teamStatus.getLowestRank()) { // Le classement de l'equipe est inferieur a la position la plus faible possible pour son statut
				if (_isProtected && _teamStatus._statusIndex == 2) {
					return Trend.STABLE;
				} else {
					return Trend.DOWN;
				}
			} else {
				return Trend.STABLE;		
			}
		}	
	}
	
	/**
	 * Fonction qui permet de mettre a jour les modules statistiques d'une equipe en fonction des resultats d'un match joue a domicile ou a l'exterieur
	 * @param parMatchResult Le resultat du match
	 * @param parGoalScored Le nombre de buts marques
	 * @param parGoalConceed Le nombre de buts encaisses
	 * @param parHome Option permettant d'indiquer si l'equipe joue a domicile (Vrai) ou a l'exterieur (Faux)
	 */
	public void updateAllTeamStatsModule(MatchResult parMatchResult, int parGoalScored, int parGoalConceed, bool parHome) {
		if (parHome) {
			updateTeamStatsModule(parMatchResult, parGoalScored, parGoalConceed, _teamGlobalStats);
			updateTeamStatsModule(parMatchResult, parGoalScored, parGoalConceed, _teamLocalStats);
		} else {
			updateTeamStatsModule(parMatchResult, parGoalScored, parGoalConceed, _teamGlobalStats);
			updateTeamStatsModule(parMatchResult, parGoalScored, parGoalConceed, _teamVisitorStats);
		}
	}
	
	/**
	 * Fonction qui permet de mettre a jour un module de statistique en fonction des resultats d'un match
	 * @param parMatchResult Le resultat du match
	 * @param parGoalScored Le nombre de buts marques
	 * @param parGoalConceed Le nombre de buts encaisses
	 * @param parTeamStatsModule Le module de statistique a mettre a jour
	 */
	public void updateTeamStatsModule(MatchResult parMatchResult, int parGoalScored, int parGoalConceed, TeamStatsModule parTeamStatsModule) {
		if (parMatchResult == MatchResult.DRAW) {
			parTeamStatsModule._nbDraw += 1;
		} else {
			if (parMatchResult == MatchResult.VICTORY) {
				parTeamStatsModule._nbVictory += 1;
			} else {
				parTeamStatsModule._nbLose += 1;
			}
		}
		int locMatchResultIndex = (int) parMatchResult;
		parTeamStatsModule._nbMatchs += 1;
		parTeamStatsModule._nbGoalScored += parGoalScored;
		parTeamStatsModule._nbGoalConceded += parGoalConceed;
		parTeamStatsModule._goalAverage = parTeamStatsModule._goalAverage + parGoalScored - parGoalConceed; 
		parTeamStatsModule._nbPoints += ConstManager.TAB_MATCH_RESULT_POINT[locMatchResultIndex];
		parTeamStatsModule.updateRateStats();
	}
	
	
	/**
	 * Function to update the team rank on a team stats module
	 * @param parTeamStatsModule The target team stats module
	 * @param parCurrentRank The new rank
	 * @param parIsInit Indicate if it is the first journey (true) or not (false)
	 */
	public void updateTeamRank(TeamStatsModule parTeamStatsModule, int parCurrentRank, bool parIsInit) {
		parTeamStatsModule.updateTeamRank(parCurrentRank, parIsInit);
	}
	
	public void resetTeam() {
		_teamGlobalStats = new TeamStatsModule();
		_teamLocalStats = new TeamStatsModule();
		_teamVisitorStats = new TeamStatsModule();
	}
	
	/**
	 * Function to set the tactics of the team regarding is the match is local and by comparing the status and the type of the opponent
	 * @param parOpponentTeam The opponent team 
	 * @param parMatchIsLocal True if the match is local false otherwise
	 */
	public void setMatchTactic(Team parOpponentTeam, bool parMatchIsLocal) {
		_teamSkills.setTeamTactic(getMatchTactic(parOpponentTeam, parMatchIsLocal));
	}
	
	/**
	 * Function to set the tactics of the team regarding is the match is local and by comparing the status and the type of the opponent
	 * @param parOpponentTeam The opponent team 
	 * @param parMatchIsLocal True if the match is local false otherwise
	 */
	public Tactic getMatchTactic(Team parOpponentTeam, bool parMatchIsLocal) {
		int locStatusComparator = _teamStatus._statusIndex - parOpponentTeam._teamStatus._statusIndex; 
		int locTypeComparator = (int) _teamSkills._teamType - (int) parOpponentTeam._teamSkills._teamType;
		
		if (parMatchIsLocal) {
			switch (locStatusComparator) {
			case 3: case 2:
				// The opponent is very lower
				return Tactic.OFF;
			case 1:
				// The opponent is a little bit lower
				// The team is at home
				// Neutral tactic versus opponent little bit more offensive
				return setMatchTacticRegardingOpponentType(locTypeComparator, -1);
			case 0:
				// The opponent has the same status
				// The team is at home
				// Neutral tactic versus opponent with the same status
				return setMatchTacticRegardingOpponentType(locTypeComparator, 0);
			case -1:
				// The opponent is a little bit better
				// The team is at home
				// Neutral tactic versus opponent little bit more defensive
				return setMatchTacticRegardingOpponentType(locTypeComparator, 1);
			default:
				// The opponent is better from far
				return Tactic.DEF;
			}
		} else {
			switch (locStatusComparator) {
			case 3: case 2:
				// The opponent is very lower
				return Tactic.OFF;
			case 1:
				// The opponent is a little bit lower
				// The team is visitor
				// Neutral tactic versus opponent little bit more offensive
				return setMatchTacticRegardingOpponentType(locTypeComparator, 0);
			case 0:
				// The opponent has the same status
				// The team is visitor
				// Neutral tactic versus opponent little bit more defensive
				return setMatchTacticRegardingOpponentType(locTypeComparator, 1);
			default:
				// The opponent is better from far
				return Tactic.DEF;
			}
		}
	}
	
	private Tactic setMatchTacticRegardingOpponentType(int parTypeComparator, int parNeutralResult) {
		if (parTypeComparator > parNeutralResult) {
			return Tactic.OFF;
		} else if (parTypeComparator == parNeutralResult) {
			return Tactic.NEUTRAL;
		} else {
			return Tactic.DEF;
		}
	}
	
	public void setMatchTacticRandomly() {
		int locRandom = Random.Range(0, 2);
		switch (locRandom) {
		case 0:
			_teamSkills.setTeamTactic(Tactic.DEF);
			break;
		case 1:
			_teamSkills.setTeamTactic(Tactic.NEUTRAL);
			break;
		case 2:
			_teamSkills.setTeamTactic(Tactic.OFF);
			break;
		}
	}
	
	public bool isObjectiveAchieve() {
		if ((_teamGlobalStats._currentRank) <= _teamStatus.getBoardObjective()) {
			return true;
		} else {
			return false;
		}
	}
}
