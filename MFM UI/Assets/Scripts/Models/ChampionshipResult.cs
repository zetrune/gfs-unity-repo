﻿using UnityEngine;
using System.Collections;

public class ChampionshipResult {

	public string _championshipFlag;
	public string _teamName;
	public TeamStatsModule _teamStats;
	
	public ChampionshipResult(Championship parChampionship, Team parTeam) {
		_championshipFlag = string.Copy(parChampionship._flagName);
		_teamName = string.Copy(parTeam._teamName);
		_teamStats = new TeamStatsModule(parTeam._teamGlobalStats);
	}
}
