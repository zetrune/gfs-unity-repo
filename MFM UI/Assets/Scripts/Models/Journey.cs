﻿using UnityEngine;
using System.Collections.Generic;
using System;

public class Journey {

    /**
	 * Variables d'une journee
	 */
    public int _id;
	public List<Match> _tabMatchs; // Liste des matchs de la journee
	
	/**
	 * Constructeur
	 */
	public Journey() {
		_tabMatchs = new List<Match>();
	}
	
	/**
	 * Fonction qui permet d'ajouter un match
	 * @param parMatch Le match a ajouter
	 */
	public void addMatch(Match parMatch) {
		_tabMatchs.Add(parMatch);
	}

	/**
	 * Fonction qui permet d'obtenir les matchs retour d'une journee
	 * @return Les matchs retours
	 */
	public Journey getReturnJourney() {
		Journey locReturnJourney = new Journey();
		Match locTmpMatch;
		int i = 0;
		for (i = 0; i < _tabMatchs.Count; i++) {
			locTmpMatch = _tabMatchs[i].getReturnMatch();
			locReturnJourney.addMatch(locTmpMatch);
		}
		return locReturnJourney;
	}

	/**
	 * Fonction qui permet de jouer les matchs d'une journee
	 */
	public void playJourney() {
		int locNbMatch = _tabMatchs.Count;
		for (int i = 0; i < locNbMatch; i++) {
			MatchManager.getInstance().playMatchActions(_tabMatchs[i]);
		}
	}
}
