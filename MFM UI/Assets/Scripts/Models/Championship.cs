﻿using System;
using UnityEngine;
using System.Collections.Generic;

public class Championship {

    /**
	 * Variables du championnat
	 */
    public int _id;
	public String _name;							// Nom du championnat
	public String _flagName;						// Nom du sprite de drapeau du championnat
	public List<Team> _tabFirstLeagueTeams; 		// First league team list
	public List<Team> _tabSecondLeagueTeams;		// Second league team list
	public List<Journey> _tabJourneysFirstLeague; 	// La  liste des journees du championnat 1

    /**
	 * Constructeur a partir d'une liste d'equipe de premiere et seconde league
	 * @param parName Championship name
	 * @param parFlagNage Flag asset name
	 * @param parTabFirstLeagueTeam First League team list
	 */
    public Championship(String parName, String parFlagName, List<Team> parTabFirstLeagueTeam) {
		_name = parName;
		_flagName = parFlagName;
		_tabFirstLeagueTeams = parTabFirstLeagueTeam;
	}

	/**
	 * Function to update team rank after the match of the journey
	 * @param parIsInit Boolean to indicate if is the first init for rank compute
	 */
	public void updateTeamRank(bool parIsInit) {
		updateAllTeamRank (_tabFirstLeagueTeams, parIsInit);
	}

	/**
	 * Function that indicate if the championship is finished (true) or not (false)
	 * @return True if the championship is finished false otherwise
	 */
	public bool isFinished(int parJourneyIndex) {
		if (parJourneyIndex > _tabJourneysFirstLeague.Count - 1)
			return true;
		else if (parJourneyIndex == _tabJourneysFirstLeague.Count - 1) {
			Journey locJourney = _tabJourneysFirstLeague[parJourneyIndex];
			for (int i = 0; i < locJourney._tabMatchs.Count; i++) {
				if (!locJourney._tabMatchs[i]._isPlayed) {
					return false;
				}
			}
			return true;
		}
		return false;
	}

	/**
	 * Function to set All team status to the target status
	 * @param parTabTeam The target teams
	 * @param parTeamStatus The target team status
	 */ 
	public void setAllTeamStatus (List<Team> parTabTeam, TeamStatus parTeamStatus){
		foreach (Team locTmpTeam in parTabTeam) {
			locTmpTeam._teamStatus._status = parTeamStatus;
			locTmpTeam._teamStatus._statusIndex = (int) parTeamStatus;
		}
	}

	/**
	 * Function to reset all championship stats and calendar
	 */
	public void resetChampionship() {
		sortGlobalRanking(_tabFirstLeagueTeams);
		sortGlobalRanking(_tabSecondLeagueTeams);
		_tabJourneysFirstLeague.Clear();
		_tabJourneysFirstLeague = null;
		Team locTmpTeam;
		for (int i = 0; i < _tabFirstLeagueTeams.Count; i++) {
			locTmpTeam = _tabFirstLeagueTeams[i];
			locTmpTeam.resetTeam();
		}
		for (int i = 0; i < _tabSecondLeagueTeams.Count; i++) {
			locTmpTeam = _tabSecondLeagueTeams[i];
            locTmpTeam.resetTeam();
		}
	}

	/**
	 ****************************************************************************************
	 *																						*
	 *---------------------------   Private functions  -------------------------------------*
	 *																						*
	 ****************************************************************************************
	 */

	/**
	 * Function to update team rank at the end of the played journey
	 * @param parLeagueTeams The concerned league teams
	 * @param parIsInit Boolean that indicate if it is the first journey
	 */
	private void updateAllTeamRank(List<Team> parLeagueTeams, bool parIsInit) {
		Team locTmpTeam;
		sortGlobalRanking(parLeagueTeams);
		for (int i = 0; i < parLeagueTeams.Count; i++) {
			locTmpTeam = parLeagueTeams[i];
			locTmpTeam.updateTeamRank(locTmpTeam._teamGlobalStats, i + 1, parIsInit);
		}
		sortLocalRanking(parLeagueTeams);
		for (int i = 0; i < parLeagueTeams.Count; i++) {
			locTmpTeam = parLeagueTeams[i];
			locTmpTeam.updateTeamRank(locTmpTeam._teamLocalStats, i + 1, parIsInit);
		}
		sortVisitorRanking(parLeagueTeams);
		for (int i = 0; i < parLeagueTeams.Count; i++) {
			locTmpTeam = parLeagueTeams[i];
			locTmpTeam.updateTeamRank(locTmpTeam._teamVisitorStats, i + 1, parIsInit);
		}
		sortGlobalRanking(parLeagueTeams);
	}

	/**
	 ****************************************************************************************
	 *																						*
	 *---------------------------   Sorting functions  -------------------------------------*
	 *																						*
	 ****************************************************************************************
	 */

	/**
	 * Function to sort team regarding the global ranking
	 */
	public void sortGlobalRanking(List<Team> parTabTeam) {
		parTabTeam.Sort (new CompareTeamGlobal ());
	}
	
	/**
	 * Function to sort team regarding the local ranking
	 */
	public void sortLocalRanking(List<Team> parTabTeam) {
		parTabTeam.Sort (new CompareTeamLocal ());
	}
	
	/**
	 * Function to sort team regarding the visitor ranking
	 */
	public void sortVisitorRanking(List<Team> parTabTeam) {
		parTabTeam.Sort (new CompareTeamVisitor ());
	}
	
	private class CompareTeamGlobal : Comparer<Team> {

		public override int Compare(Team parTeam1, Team parTeam2) {
			if (parTeam1._teamGlobalStats._nbPoints > parTeam2._teamGlobalStats._nbPoints) {
				return -1;
			} else if (parTeam1._teamGlobalStats._nbPoints < parTeam2._teamGlobalStats._nbPoints) {
				return 1;
			} else if (parTeam1._teamGlobalStats._goalAverage > parTeam2._teamGlobalStats._goalAverage) {
				return -1;
			} else if (parTeam1._teamGlobalStats._goalAverage < parTeam2._teamGlobalStats._goalAverage) {
				return 1;
			} else if (parTeam1._teamGlobalStats._nbGoalScored > parTeam2._teamGlobalStats._nbGoalScored) {
				return -1;
			} else if (parTeam1._teamGlobalStats._nbGoalScored < parTeam2._teamGlobalStats._nbGoalScored) {
				return 1;
			} else {
				return string.Compare(parTeam1._teamName, parTeam2._teamName);
			}
		}
	}
	
	private class CompareTeamLocal : Comparer<Team> {

		public override int Compare(Team parTeam1, Team parTeam2) {
			if (parTeam1._teamLocalStats._nbPoints > parTeam2._teamLocalStats._nbPoints) {
				return -1;
			} else if (parTeam1._teamLocalStats._nbPoints < parTeam2._teamLocalStats._nbPoints) {
				return 1;
			} else if (parTeam1._teamLocalStats._goalAverage > parTeam2._teamLocalStats._goalAverage) {
				return -1;
			} else if (parTeam1._teamLocalStats._goalAverage < parTeam2._teamLocalStats._goalAverage) {
				return 1;
			} else if (parTeam1._teamLocalStats._nbGoalScored > parTeam2._teamLocalStats._nbGoalScored) {
				return -1;
			} else if (parTeam1._teamLocalStats._nbGoalScored < parTeam2._teamLocalStats._nbGoalScored) {
				return 1;
			} else {
				return string.Compare(parTeam1._teamName, parTeam2._teamName);
			}
		}
	}
	
	private class CompareTeamVisitor : Comparer<Team> {

		public override int Compare(Team parTeam1, Team parTeam2) {
			if (parTeam1._teamVisitorStats._nbPoints > parTeam2._teamVisitorStats._nbPoints) {
				return -1;
			} else if (parTeam1._teamVisitorStats._nbPoints < parTeam2._teamVisitorStats._nbPoints) {
				return 1;
			} else if (parTeam1._teamVisitorStats._goalAverage > parTeam2._teamVisitorStats._goalAverage) {
				return -1;
			} else if (parTeam1._teamVisitorStats._goalAverage < parTeam2._teamVisitorStats._goalAverage) {
				return 1;
			} else if (parTeam1._teamVisitorStats._nbGoalScored > parTeam2._teamVisitorStats._nbGoalScored) {
				return -1;
			} else if (parTeam1._teamVisitorStats._nbGoalScored < parTeam2._teamVisitorStats._nbGoalScored) {
				return 1;
			} else {
                return string.Compare(parTeam1._teamName, parTeam2._teamName);
			}
		}
	}
}
