﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Random = UnityEngine.Random;

public class Action
{
    public int Id { get; set; }
    public ActionSide Side { get; set; }
    public int Time { get; set; }
    public ActionBonus Bonus { get; set; }
    public ActionState Defense1 { get; set; }
    public ActionState Attack1 { get; set; }
    public ActionState Attack2 { get; set; }
    public ActionState Defense2 { get; set; }


    public Action()
    {

    }

    public Action(ActionSide parSide, int parPosition)
    {
        Side = parSide;
        Time = 0;
        if (parPosition >= ConstManager.MAX_ACTIONS_PER_HALF)
        {
            Time = 45;
        }
        Time += (parPosition % ConstManager.MAX_ACTIONS_PER_HALF) * ConstManager.MAX_ACTIONS_PER_HALF + Random.Range(0, ConstManager.MAX_ACTIONS_PER_HALF);

        Bonus = ActionBonus.UNKNOWN;
        Defense1 = ActionState.NONE;
        Attack1 = ActionState.NONE;
        Attack2 = ActionState.NONE;
        Defense2 = ActionState.NONE;

    }

    public bool Resolved()
    {
        bool result = false;

        if (Defense1 == ActionState.WIN ||
            Attack1 == ActionState.LOSE ||
            Attack2 == ActionState.LOSE ||
            Defense2 != ActionState.NONE)
        {
            result = true;
        }

        return result;
    }
}
