﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.Models
{
    public class Game
    {
        public int Id { get; set; }
        public Player Player { get; set; }
        public Championship Championship { get; set; }
        public int JourneyIndex { get; set; }
        public int Season { get; set; }
    }
}
