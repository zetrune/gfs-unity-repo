﻿using UnityEngine;
using System.Collections.Generic;
using System;

public class Match {

    /**
	 * Variables d'un match
	 */
    public int _id;
    public bool _firstHalfIsPlayed;                 // Indique si la premiere mi-temps a ete joue ou pas
    public bool _isPlayed;                          // Indique si le match a ete joue ou pas
    public int _localScore;							// Score de l'equipe a domicile
	public int _visitorScore; 						// Score de l'equipe a l'exterieur
	public int _time;								// Minute de jeu
    public int _availableBonus;                     // Nombre de bonus disponible
    public int _usedBonus;                          // Nombre de bonus utilises
    public int _localTeam;                         // Equipe a domicile
    public int _visitorTeam; 						// Equipe a l'exterieur
    public List<Action> _actions;                   // List des actions du match

    /**
	 * Constructeur
	 * @param parLocalTeam L'equipe jouant a domicile
	 * @param parVisitorTeam L'equipe jouant a l'exterieur
	 */
    public Match(int parLocalTeam, int parVisitorTeam) {
		_localTeam = parLocalTeam;
		_visitorTeam = parVisitorTeam;
		_localScore = 0;
		_visitorScore = 0;
		_isPlayed = false;
		_time = 0;
        _actions = new List<Action>();
        _availableBonus = 0;
        _usedBonus = 0;
	}
	
	public MatchResult getMatchResult(int parTeam) {
		if (_localScore == _visitorScore) {
			return MatchResult.DRAW;
		} else if (((_localScore > _visitorScore) && (_localTeam == parTeam)) ||
		           ((_localScore < _visitorScore) && (_visitorTeam == parTeam))) {
			return MatchResult.VICTORY;
		} else {
			return MatchResult.LOSE;
		}
	}
	
	public int getGoalFor(int parTeam) {
		if (_localTeam == parTeam) {
			return _localScore;
		} else {
			return _visitorScore;
		}
	}
	
	public int getGoalAgainst(int parTeam) {
		if (_localTeam == parTeam) {
			return _visitorScore;
		} else {
			return _localScore;
		}
	}
	
	/**
	 * Fonction qui permet d'obtenir un match retour en inversant l'equipe a domicile et celle a l'exterieur
	 * @return Le match retour
	 */
	public Match getReturnMatch() {
		Match locReturnMatch = new Match(_visitorTeam, _localTeam);
		return locReturnMatch;
	}
	
	public bool isLocal(Team parTeam) {
		return _localTeam.Equals(parTeam);
	}
	
	public bool isVisitor(Team parTeam) {
		return _visitorTeam.Equals(parTeam);
	}

    public Action LastAction()
    {
        Action locLastAction = null;

        if (_actions.Count != 0)
        {
            locLastAction = _actions[_actions.Count - 1];
        }

        return locLastAction;
    }

    public bool FirstHalfPlayed()
    {
        return (_actions.Count >= ConstManager.MAX_ACTIONS_PER_HALF);
    }

    public bool SecondHalfPlayed()
    {
        return (FirstHalfPlayed() && _actions.Count < ConstManager.MAX_ACTIONS_PER_HALF * 2);
    }
}
