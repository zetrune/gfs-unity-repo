﻿using UnityEngine;
using System.Collections;
using System;

public class TeamSkill {

	/**
	 * Variables d'une competence
	 */
	private string _name; 			// Nom de la competence
	private Grade _grade;					// Niveau de la competence
	private float _skillTreeBonusRate; 	// Pourcentage de chances supplementaires d'obtenir les modificateurs (points depenses dans l'arbre de competences)
	private float _atk;					// Percentage of atk provided by the skill
	private float _def;					// Percentage of def provided by the skill
	private string _textureName;			// Name of the texture.
	
	/**
	 * Constructeur
	 * @param parName Le nom de la competence
	 * @param parGrade Le grade de la competence
	 * @param parAtk The atk power
	 * @param parDef The def power
	 * @param parTextureName The name of the texture
	 */
	public TeamSkill (string parName, Grade parGrade, float parAtk, float parDef, string parTextureName){
		_name = parName;
		_grade = parGrade;
		_skillTreeBonusRate = 0.0f;
		_atk = parAtk;
		_def = parDef;
		_textureName = parTextureName;
	}
	
	/**
	 * Fonction qui permet de renvoyer le nom de la competence
	 * @return Le nom de la competence
	 */
	public string tostring() {
		return _name;
	}
	
	/**
	 * Fonction qui permet d'obtenir le grade de la competence
	 * @return Le grade de la competence
	 */
	public Grade getGrade() {
		return _grade;
	}

	private float getGradeRate() {
		int locGradeIndex = (int)_grade;
		return ConstManager.TAB_GRADE_RATE[locGradeIndex];
	}
	
	/**
	 * Fonction qui permet d'obtenir le pourcentage de chance necessaire pour avoir une occasion
	 * @return Le pourcentage de chance necessaire
	 */
	public float getSkillScoreAtk() {
		float locSkillRate = (getGradeRate() + _skillTreeBonusRate) * _atk;
		return locSkillRate;
	}
	
	/**
	 * Fonction qui permet d'obtenir le pourcentage de chance necessaire pour avoir un sauvetage
	 * @return Le pourcentage de chance necessaire
	 */
	public float getSkillScoreDef() {
		float locSkillRate = (getGradeRate() + _skillTreeBonusRate) * _def;
		return locSkillRate;
	}
	
	/**
	 * Fonction qui permet d'ajouter un pourcentage de chance supplementaire en fonction des points depenses dans l'arbre de competences
	 * @param parBonusRate Le pourcentage de chance a ajouter
	 */
	public void addSkillTreeBonusRate(float parSkillTreeBonusRate) {
		_skillTreeBonusRate += parSkillTreeBonusRate;
	}
	
	/**
	 * Fonction qui permet de remettre a zero le pourcentage de chance supplementaires issue de l'arbre des competences lors d'une remise a zero de celui-ci
	 */
	public void resetSkillTreeBonusRate() {
		_skillTreeBonusRate = 0.0f;
	}
	
	public string getTextureName() {
		return _textureName;
	}
}
