﻿using UnityEngine;
using System.Collections;
using System;

public class Player {

    public int _id;
	public int _gamePoints;
    public int _boardTrust;     // La confiance des dirigeants
    public int _team;
    public PlayerStatsModule _statsModule;
	public PlayerPalmaresModule _palmaresModule;
	public PlayerHistoryModule _historyModule;
	
	public Player() {
		_gamePoints = 0;
		_team = 0;
		_statsModule = new PlayerStatsModule();
		_palmaresModule = new PlayerPalmaresModule();
		_historyModule = new PlayerHistoryModule();
        _boardTrust = 1;
	}
}
