﻿using UnityEngine;
using System.Collections;
using System;

public class PlayerInfoModule {

	public string _firstname;
	public string _lastname;
	
	public PlayerInfoModule() {
		_firstname = "John";
		_lastname = "Doe";
	}
}
