﻿using UnityEngine;
using System.Collections.Generic;
using System;

public class PlayerHistoryModule {

	public List<Match> _matchHistory;
	
	public PlayerHistoryModule() {
		_matchHistory = new List<Match>();
	}

	public void update(Match parMatch) {
		_matchHistory.Add(parMatch);

		if (_matchHistory.Count > ConstManager.MAX_MATCH) {
			_matchHistory.RemoveAt(0);
		}
	}
}
