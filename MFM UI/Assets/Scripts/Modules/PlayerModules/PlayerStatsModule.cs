﻿using UnityEngine;
using System;
using System.Collections.Generic;

public class PlayerStatsModule {

	public int _nbChampionship;
	public int _nbCompletedChampionship;
	public int _nbMatch;
	public int _nbVictory;
	public int _nbDraw;
	public int _nbLoss;
	public int _nbGA;
	public int _nbGF;
	
	public float _rateChampionship;
	public float _rateVictory;
	public float _rateDraw;
	public float _rateLoss;
	public float _rateGA;
	public float _rateGF;
	
	public PlayerStatsModule() {
		_nbChampionship = 0;
		_nbCompletedChampionship = 0;
		_nbMatch = 0;
		_nbVictory = 0;
		_nbDraw = 0;
		_nbLoss = 0;
		_nbGA = 0;
		_nbGF = 0;
		
		_rateChampionship = 0;
		_rateVictory = 0;
		_rateDraw = 0;
		_rateLoss = 0;
		_rateGA = 0;
		_rateGF = 0;
	
	}
	

	
	public void update(Championship parChampionship, int parTeam, Match parMatch) {
		updatePlayerStats(parMatch.getMatchResult(parTeam), parMatch.getGoalFor(parTeam), parMatch.getGoalAgainst(parTeam));
	}
	
	public void updateAfterChampionship(bool parIsCompleted) {
		_nbChampionship += 1;
		if (parIsCompleted) {
			_nbCompletedChampionship += 1;
		}
		float locRate = (float) _nbCompletedChampionship / _nbChampionship;
		_rateChampionship = (float) Math.Round(locRate, 2);
	}
	
	
	private void updatePlayerStats(MatchResult parMatchResult, int parGoalScored, int parGoalConceed) {
		if (parMatchResult == MatchResult.DRAW) {
			_nbDraw += 1;
		} else {
			if (parMatchResult == MatchResult.VICTORY) {
				_nbVictory += 1;
			} else {
				_nbLoss += 1;
			}
		}
		_nbMatch += 1;
		_nbGF += parGoalScored;
		_nbGA += parGoalConceed;
		updateRateStats();
	}
	
	private void updateRateStats() {
		if (_nbMatch > 0) {
			float locTmpRate = (float) _nbVictory / _nbMatch;
			_rateVictory = (float) Math.Round(locTmpRate, 2);
			locTmpRate = (float)_nbDraw / _nbMatch;
			_rateDraw = (float) Math.Round(locTmpRate, 2);
			locTmpRate = (float) _nbLoss / _nbMatch;
			_rateLoss = (float) Math.Round(locTmpRate, 2);
			locTmpRate = (float) _nbGF / _nbMatch;
			_rateGF = (float) Math.Round(locTmpRate, 2);
			locTmpRate = (float)_nbGA / _nbMatch;
			_rateGA = (float) Math.Round(locTmpRate, 2);
		}
	}
}
