﻿using UnityEngine;
using System.Collections.Generic;
using System;

public class PlayerPalmaresModule {

	public List<ChampionshipResult> _tabChampionshipResult;
	
	public PlayerPalmaresModule() {
		_tabChampionshipResult = new List<ChampionshipResult>();
	}
	
	public void update(ChampionshipResult parChampionshipResult) {
		_tabChampionshipResult.Add(parChampionshipResult);
	}
}
