﻿using UnityEngine;
using System;
using System.Collections;

public class TeamStatsModule {

    /**
	 * Variables pour un module de statistiques
	 */
    public int _id;
	public int _currentRank;			// The team rank
	public int _previousRank;			// The team last rank
	public int _nbVictory; 				// Le nombre de victoires
	public int _nbDraw; 				// Le nombre de matchs nuls
	public int _nbLose; 				// Le nombre de defaites
	public int _nbMatchs;				// Le nombre de match joues
	public int _nbGoalScored; 			// Le nombre de buts marques
	public int _nbGoalConceded; 		// Le nombre de buts encaisses
	public int _goalAverage;			// The team goal average
	public int _nbPoints; 				// Le nombre de points
	public float _rateVictory;			// The victory rate	
	public float _rateDraw;				// The draw rate
	public float _rateLose;				// The lose rate
	public float _averageGoalScored;	// The average number of goal scored
	public float _averageGoalConceded;	// The average number of goal conceded
	
	/**
	 * Constructeur
	 */
	public TeamStatsModule() {
		_currentRank = 0;
		_previousRank = 0;
		_nbVictory = 0;
		_nbDraw = 0;
		_nbLose = 0;
		_nbMatchs = 0;
		_nbGoalScored = 0;
		_nbGoalConceded = 0;
		_goalAverage = 0;
		_nbPoints = 0;
		_rateVictory = 0f;
		_rateDraw = 0f;
		_rateLose = 0f;
		_averageGoalScored = 0f;
		_averageGoalConceded = 0f;
	}
	
	public TeamStatsModule(TeamStatsModule parTeamStatsModule) {
		_currentRank = parTeamStatsModule._currentRank;
		_previousRank = parTeamStatsModule._previousRank;
		_nbVictory = parTeamStatsModule._nbVictory;
		_nbDraw = parTeamStatsModule._nbDraw;
		_nbLose = parTeamStatsModule._nbLose;
		_nbMatchs = parTeamStatsModule._nbMatchs;
		_nbGoalScored = parTeamStatsModule._nbGoalScored;
		_nbGoalConceded = parTeamStatsModule._nbGoalConceded;
		_goalAverage = parTeamStatsModule._goalAverage;
		_nbPoints = parTeamStatsModule._nbPoints;
		_rateVictory = parTeamStatsModule._rateVictory;
		_rateDraw = parTeamStatsModule._rateDraw;
		_rateLose = parTeamStatsModule._rateLose;
		_averageGoalScored = parTeamStatsModule._averageGoalScored;
		_averageGoalConceded = parTeamStatsModule._averageGoalConceded;
	}
	
	/**
	 * Function to update the team rank
	 * @param parCurrentRank the new rank
	 * @param parIsInit Indicate if it is the first journey (true) or not (false)
	 */
	public void updateTeamRank(int parCurrentRank, bool parIsInit) {
		if (parIsInit) {
			_previousRank = parCurrentRank;
		} else {
			_previousRank = _currentRank;
		}
		_currentRank = parCurrentRank;
	}
	
	public void updateRateStats() {
		if (_nbMatchs > 0) {
			float locTmpRate = (float) _nbVictory / _nbMatchs;
			_rateVictory = (float) Math.Round(locTmpRate, 2);
			locTmpRate = (float) _nbDraw / _nbMatchs;
			_rateDraw = (float) Math.Round(locTmpRate, 2);
			locTmpRate = (float) _nbLose / _nbMatchs;
			_rateLose = (float) Math.Round(locTmpRate, 2);
			locTmpRate = (float) _nbGoalScored / _nbMatchs;
			_averageGoalScored = (float) Math.Round(locTmpRate, 2);
			locTmpRate = (float) _nbGoalConceded / _nbMatchs;
			_averageGoalConceded = (float) Math.Round(locTmpRate, 2);
		}
	}
}
