using UnityEngine;
using System;
using System.Collections.Generic;

public class TeamSkillsModule
{

    /**
	 * Variable pour le module de competences d'une equipe
	 */
    public TeamType _teamType;                      // Le type de l'equipe
    public List<TeamSkill> _tabTeamFormation;       // Le tableau des formations de l'equipe (modifiable)
    public List<TeamSkill> _tabTeamPosition;        // Le tableau des positions de l'equipe (modifiable)
    public List<TeamSkill> _tabTeamPlay;            // Le tableau des types de jeu de l'equipe (modifiable)
    public List<TeamSkill> _tabTeamPass;            // Le tableau des types de passes de l'equipe (modifiable)
    public List<TeamSkill> _tabTeamPressing;        // Le tableau des types de pressing de l'equipe (modifiable)
    public List<TeamSkill> _tabTeamMarking;         // Le tableau des types de marquage de l'equipe (modifiable)
    public int _teamFormationIndex;                 // La formation actuelle de l'equipe
    public int _teamPositionIndex;                  // La position actuelle de l'equipe
    public int _teamPlayIndex;                      // Le type de jeu actuel de l'equipe
    public int _teamPassIndex;                      // Le type de passes actuel de l'equipe
    public int _teamPressingIndex;                  // Le type de pressing actuel de l'equipe
    public int _teamMarkingIndex;                   // Le type de marquage actuel de l'equipe
    public float _teamScoreAtk;                 // Le score d'attaque issus des competences actives
    public float _teamScoreDef;                 // Le score de defense issus des competences actives

    /**
	 * Constructeur
	 * @param parTeamType Le type de l'equipe
	 */
    public TeamSkillsModule(TeamType parTeamType)
    {
        _teamType = parTeamType;
        if (parTeamType.Equals(TeamType.DEF))
        {
            populateTeamDefSkills();
        }
        else if (parTeamType.Equals(TeamType.MID))
        {
            populateTeamMidSkills();
        }
        else
        {
            populateTeamOffSkills();
        }
        computeTeamScoreAtk();
        computeTeamScoreDef();
    }

    /**
	 * Fonction qui permet de remettre a zero les bonus des competences actives
	 */
    public void resetTeamScore()
    {
        _teamScoreAtk = 0f;
        _teamScoreDef = 0f;
    }

    /**
	 * Fonction qui permet de calculer le score d'atk pour l'ensemble des competences actives
	 */
    public void computeTeamScoreAtk()
    {
        _teamScoreAtk = 0f;
        _teamScoreAtk += _tabTeamFormation[_teamFormationIndex].getSkillScoreAtk() * 0.35f;
        _teamScoreAtk += _tabTeamPosition[_teamPositionIndex].getSkillScoreAtk() * 0.25f;
        _teamScoreAtk += _tabTeamPlay[_teamPlayIndex].getSkillScoreAtk() * 0.1f;
        _teamScoreAtk += _tabTeamPass[_teamPassIndex].getSkillScoreAtk() * 0.1f;
        _teamScoreAtk += _tabTeamPressing[_teamPressingIndex].getSkillScoreAtk() * 0.1f;
        _teamScoreAtk += _tabTeamMarking[_teamMarkingIndex].getSkillScoreAtk() * 0.1f;
        Math.Round(_teamScoreAtk, 2);
    }

    /**
	 * Fonction qui permet de calculer le score de def pour l'ensemble des competences actives
	 */
    public void computeTeamScoreDef()
    {
        _teamScoreDef = 0f;
        _teamScoreDef += _tabTeamFormation[_teamFormationIndex].getSkillScoreDef() * 0.35f;
        _teamScoreDef += _tabTeamPosition[_teamPositionIndex].getSkillScoreDef() * 0.25f;
        _teamScoreDef += _tabTeamPlay[_teamPlayIndex].getSkillScoreDef() * 0.1f;
        _teamScoreDef += _tabTeamPass[_teamPassIndex].getSkillScoreDef() * 0.1f;
        _teamScoreDef += _tabTeamPressing[_teamPressingIndex].getSkillScoreDef() * 0.1f;
        _teamScoreDef += _tabTeamMarking[_teamMarkingIndex].getSkillScoreDef() * 0.1f;
        Math.Round(_teamScoreDef, 2);
    }

    public void setTeamTactic(Tactic parTactic)
    {
        // Apply tactic Def regarding team type
        if (parTactic.Equals(Tactic.DEF))
        {
            if (_teamType.Equals(TeamType.DEF))
            {
                setTacticDefTeamDef();
            }
            else if (_teamType.Equals(TeamType.MID))
            {
                setTacticDefTeamMid();
            }
            else if (_teamType.Equals(TeamType.OFF))
            {
                setTacticDefTeamOff();
            }
            // Apply tactic Neutral regarding team type
        }
        else if (parTactic.Equals(Tactic.NEUTRAL))
        {
            if (_teamType.Equals(TeamType.DEF))
            {
                setTacticNeutralTeamDef();
            }
            else if (_teamType.Equals(TeamType.MID))
            {
                setTacticNeutralTeamMid();
            }
            else if (_teamType.Equals(TeamType.OFF))
            {
                setTacticNeutralTeamOff();
            }
            // Apply tactic Off regarding team type
        }
        else if (parTactic.Equals(Tactic.OFF))
        {
            if (_teamType.Equals(TeamType.DEF))
            {
                setTacticOffTeamDef();
            }
            else if (_teamType.Equals(TeamType.MID))
            {
                setTacticOffTeamMid();
            }
            else if (_teamType.Equals(TeamType.OFF))
            {
                setTacticOffTeamOff();
            }
        }
        computeTeamScoreAtk();
        computeTeamScoreDef();
    }

    private void setTacticDefTeamDef()
    {
        setNormalTacticsIndex(3, 1);
        setAdvancedTacticsIndex(0, 1, 0, 0);
    }

    private void setTacticDefTeamMid()
    {
        setNormalTacticsIndex(1, 0);
        setAdvancedTacticsIndex(0, 1, 0, 0);
    }

    private void setTacticDefTeamOff()
    {
        setNormalTacticsIndex(1, 0);
        setAdvancedTacticsIndex(0, 1, 0, 0);
    }

    private void setTacticNeutralTeamDef()
    {
        setNormalTacticsIndex(0, 2);
        setAdvancedTacticsIndex(2, 2, 1, 2);
    }

    private void setTacticNeutralTeamMid()
    {
        setNormalTacticsIndex(3, 1);
        setAdvancedTacticsIndex(2, 2, 1, 2);
    }

    private void setTacticNeutralTeamOff()
    {
        setNormalTacticsIndex(0, 1);
        setAdvancedTacticsIndex(2, 2, 1, 2);
    }

    private void setTacticOffTeamDef()
    {
        setNormalTacticsIndex(2, 3);
        setAdvancedTacticsIndex(1, 0, 2, 1);
    }

    private void setTacticOffTeamMid()
    {
        setNormalTacticsIndex(2, 2);
        setAdvancedTacticsIndex(1, 0, 2, 1);
    }

    private void setTacticOffTeamOff()
    {
        setNormalTacticsIndex(3, 2);
        setAdvancedTacticsIndex(1, 0, 2, 1);
    }

    private void setAdvancedTacticsIndex(int parPlayIndex, int parPassIndex, int parPressingIndex, int parMarkingIndex)
    {
        _teamPlayIndex = parPlayIndex;
        _teamPassIndex = parPassIndex;
        _teamPressingIndex = parPressingIndex;
        _teamMarkingIndex = parMarkingIndex;
    }

    private void setNormalTacticsIndex(int parFormationIndex, int parPositionIndex)
    {
        _teamFormationIndex = parFormationIndex;
        _teamPositionIndex = parPositionIndex;
    }

    private void populateTeamDefSkills()
    {
        // Creation du tableau de formations
        _tabTeamFormation = new List<TeamSkill>();
        this._tabTeamFormation.Add(new TeamSkill("formation_442_neutral", Grade.REGIONAL, 0.5f, 0.5f, "mfm_texture_formation_4-4-2_mid"));
        this._tabTeamFormation.Add(new TeamSkill("formation_442_defensive", Grade.REGIONAL, 0.45f, 0.55f, "mfm_texture_formation_4-4-2_def"));
        this._tabTeamFormation.Add(new TeamSkill("formation_442_offensive", Grade.REGIONAL, 0.55f, 0.45f, "mfm_texture_formation_4-4-2_off"));
        this._tabTeamFormation.Add(new TeamSkill("formation_451_neutral", Grade.REGIONAL, 0.4f, 0.6f, "mfm_texture_formation_4-5-1_mid"));
        this._tabTeamFormation.Add(new TeamSkill("formation_451_defensive", Grade.CONTINENTAL, 0.3f, 0.7f, "mfm_texture_formation_4-5-1_def"));
        this._tabTeamFormation.Add(new TeamSkill("formation_451_offensive", Grade.NATIONAL, 0.55f, 0.45f, "mfm_texture_formation_4-5-1_off"));
        this._tabTeamFormation.Add(new TeamSkill("formation_352_neutral", Grade.NATIONAL, 0.5f, 0.5f, "mfm_texture_formation_3-5-2_mid"));
        this._tabTeamFormation.Add(new TeamSkill("formation_352_defensive", Grade.CONTINENTAL, 0.35f, 0.65f, "mfm_texture_formation_3-5-2_def"));
        // Creation du tableau de type de jeu
        _tabTeamPlay = new List<TeamSkill>();
        this._tabTeamPlay.Add(new TeamSkill("play_direct", Grade.REGIONAL, 0.4f, 0.6f, "mfm_texture_play_direct"));
        this._tabTeamPlay.Add(new TeamSkill("play_wing", Grade.NATIONAL, 0.6f, 0.4f, "mfm_texture_play_wings"));
        this._tabTeamPlay.Add(new TeamSkill("play_possess", Grade.CONTINENTAL, 0.5f, 0.5f, "mfm_texture_play_possession"));
        // Creation du tableau de type de marquage
        _tabTeamMarking = new List<TeamSkill>();
        this._tabTeamMarking.Add(new TeamSkill("marking_individual", Grade.REGIONAL, 0.35f, 0.65f, "mfm_texture_marking_individual"));
        this._tabTeamMarking.Add(new TeamSkill("marking_zone", Grade.CONTINENTAL, 0.65f, 0.35f, "mfm_texture_marking_zone"));
        this._tabTeamMarking.Add(new TeamSkill("marking_mixed", Grade.NATIONAL, 0.5f, 0.5f, "mfm_texture_marking_mixed"));
        // Creation du tableau de type de passes
        _tabTeamPass = new List<TeamSkill>();
        this._tabTeamPass.Add(new TeamSkill("pass_short", Grade.CONTINENTAL, 0.55f, 0.45f, "mfm_texture_pass_short"));
        this._tabTeamPass.Add(new TeamSkill("pass_long", Grade.REGIONAL, 0.45f, 0.55f, "mfm_texture_pass_long"));
        this._tabTeamPass.Add(new TeamSkill("pass_mixed", Grade.NATIONAL, 0.5f, 0.5f, "mfm_texture_pass_mixed"));
        // Creation du tableau de positions
        _tabTeamPosition = new List<TeamSkill>();
        this._tabTeamPosition.Add(new TeamSkill("position_defense_full", Grade.CONTINENTAL, 0.3f, 0.7f, "mfm_texture_position_ultra_def"));
        this._tabTeamPosition.Add(new TeamSkill("position_defense", Grade.REGIONAL, 0.4f, 0.6f, "mfm_texture_position_def"));
        this._tabTeamPosition.Add(new TeamSkill("position_normal", Grade.REGIONAL, 0.5f, 0.5f, "mfm_texture_position_mid"));
        this._tabTeamPosition.Add(new TeamSkill("position_attack", Grade.NATIONAL, 0.6f, 0.4f, "mfm_texture_position_off"));
        // Creation du tableau de type de pressing
        _tabTeamPressing = new List<TeamSkill>();
        this._tabTeamPressing.Add(new TeamSkill("pressing_defense", Grade.REGIONAL, 0.3f, 0.7f, "mfm_texture_pressing_def"));
        this._tabTeamPressing.Add(new TeamSkill("pressing_mid_field", Grade.NATIONAL, 0.5f, 0.5f, "mfm_texture_pressing_mid"));
        this._tabTeamPressing.Add(new TeamSkill("pressing_all_field", Grade.CONTINENTAL, 0.7f, 0.3f, "mfm_texture_pressing_off"));
        // Set tactics to neutral
        setNormalTacticsIndex(0, 2);
        setAdvancedTacticsIndex(2, 2, 1, 2);
    }

    private void populateTeamMidSkills()
    {
        // Creation du tableau de formations
        _tabTeamFormation = new List<TeamSkill>();
        this._tabTeamFormation.Add(new TeamSkill("formation_442_neutral", Grade.REGIONAL, 0.5f, 0.5f, "mfm_texture_formation_4-4-2_mid"));
        this._tabTeamFormation.Add(new TeamSkill("formation_442_defensive", Grade.REGIONAL, 0.45f, 0.55f, "mfm_texture_formation_4-4-2_def"));
        this._tabTeamFormation.Add(new TeamSkill("formation_442_offensive", Grade.REGIONAL, 0.55f, 0.45f, "mfm_texture_formation_4-4-2_off"));
        this._tabTeamFormation.Add(new TeamSkill("formation_352_neutral", Grade.REGIONAL, 0.5f, 0.5f, "mfm_texture_formation_3-5-2_mid"));
        this._tabTeamFormation.Add(new TeamSkill("formation_352_defensive", Grade.CONTINENTAL, 0.35f, 0.65f, "mfm_texture_formation_3-5-2_def"));
        this._tabTeamFormation.Add(new TeamSkill("formation_352_offensive", Grade.CONTINENTAL, 0.65f, 0.35f, "mfm_texture_formation_3-5-2_off"));
        this._tabTeamFormation.Add(new TeamSkill("formation_451_neutral", Grade.NATIONAL, 0.4f, 0.6f, "mfm_texture_formation_4-5-1_mid"));
        this._tabTeamFormation.Add(new TeamSkill("formation_433_neutral", Grade.NATIONAL, 0.6f, 0.4f, "mfm_texture_formation_4-3-3_mid"));
        // Creation du tableau de type de jeu
        _tabTeamPlay = new List<TeamSkill>();
        this._tabTeamPlay.Add(new TeamSkill("play_direct", Grade.CONTINENTAL, 0.4f, 0.6f, "mfm_texture_play_direct"));
        this._tabTeamPlay.Add(new TeamSkill("play_wing", Grade.NATIONAL, 0.6f, 0.4f, "mfm_texture_play_wings"));
        this._tabTeamPlay.Add(new TeamSkill("play_possess", Grade.REGIONAL, 0.5f, 0.5f, "mfm_texture_play_possession"));
        // Creation du tableau de type de marquage
        _tabTeamMarking = new List<TeamSkill>();
        this._tabTeamMarking.Add(new TeamSkill("marking_individual", Grade.CONTINENTAL, 0.35f, 0.65f, "mfm_texture_marking_individual"));
        this._tabTeamMarking.Add(new TeamSkill("marking_zone", Grade.NATIONAL, 0.65f, 0.35f, "mfm_texture_marking_zone"));
        this._tabTeamMarking.Add(new TeamSkill("marking_mixed", Grade.REGIONAL, 0.5f, 0.5f, "mfm_texture_marking_mixed"));
        // Creation du tableau de type de passes
        _tabTeamPass = new List<TeamSkill>();
        this._tabTeamPass.Add(new TeamSkill("pass_short", Grade.NATIONAL, 0.55f, 0.45f, "mfm_texture_pass_short"));
        this._tabTeamPass.Add(new TeamSkill("pass_long", Grade.CONTINENTAL, 0.45f, 0.55f, "mfm_texture_pass_long"));
        this._tabTeamPass.Add(new TeamSkill("pass_mixed", Grade.REGIONAL, 0.5f, 0.5f, "mfm_texture_pass_mixed"));
        // Creation du tableau de positions
        _tabTeamPosition = new List<TeamSkill>();
        this._tabTeamPosition.Add(new TeamSkill("position_defense", Grade.NATIONAL, 0.4f, 0.6f, "mfm_texture_position_def"));
        this._tabTeamPosition.Add(new TeamSkill("position_normal", Grade.REGIONAL, 0.5f, 0.5f, "mfm_texture_position_mid"));
        this._tabTeamPosition.Add(new TeamSkill("position_attack", Grade.CONTINENTAL, 0.6f, 0.4f, "mfm_texture_position_off"));
        // Creation du tableau de type de pressing
        _tabTeamPressing = new List<TeamSkill>();
        this._tabTeamPressing.Add(new TeamSkill("pressing_defense", Grade.NATIONAL, 0.3f, 0.7f, "mfm_texture_pressing_def"));
        this._tabTeamPressing.Add(new TeamSkill("pressing_mid_field", Grade.REGIONAL, 0.5f, 0.5f, "mfm_texture_pressing_mid"));
        this._tabTeamPressing.Add(new TeamSkill("pressing_all_field", Grade.CONTINENTAL, 0.7f, 0.3f, "mfm_texture_pressing_off"));
        // Set tactics index
        setNormalTacticsIndex(3, 1);
        setAdvancedTacticsIndex(2, 2, 1, 2);
    }

    private void populateTeamOffSkills()
    {
        // Creation du tableau de formations
        _tabTeamFormation = new List<TeamSkill>();
        this._tabTeamFormation.Add(new TeamSkill("formation_442_neutral", Grade.REGIONAL, 0.5f, 0.5f, "mfm_texture_formation_4-4-2_mid"));
        this._tabTeamFormation.Add(new TeamSkill("formation_442_defensive", Grade.REGIONAL, 0.45f, 0.55f, "mfm_texture_formation_4-4-2_def"));
        this._tabTeamFormation.Add(new TeamSkill("formation_442_offensive", Grade.REGIONAL, 0.55f, 0.45f, "mfm_texture_formation_4-4-2_off"));
        this._tabTeamFormation.Add(new TeamSkill("formation_433_neutral", Grade.REGIONAL, 0.6f, 0.4f, "mfm_texture_formation_4-3-3_mid"));
        this._tabTeamFormation.Add(new TeamSkill("formation_433_defensive", Grade.NATIONAL, 0.45f, 0.55f, "mfm_texture_formation_4-3-3_def"));
        this._tabTeamFormation.Add(new TeamSkill("formation_433_offensive", Grade.CONTINENTAL, 0.7f, 0.3f, "mfm_texture_formation_4-3-3_off"));
        this._tabTeamFormation.Add(new TeamSkill("formation_352_neutral", Grade.NATIONAL, 0.5f, 0.5f, "mfm_texture_formation_3-5-2_mid"));
        this._tabTeamFormation.Add(new TeamSkill("formation_352_offensive", Grade.CONTINENTAL, 0.65f, 0.35f, "mfm_texture_formation_3-5-2_off"));
        // Creation du tableau de type de jeu
        _tabTeamPlay = new List<TeamSkill>();
        this._tabTeamPlay.Add(new TeamSkill("play_direct", Grade.NATIONAL, 0.4f, 0.6f, "mfm_texture_play_direct"));
        this._tabTeamPlay.Add(new TeamSkill("play_wing", Grade.REGIONAL, 0.6f, 0.4f, "mfm_texture_play_wings"));
        this._tabTeamPlay.Add(new TeamSkill("play_possess", Grade.CONTINENTAL, 0.5f, 0.5f, "mfm_texture_play_possession"));
        // Creation du tableau de type de marquage
        _tabTeamMarking = new List<TeamSkill>();
        this._tabTeamMarking.Add(new TeamSkill("marking_individual", Grade.CONTINENTAL, 0.35f, 0.65f, "mfm_texture_marking_individual"));
        this._tabTeamMarking.Add(new TeamSkill("marking_zone", Grade.REGIONAL, 0.65f, 0.35f, "mfm_texture_marking_zone"));
        this._tabTeamMarking.Add(new TeamSkill("marking_mixed", Grade.NATIONAL, 0.5f, 0.5f, "mfm_texture_marking_mixed"));
        // Creation du tableau de type de passes
        _tabTeamPass = new List<TeamSkill>();
        this._tabTeamPass.Add(new TeamSkill("pass_short", Grade.REGIONAL, 0.55f, 0.45f, "mfm_texture_pass_short"));
        this._tabTeamPass.Add(new TeamSkill("pass_long", Grade.CONTINENTAL, 0.45f, 0.55f, "mfm_texture_pass_long"));
        this._tabTeamPass.Add(new TeamSkill("pass_mixed", Grade.NATIONAL, 0.5f, 0.5f, "mfm_texture_pass_mixed"));
        // Creation du tableau de positions
        _tabTeamPosition = new List<TeamSkill>();
        this._tabTeamPosition.Add(new TeamSkill("position_defense", Grade.NATIONAL, 0.4f, 0.6f, "mfm_texture_position_def"));
        this._tabTeamPosition.Add(new TeamSkill("position_normal", Grade.REGIONAL, 0.5f, 0.5f, "mfm_texture_position_mid"));
        this._tabTeamPosition.Add(new TeamSkill("position_attack", Grade.REGIONAL, 0.6f, 0.4f, "mfm_texture_position_off"));
        this._tabTeamPosition.Add(new TeamSkill("position_attack_full", Grade.CONTINENTAL, 0.7f, 0.3f, "mfm_texture_position_ultra_off"));
        // Creation du tableau de type de pressing
        _tabTeamPressing = new List<TeamSkill>();
        this._tabTeamPressing.Add(new TeamSkill("pressing_defense", Grade.CONTINENTAL, 0.3f, 0.7f, "mfm_texture_pressing_def"));
        this._tabTeamPressing.Add(new TeamSkill("pressing_mid_field", Grade.NATIONAL, 0.5f, 0.5f, "mfm_texture_pressing_mid"));
        this._tabTeamPressing.Add(new TeamSkill("pressing_all_field", Grade.REGIONAL, 0.7f, 0.3f, "mfm_texture_pressing_off"));
        // Set tactics index
        setNormalTacticsIndex(0, 1);
        setAdvancedTacticsIndex(2, 2, 1, 2);
    }
}