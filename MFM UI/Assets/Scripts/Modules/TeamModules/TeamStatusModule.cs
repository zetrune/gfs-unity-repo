﻿using UnityEngine;
using System.Collections;
using System;

public class TeamStatusModule {

	/**
	 * Variable pour le module de statut d'une equipe
	 */
	public TeamStatus _status;	// Le statut de l'equipe
	public int _statusIndex;	// L'index du status
	
	/**
	 * Constructeur
	 * @param parTeamStatus Le statut initial de l'equipe
	 */
	public TeamStatusModule(TeamStatus parTeamStatus) {
		_status = parTeamStatus;
		_statusIndex = (int)parTeamStatus;
	}
	
	/**
	 * Fonction qui permet d'augmenter le statut global d'une equipe
	 */
	public void upgradeTeamGlobalStatus(){
		if (_statusIndex != 3) {
			_statusIndex++;
			_status = (TeamStatus)_statusIndex;
		}
	}
	
	/**
	 * Fonction qui permet de diminuer le statut global d'une equipe
	 */
	public void downgradeTeamGlobalStatus(){
		if (_statusIndex != 0) {
			_statusIndex--;
			_status = (TeamStatus) _statusIndex;
		}
	}

	public int getBoardObjective() {
		return ConstManager.TAB_TEAM_STATUS_BOARD_OBJECTIVE[_statusIndex];
	}

	public int getHighestRank() {
		return ConstManager.TAB_TEAM_STATUS_RANK_HIGH[_statusIndex];
	}

	public int getLowestRank() {
		return ConstManager.TAB_TEAM_STATUS_RANK_LOW[_statusIndex];
	}
}
