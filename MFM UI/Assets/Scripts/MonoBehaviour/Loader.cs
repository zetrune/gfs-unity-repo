﻿using System;
using UnityEngine;
using System.Collections;

public class Loader : MonoBehaviour {

	public GameObject _gameManager;

	// Use this for initialization
	void Awake () {
		//Check if a GameManager has already been assigned to static variable GameManager._instance or if it's still null
		if (GameManager._instance == null) {
			//Instantiate GameManager prefab
			Instantiate (_gameManager);
		}
	}
}
