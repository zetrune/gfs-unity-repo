﻿using UnityEngine;
using System.Collections;
using Assets.Scripts.DAO.Services;
using Assets.Scripts.DAO.Repository;
using Assets.Scripts.Managers;

public class DIContainer : MonoBehaviour {

    public static IDbManager DbManager {get; private set;}

    public static IGameRepository GameRepository { get; private set; }
    public static IChampionshipRepository ChampionshipRepository { get; private set; }
    public static IJourneyChampRepository JourneyChampRepository { get; private set; }
    public static ITeamChampRepository TeamChampRepository { get; private set; }
    public static ITeamStatsRepository TeamStatsRepository { get; private set; }
    public static ITeamRepository TeamRepository { get; private set; }
    public static IJourneyRepository JourneyRepository { get; private set; }
    public static IMatchJourneyRepository MatchJourneyRepository { get; private set; }
    public static IMatchRepository MatchRepository { get; private set; }
    public static IActionRepository ActionRepository { get; private set; }
    public static IActionMatchRepository ActionMatchRepository { get; private set; }
    public static IPlayerRepository PlayerRepository { get; private set; }
    public static IPlayerMatchRepository PlayerMatchRepository { get; private set; }


    public static IGameService GameService { get; private set; }
    public static IChampionshipService ChampionshipService { get; private set; }
    public static ITeamService TeamService { get; private set; }
    public static IJourneyService JourneyService { get; private set; }
    public static IMatchService MatchService { get; private set; }
    public static IPlayerService PlayerService { get; private set; }


    // Use this for initialization
    void Awake ()
    {
        DbManager = new DbManager();

        GameRepository = new GameRepository(DbManager);
        ChampionshipRepository = new ChampionshipRepository(DbManager);
        JourneyChampRepository = new JourneyChampRepository(DbManager);
        TeamChampRepository = new TeamChampRepository(DbManager);
        TeamStatsRepository = new TeamStatsRepository(DbManager);
        TeamRepository = new TeamRepository(DbManager);
        JourneyRepository = new JourneyRepository(DbManager);
        MatchJourneyRepository = new MatchJourneyRepository(DbManager);
        MatchRepository = new MatchRepository(DbManager);
        ActionRepository = new ActionRepository(DbManager);
        ActionMatchRepository = new ActionMatchRepository(DbManager);
        PlayerRepository = new PlayerRepository(DbManager);
        PlayerMatchRepository = new PlayerMatchRepository(DbManager);


        TeamService = new TeamService(TeamRepository, TeamChampRepository, TeamStatsRepository);
        MatchService = new MatchService(MatchRepository, ActionRepository, ActionMatchRepository);
        JourneyService = new JourneyService(JourneyRepository, MatchService, MatchJourneyRepository);
        ChampionshipService = new ChampionshipService(ChampionshipRepository, JourneyService, JourneyChampRepository, TeamChampRepository, TeamService);
        PlayerService = new PlayerService(PlayerRepository, PlayerMatchRepository, MatchService);
        GameService = new GameService(GameRepository, PlayerService, ChampionshipService);

        DontDestroyOnLoad(gameObject);
    }


}
