﻿using Assets.Scripts.DAO.Services;
using Assets.Scripts.Managers;
using Assets.Scripts.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;


public class GameLoader : MonoBehaviour
{
    void Start()
    {
        var locDbMgr = DIContainer.DbManager;
        locDbMgr.OpenConnection();

        IGameService locGameService = DIContainer.GameService;

        var locGame = locGameService.GetGame();

        locDbMgr.CloseConnection();


        if (locGame != null && locGame.Player != null && locGame.Championship != null)
        {
            GameManager._instance.Game = locGame;
            ChampionshipManager.getInstance()._championship = locGame.Championship;
            Application.LoadLevel("DashboardScene");
        }
        else
        {
            GameManager._instance.Game = new Game { JourneyIndex = 0, Player = new Player(), Season = 2015 };
            Application.LoadLevel("ChampionshipSelectionScene");
        }


    }
}

