﻿using System;
using UnityEngine;
using System.Collections.Generic;

public class ChampionshipManager {
	
	public Championship _championship;

	/** Instance unique pre-initialisee */
	private static ChampionshipManager _instance = new ChampionshipManager();
	
	/** Constructeur prive */
	private ChampionshipManager() {

	}

	public static ChampionshipManager getInstance() {
		return _instance;
	}

	/**
	 * Function to launch all functions at the beginning of a journey
	 * @param parJourneyIndex The current journey
	 */ 
	public void initJourney(int parJourneyIndex) {

		if ((parJourneyIndex != 0) && (parJourneyIndex % ConstManager.STATUS_UPDATE_DAY == 0)) {
			updateTeamStatus(_championship);
		}
	}

	/**
	 * Function to launch all functions at the end of a journey
	 * @param parJourneyIndex The current journey
	 */ 
	public void endJourney(int parJourneyIndex) {
		playJourney (_championship, parJourneyIndex);
        updateTeamRank(_championship, parJourneyIndex == 0);
	}

    public Team GetTeamById(int parTeamId)
    {
        Team locResult = null;
        
        foreach (Team locTmpTeam in _championship._tabFirstLeagueTeams)
        {
            if (locTmpTeam._id == parTeamId)
            {
                locResult = locTmpTeam;
                break;
            }
        }

        return locResult;
    }

    public Match GetCurrentTeamMatch(Team parTeam, int parJourneyIndex)
    {
        List<Match> locTabMatch = _championship._tabJourneysFirstLeague[parJourneyIndex]._tabMatchs;

        Match locResult = null;

        foreach (Match locMatch in locTabMatch)
        {
            if (locMatch._localTeam == parTeam._id || locMatch._visitorTeam == parTeam._id)
            {
                locResult = locMatch;
                break;
            }
        }

        return locResult;
    }

    /**
	 ****************************************************************************************
	 *																						*
	 *----------------------------   Creation functions  -----------------------------------*
	 *																						*
	 ****************************************************************************************
	 */

    /**
	 * Function to create the english championship
	 */
    public void createEnglishChampionship() {
		_championship = new Championship("english_league", "english_flag", createEnglishFirstLeague());
        GameManager._instance.SetChampionship(_championship);
		_championship._tabJourneysFirstLeague = CalendarManager.getInstance().createChampionshipJourney (_championship._tabFirstLeagueTeams);
	}

    /**
	 * Function to create the french championship
	 */
    public void createFrenchChampionship() {
		_championship = new Championship("french_league", "french_flag", createFrenchFirstLeague());
        GameManager._instance.SetChampionship(_championship);
        _championship._tabJourneysFirstLeague = CalendarManager.getInstance().createChampionshipJourney (_championship._tabFirstLeagueTeams);
	}
	
	/**
	 * Function to create the german championship
	 */ 
	public void createGermanChampionship() {
		_championship = new Championship("german_league", "german_flag", createGermanFirstLeague());
        GameManager._instance.SetChampionship(_championship);
        _championship._tabJourneysFirstLeague = CalendarManager.getInstance().createChampionshipJourney (_championship._tabFirstLeagueTeams);
	}
	
	/**
	 * Function to create the italian championship
	 */ 
	public void createItalianChampionship() {
		_championship = new Championship("italian_league", "italian_flag", createItalianFirstLeague());
		_championship._tabJourneysFirstLeague = CalendarManager.getInstance().createChampionshipJourney (_championship._tabFirstLeagueTeams);
	}
	
	/**
	 * Function to create the spanish championship
	 */ 
	public void createSpanishChampionship() {
		_championship = new Championship("spanish_league", "spanish_flag", createSpanishFirstLeague());
        GameManager._instance.SetChampionship(_championship);
        _championship._tabJourneysFirstLeague = CalendarManager.getInstance().createChampionshipJourney (_championship._tabFirstLeagueTeams);
	}

	/**
	 ****************************************************************************************
	 *																						*
	 *-----------------------------   Private functions  -----------------------------------*
	 *																						*
	 ****************************************************************************************
	 */

	/**
	 * Fonction qui joue une  journéee d'un championnat et met à jour le status des équipes
	 * @param parChampionship The concerned championship
	 */
	private void playJourney(Championship parChampionship, int parJourneyIndex) {
		parChampionship._tabJourneysFirstLeague[parJourneyIndex].playJourney();
	}

	/**
	 * Fonction qui permet de mettre a jour le statut d'une equipe en fonction du son classement
	 * @param parChampionship The concerned championship
	 */
	private void updateTeamStatus(Championship parChampionship) {
		for (int i = 0; i < parChampionship._tabFirstLeagueTeams.Count; i++) {
			parChampionship._tabFirstLeagueTeams[i].updateTeamStatus();
			parChampionship._tabFirstLeagueTeams[i].updateBoardTrust();
		}
	}
	
	/**
	 * Function team rank for all teams of a championship
	 * @param parChampionship The target championship
	 * @param parIsInit Boolean to indicate if it is the first journey
	 */
	private void updateTeamRank(Championship parChampionship, bool parIsInit) {
		parChampionship.updateTeamRank (parIsInit);
	}

	/** Function to set the teams of the first and second league for the next championship
	 * @param parChampionship The target championship
	 */ 
	private void nextChampionship(Championship parChampionship) {
		updateTeamStatus(parChampionship);
		parChampionship.resetChampionship();
		Team locTmpTeam;
		List<Team> locTmpTabTeam = new List<Team>();
		int locTmpTeamPos;
		
		// Retrieving downgraded teams
		for (int i = 0; i < 3; i++) {
			locTmpTeamPos = parChampionship._tabFirstLeagueTeams.Count - 1; // Last team of the tab
			locTmpTeam = parChampionship._tabFirstLeagueTeams[locTmpTeamPos];
			locTmpTeam._teamStatus._status = TeamStatus.FAV;
			locTmpTeam._isProtected = true;
			locTmpTabTeam.Add(locTmpTeam);
			parChampionship._tabFirstLeagueTeams.RemoveAt(locTmpTeamPos);	// Removing last team
		}
		// Retrieving upgraded teams
		for (int i = 0; i < 3; i++) {
			locTmpTeam = parChampionship._tabSecondLeagueTeams[0];
			locTmpTeam._teamStatus._status = TeamStatus.LOW;
			locTmpTeam._isProtected = false;
			parChampionship._tabFirstLeagueTeams.Add(locTmpTeam);
			parChampionship._tabSecondLeagueTeams.RemoveAt(0);
		}
		// Update of the second league
		parChampionship.setAllTeamStatus(parChampionship._tabSecondLeagueTeams, TeamStatus.LOW);
		for (int i = 0; i < locTmpTabTeam.Count; i++) {
			locTmpTeam = locTmpTabTeam[i];
			parChampionship._tabSecondLeagueTeams.Add(locTmpTeam);
		}
		parChampionship.resetChampionship ();
		parChampionship._tabJourneysFirstLeague = CalendarManager.getInstance().createChampionshipJourney(parChampionship._tabFirstLeagueTeams);
	}

	/**
	 * Function that create a list of team for the English first league
	 * @return the English first league
	 */
	private List<Team> createEnglishFirstLeague() {
		List<Team> locEnglishFirstLeague = new List<Team>();
		Team locTeamArsenal = new Team("Arsenal", new TeamStatusModule(TeamStatus.FAV), new TeamSkillsModule(TeamType.MID), new Color32(220,32,35,255), new Color32(255,255,255,255));locEnglishFirstLeague.Add(locTeamArsenal);
		Team locTeamAstonVilla = new Team("Aston Villa", new TeamStatusModule(TeamStatus.MID), new TeamSkillsModule(TeamType.DEF), new Color32(120,0,70,255), new Color32(165,200,235,255));locEnglishFirstLeague.Add(locTeamAstonVilla);
		Team locTeamBurnley = new Team("Burnley", new TeamStatusModule(TeamStatus.MID), new TeamSkillsModule(TeamType.MID), new Color32(115,40,75,255), new Color32(189,228,247,255));locEnglishFirstLeague.Add(locTeamBurnley);
		Team locTeamChelsea = new Team("Chelsea", new TeamStatusModule(TeamStatus.FAV), new TeamSkillsModule(TeamType.DEF), new Color32(2,58,180,255), new Color32(255,255,255,255));locEnglishFirstLeague.Add(locTeamChelsea);
		Team locTeamCrystalPalace = new Team("Crystal Palace", new TeamStatusModule(TeamStatus.MID), new TeamSkillsModule(TeamType.OFF), new Color32(0,73,144,255), new Color32(195,16,46,255));locEnglishFirstLeague.Add(locTeamCrystalPalace);
		Team locTeamEverton = new Team("Everton", new TeamStatusModule(TeamStatus.OUT), new TeamSkillsModule(TeamType.OFF), new Color32(0,79,157,255), new Color32(255,255,255,255));locEnglishFirstLeague.Add(locTeamEverton);
		Team locTeamHullCity = new Team("Hull City", new TeamStatusModule(TeamStatus.MID), new TeamSkillsModule(TeamType.MID), new Color32(255,121,0,255), new Color32(0,0,0,255));locEnglishFirstLeague.Add(locTeamHullCity);
		Team locTeamLeicesterCity = new Team("Leicester City", new TeamStatusModule(TeamStatus.MID), new TeamSkillsModule(TeamType.OFF), new Color32(0,82,159,255), new Color32(255,255,255,255));locEnglishFirstLeague.Add(locTeamLeicesterCity);
		Team locTeamLiverpool = new Team("Liverpool", new TeamStatusModule(TeamStatus.FAV), new TeamSkillsModule(TeamType.OFF), new Color32(208,0,39,255), new Color32(255,255,255,255));locEnglishFirstLeague.Add(locTeamLiverpool);
		Team locTeamManchesterCity = new Team("Manchester City", new TeamStatusModule(TeamStatus.FAV), new TeamSkillsModule(TeamType.OFF), new Color32(60,185,240,255), new Color32(0,0,0,255));locEnglishFirstLeague.Add(locTeamManchesterCity);
		Team locTeamManchesterUnited = new Team("Manchester United", new TeamStatusModule(TeamStatus.FAV), new TeamSkillsModule(TeamType.MID), new Color32(230,15,12,255), new Color32(255,255,255,255));locEnglishFirstLeague.Add(locTeamManchesterUnited);
		Team locTeamNewcastleUnited = new Team("Newcastle United", new TeamStatusModule(TeamStatus.OUT), new TeamSkillsModule(TeamType.DEF), new Color32(19,17,17,255), new Color32(255,255,255,255));locEnglishFirstLeague.Add(locTeamNewcastleUnited);
		Team locTeamSouthampton = new Team("Southampton", new TeamStatusModule(TeamStatus.OUT), new TeamSkillsModule(TeamType.OFF), new Color32(225,18,44,255), new Color32(255,255,255,255));locEnglishFirstLeague.Add(locTeamSouthampton);
		Team locTeamStokeCity = new Team("Stoke City", new TeamStatusModule(TeamStatus.OUT), new TeamSkillsModule(TeamType.DEF), new Color32(244,38,43,255), new Color32(255,255,255,255));locEnglishFirstLeague.Add(locTeamStokeCity);
		Team locTeamSunderland = new Team("Sunderland", new TeamStatusModule(TeamStatus.MID), new TeamSkillsModule(TeamType.MID), new Color32(235,23,43,255), new Color32(0,0,0,255));locEnglishFirstLeague.Add(locTeamSunderland);
		Team locTeamSwanseaCity = new Team("Swansea City", new TeamStatusModule(TeamStatus.MID), new TeamSkillsModule(TeamType.OFF), new Color32(255,255,255,255), new Color32(0,0,0,255));locEnglishFirstLeague.Add(locTeamSwanseaCity);
		Team locTeamTottenhamHotspur = new Team("Tottenham Hotspur", new TeamStatusModule(TeamStatus.OUT), new TeamSkillsModule(TeamType.MID), new Color32(255,255,255,255), new Color32(0,28,88,255));locEnglishFirstLeague.Add(locTeamTottenhamHotspur);
		Team locTeamWestBromwichAlbion = new Team("West Bromwich Albion", new TeamStatusModule(TeamStatus.MID), new TeamSkillsModule(TeamType.MID), new Color32(255,255,255,255), new Color32(9,20,83,255));locEnglishFirstLeague.Add(locTeamWestBromwichAlbion);
		Team locTeamWestHamUnited = new Team("West Ham United", new TeamStatusModule(TeamStatus.MID), new TeamSkillsModule(TeamType.DEF), new Color32(110,7,27,255), new Color32(84,189,249,255));
		locEnglishFirstLeague.Add(locTeamWestHamUnited);
		return locEnglishFirstLeague;
	}
	
	/**
	 * Function that create a list of team for the English second league
	 * @return the English seconde league
	 */
	private List<Team> createEnglishSecondLeague() {
		List<Team> locEnglishSecondLeague = new List<Team>();

		Team locTeamBarnsley = new Team("Barnsley", new TeamStatusModule(TeamStatus.LOW), new TeamSkillsModule(TeamType.MID), new Color32(255,255,255,255), new Color32(255,255,255,255));locEnglishSecondLeague.Add(locTeamBarnsley);
		Team locTeamBirminghamCity = new Team("Birmingham City", new TeamStatusModule(TeamStatus.LOW), new TeamSkillsModule(TeamType.DEF), new Color32(255,255,255,255), new Color32(255,255,255,255));locEnglishSecondLeague.Add(locTeamBirminghamCity);
		Team locTeamBlackburnRovers = new Team("Blackburn Rovers", new TeamStatusModule(TeamStatus.LOW), new TeamSkillsModule(TeamType.OFF), new Color32(255,255,255,255), new Color32(255,255,255,255));locEnglishSecondLeague.Add(locTeamBlackburnRovers);
		Team locTeamBlackpool = new Team("Blackpool", new TeamStatusModule(TeamStatus.LOW), new TeamSkillsModule(TeamType.MID), new Color32(255,255,255,255), new Color32(255,255,255,255));locEnglishSecondLeague.Add(locTeamBlackpool);
		Team locTeamBoltonWanderers = new Team("Bolton Wanderers", new TeamStatusModule(TeamStatus.LOW), new TeamSkillsModule(TeamType.MID), new Color32(255,255,255,255), new Color32(255,255,255,255));locEnglishSecondLeague.Add(locTeamBoltonWanderers);
		Team locTeamBournemouth = new Team("Bournemouth", new TeamStatusModule(TeamStatus.LOW), new TeamSkillsModule(TeamType.MID), new Color32(255,255,255,255), new Color32(255,255,255,255));locEnglishSecondLeague.Add(locTeamBournemouth);
		Team locTeamBrightonHoveAlbion = new Team("Brighton Hove Albion", new TeamStatusModule(TeamStatus.LOW), new TeamSkillsModule(TeamType.DEF), new Color32(255,255,255,255), new Color32(255,255,255,255));locEnglishSecondLeague.Add(locTeamBrightonHoveAlbion);
		Team locTeamCardiffCity = new Team("Cardiff City", new TeamStatusModule(TeamStatus.LOW), new TeamSkillsModule(TeamType.DEF), new Color32(255,255,255,255), new Color32(255,255,255,255));locEnglishSecondLeague.Add(locTeamCardiffCity);
		Team locTeamCharltonAthletic = new Team("Charlton Athletic", new TeamStatusModule(TeamStatus.LOW), new TeamSkillsModule(TeamType.DEF), new Color32(255,255,255,255), new Color32(255,255,255,255));locEnglishSecondLeague.Add(locTeamCharltonAthletic);
		Team locTeamDerbyCounty = new Team("Derby County", new TeamStatusModule(TeamStatus.LOW), new TeamSkillsModule(TeamType.OFF), new Color32(255,255,255,255), new Color32(255,255,255,255));locEnglishSecondLeague.Add(locTeamDerbyCounty);
		Team locTeamDoncasterRovers = new Team("Doncaster Rovers", new TeamStatusModule(TeamStatus.LOW), new TeamSkillsModule(TeamType.DEF), new Color32(255,255,255,255), new Color32(255,255,255,255));locEnglishSecondLeague.Add(locTeamDoncasterRovers);
		Team locTeamFulham = new Team("Fulham", new TeamStatusModule(TeamStatus.LOW), new TeamSkillsModule(TeamType.DEF), new Color32(255,255,255,255), new Color32(255,255,255,255));locEnglishSecondLeague.Add(locTeamFulham);
		Team locTeamHuddersfieldTown = new Team("Huddersfield Town", new TeamStatusModule(TeamStatus.LOW), new TeamSkillsModule(TeamType.MID), new Color32(255,255,255,255), new Color32(255,255,255,255));locEnglishSecondLeague.Add(locTeamHuddersfieldTown);
		Team locTeamIpswichTown = new Team("Ipswich Town", new TeamStatusModule(TeamStatus.LOW), new TeamSkillsModule(TeamType.MID), new Color32(255,255,255,255), new Color32(255,255,255,255));locEnglishSecondLeague.Add(locTeamIpswichTown);
		Team locTeamLeedsUnited = new Team("Leeds United", new TeamStatusModule(TeamStatus.LOW), new TeamSkillsModule(TeamType.MID), new Color32(255,255,255,255), new Color32(255,255,255,255));locEnglishSecondLeague.Add(locTeamLeedsUnited);
		Team locTeamMiddlesbrough = new Team("Middlesbrough", new TeamStatusModule(TeamStatus.LOW), new TeamSkillsModule(TeamType.DEF), new Color32(255,255,255,255), new Color32(255,255,255,255));locEnglishSecondLeague.Add(locTeamMiddlesbrough);
		Team locTeamMillwall = new Team("Millwall", new TeamStatusModule(TeamStatus.LOW), new TeamSkillsModule(TeamType.DEF), new Color32(255,255,255,255), new Color32(255,255,255,255));locEnglishSecondLeague.Add(locTeamMillwall);
		Team locTeamNorwichCity = new Team("Norwich City", new TeamStatusModule(TeamStatus.LOW), new TeamSkillsModule(TeamType.MID), new Color32(255,255,255,255), new Color32(255,255,255,255));locEnglishSecondLeague.Add(locTeamNorwichCity);
		Team locTeamNottinghamForest = new Team("Nottingham Forest", new TeamStatusModule(TeamStatus.LOW), new TeamSkillsModule(TeamType.MID), new Color32(255,255,255,255), new Color32(255,255,255,255));locEnglishSecondLeague.Add(locTeamNottinghamForest);
		Team locTeamQueensParkRangers = new Team("Queens Park Rangers", new TeamStatusModule(TeamStatus.LOW), new TeamSkillsModule(TeamType.MID), new Color32(0,45,160,255), new Color32(255,255,255,255));locEnglishSecondLeague.Add(locTeamQueensParkRangers);
		Team locTeamReading = new Team("Reading", new TeamStatusModule(TeamStatus.LOW), new TeamSkillsModule(TeamType.OFF), new Color32(255,255,255,255), new Color32(255,255,255,255));locEnglishSecondLeague.Add(locTeamReading);
		Team locTeamSheffieldWednesday = new Team("Sheffield Wednesday", new TeamStatusModule(TeamStatus.LOW), new TeamSkillsModule(TeamType.MID), new Color32(255,255,255,255), new Color32(255,255,255,255));locEnglishSecondLeague.Add(locTeamSheffieldWednesday);
		Team locTeamWatford = new Team("Watford", new TeamStatusModule(TeamStatus.LOW), new TeamSkillsModule(TeamType.OFF), new Color32(255,255,255,255), new Color32(255,255,255,255));locEnglishSecondLeague.Add(locTeamWatford);
		Team locTeamWiganAthletic = new Team("Wigan Athletic", new TeamStatusModule(TeamStatus.LOW), new TeamSkillsModule(TeamType.OFF), new Color32(255,255,255,255), new Color32(255,255,255,255));locEnglishSecondLeague.Add(locTeamWiganAthletic);
		Team locTeamYeovilTown = new Team("Yeovil Town", new TeamStatusModule(TeamStatus.LOW), new TeamSkillsModule(TeamType.DEF), new Color32(255,255,255,255), new Color32(255,255,255,255));locEnglishSecondLeague.Add(locTeamYeovilTown);

		return locEnglishSecondLeague;
	}
	
	/**
	 * Function that create a list of team for the French first league
	 * @return the French first league
	 */
	private List<Team> createFrenchFirstLeague() {
		List<Team> locFrenchFirstLeague = new List<Team>();

		Team locTeamASSaintEtienne = new Team("AS Saint Etienne", new TeamStatusModule(TeamStatus.OUT), new TeamSkillsModule(TeamType.MID), new Color32(0,150,70,255), new Color32(255,255,255,255));locFrenchFirstLeague.Add(locTeamASSaintEtienne);
		Team locTeamBastia = new Team("Bastia", new TeamStatusModule(TeamStatus.MID), new TeamSkillsModule(TeamType.DEF), new Color32(0,70,137,255), new Color32(255,255,255,255));locFrenchFirstLeague.Add(locTeamBastia);
		Team locTeamEnAvantGuingamp = new Team("En Avant Guingamp", new TeamStatusModule(TeamStatus.MID), new TeamSkillsModule(TeamType.OFF), new Color32(237,28,36,255), new Color32(0,0,0,255));locFrenchFirstLeague.Add(locTeamEnAvantGuingamp);
		Team locTeamEvianTG = new Team("Evian TG", new TeamStatusModule(TeamStatus.MID), new TeamSkillsModule(TeamType.OFF), new Color32(230,145,200,255), new Color32(0,0,0,255));locFrenchFirstLeague.Add(locTeamEvianTG);
		Team locTeamFCLorient = new Team("FC Lorient", new TeamStatusModule(TeamStatus.OUT), new TeamSkillsModule(TeamType.MID), new Color32(255,116,2,255), new Color32(0,0,0,255));locFrenchFirstLeague.Add(locTeamFCLorient);
		Team locTeamFCMetz = new Team("FC Metz", new TeamStatusModule(TeamStatus.MID), new TeamSkillsModule(TeamType.OFF), new Color32(176,6,58,255), new Color32(255,255,255,255));locFrenchFirstLeague.Add(locTeamFCMetz);
		Team locTeamGirondinsDeBordeaux = new Team("Girondins De Bordeaux", new TeamStatusModule(TeamStatus.OUT), new TeamSkillsModule(TeamType.DEF), new Color32(0,27,80,255), new Color32(255,255,255,255));locFrenchFirstLeague.Add(locTeamGirondinsDeBordeaux);
		Team locTeamLilleOSC = new Team("Lille OSC", new TeamStatusModule(TeamStatus.FAV), new TeamSkillsModule(TeamType.DEF), new Color32(202,2,29,255), new Color32(255,255,255,255));locFrenchFirstLeague.Add(locTeamLilleOSC);
		Team locTeamMonaco = new Team("Monaco", new TeamStatusModule(TeamStatus.FAV), new TeamSkillsModule(TeamType.MID), new Color32(235,15,25,255), new Color32(255,255,255,255));locFrenchFirstLeague.Add(locTeamMonaco);
		Team locTeamMontpellierHerault = new Team("Montpellier Herault", new TeamStatusModule(TeamStatus.MID), new TeamSkillsModule(TeamType.OFF), new Color32(15,35,175,255), new Color32(250,110,10,255));locFrenchFirstLeague.Add(locTeamMontpellierHerault);
		Team locTeamNantes = new Team("Nantes", new TeamStatusModule(TeamStatus.MID), new TeamSkillsModule(TeamType.DEF), new Color32(255,225,0,255), new Color32(5,125,35,255));locFrenchFirstLeague.Add(locTeamNantes);
		Team locTeamOGCNice = new Team("OGC Nice", new TeamStatusModule(TeamStatus.MID), new TeamSkillsModule(TeamType.DEF), new Color32(217,35,35,255), new Color32(30,30,30,255));locFrenchFirstLeague.Add(locTeamOGCNice);
		Team locTeamOlympiqueDeMarseille = new Team("Olympique De Marseille", new TeamStatusModule(TeamStatus.FAV), new TeamSkillsModule(TeamType.OFF), new Color32(0,160,235,255), new Color32(255,255,255,255));locFrenchFirstLeague.Add(locTeamOlympiqueDeMarseille);
		Team locTeamOlympiqueLyonnais = new Team("Olympique Lyonnais", new TeamStatusModule(TeamStatus.FAV), new TeamSkillsModule(TeamType.OFF), new Color32(13,35,150,255), new Color32(235,0,15,255));locFrenchFirstLeague.Add(locTeamOlympiqueLyonnais);
		Team locTeamParisSaintGermain = new Team("Paris Saint Germain", new TeamStatusModule(TeamStatus.FAV), new TeamSkillsModule(TeamType.MID), new Color32(0,45,125,255), new Color32(245,45,55,255));locFrenchFirstLeague.Add(locTeamParisSaintGermain);
		Team locTeamRCLens = new Team("RC Lens", new TeamStatusModule(TeamStatus.MID), new TeamSkillsModule(TeamType.MID), new Color32(167,26,27,255), new Color32(245,210,15,255));locFrenchFirstLeague.Add(locTeamRCLens);
		Team locTeamSMCaen = new Team("SM Caen", new TeamStatusModule(TeamStatus.MID), new TeamSkillsModule(TeamType.OFF), new Color32(2,45,130,255), new Color32(215,23,31,255));locFrenchFirstLeague.Add(locTeamSMCaen);
		Team locTeamStadeDeReims = new Team("Stade De Reims", new TeamStatusModule(TeamStatus.MID), new TeamSkillsModule(TeamType.OFF), new Color32(229,0,51,255), new Color32(255,255,255,255));locFrenchFirstLeague.Add(locTeamStadeDeReims);
		Team locTeamStadeRennais = new Team("Stade Rennais", new TeamStatusModule(TeamStatus.OUT), new TeamSkillsModule(TeamType.DEF), new Color32(244,13,29,255), new Color32(0,0,0,255));locFrenchFirstLeague.Add(locTeamStadeRennais);
		Team locTeamToulouseFC = new Team("Toulouse FC", new TeamStatusModule(TeamStatus.OUT), new TeamSkillsModule(TeamType.MID), new Color32(90,60,195,255), new Color32(255,255,255,255));locFrenchFirstLeague.Add(locTeamToulouseFC);

		return locFrenchFirstLeague;
	}
	
	/**
	 * Function that create a list of team for the French second league
	 * @return the French second league
	 */
	private List<Team> createFrenchSecondLeague() {
		List<Team> locFrenchSecondLeague = new List<Team>();

		Team locTeamACAjaccio = new Team("AC Ajaccio", new TeamStatusModule(TeamStatus.LOW), new TeamSkillsModule(TeamType.DEF), new Color32(255,255,255,255), new Color32(255,255,255,255));locFrenchSecondLeague.Add(locTeamACAjaccio);
		Team locTeamACArlesAvignon = new Team("AC Arles Avignon", new TeamStatusModule(TeamStatus.LOW), new TeamSkillsModule(TeamType.DEF), new Color32(255,255,255,255), new Color32(255,255,255,255));locFrenchSecondLeague.Add(locTeamACArlesAvignon);
		Team locTeamAJAuxerre = new Team("AJ Auxerre", new TeamStatusModule(TeamStatus.LOW), new TeamSkillsModule(TeamType.OFF), new Color32(255,255,255,255), new Color32(255,255,255,255));locFrenchSecondLeague.Add(locTeamAJAuxerre);
		Team locTeamAngersSCO = new Team("Angers SCO", new TeamStatusModule(TeamStatus.LOW), new TeamSkillsModule(TeamType.MID), new Color32(255,255,255,255), new Color32(255,255,255,255));locFrenchSecondLeague.Add(locTeamAngersSCO);
		Team locTeamASNancyLorraine = new Team("AS Nancy Lorraine", new TeamStatusModule(TeamStatus.LOW), new TeamSkillsModule(TeamType.OFF), new Color32(255,255,255,255), new Color32(255,255,255,255));locFrenchSecondLeague.Add(locTeamASNancyLorraine);
		Team locTeamCABastia = new Team("CA Bastia", new TeamStatusModule(TeamStatus.LOW), new TeamSkillsModule(TeamType.DEF), new Color32(255,255,255,255), new Color32(255,255,255,255));locFrenchSecondLeague.Add(locTeamCABastia);
		Team locTeamChamoisNiortais = new Team("Chamois Niortais", new TeamStatusModule(TeamStatus.LOW), new TeamSkillsModule(TeamType.DEF), new Color32(255,255,255,255), new Color32(255,255,255,255));locFrenchSecondLeague.Add(locTeamChamoisNiortais);
		Team locTeamChateauroux = new Team("Chateauroux", new TeamStatusModule(TeamStatus.LOW), new TeamSkillsModule(TeamType.OFF), new Color32(255,255,255,255), new Color32(255,255,255,255));locFrenchSecondLeague.Add(locTeamChateauroux);
		Team locTeamClermontFoot = new Team("Clermont Foot", new TeamStatusModule(TeamStatus.LOW), new TeamSkillsModule(TeamType.DEF), new Color32(255,255,255,255), new Color32(255,255,255,255));locFrenchSecondLeague.Add(locTeamClermontFoot);
		Team locTeamDijonFCO = new Team("Dijon FCO", new TeamStatusModule(TeamStatus.LOW), new TeamSkillsModule(TeamType.DEF), new Color32(255,255,255,255), new Color32(255,255,255,255));locFrenchSecondLeague.Add(locTeamDijonFCO);
		Team locTeamESTACTroyes = new Team("ESTAC Troyes", new TeamStatusModule(TeamStatus.LOW), new TeamSkillsModule(TeamType.MID), new Color32(255,255,255,255), new Color32(255,255,255,255));locFrenchSecondLeague.Add(locTeamESTACTroyes);
		Team locTeamFCIstres = new Team("FC Istres", new TeamStatusModule(TeamStatus.LOW), new TeamSkillsModule(TeamType.DEF), new Color32(255,255,255,255), new Color32(255,255,255,255));locFrenchSecondLeague.Add(locTeamFCIstres);
		Team locTeamFCSochaux = new Team("FC Sochaux", new TeamStatusModule(TeamStatus.LOW), new TeamSkillsModule(TeamType.MID), new Color32(255,255,255,255), new Color32(255,255,255,255));locFrenchSecondLeague.Add(locTeamFCSochaux);
		Team locTeamHavreAC = new Team("Havre AC", new TeamStatusModule(TeamStatus.LOW), new TeamSkillsModule(TeamType.DEF), new Color32(255,255,255,255), new Color32(255,255,255,255));locFrenchSecondLeague.Add(locTeamHavreAC);
		Team locTeamNimesOlympique = new Team("Nimes Olympique", new TeamStatusModule(TeamStatus.LOW), new TeamSkillsModule(TeamType.MID), new Color32(255,255,255,255), new Color32(255,255,255,255));locFrenchSecondLeague.Add(locTeamNimesOlympique);
		Team locTeamStadeBrestois29 = new Team("Stade Brestois 29", new TeamStatusModule(TeamStatus.LOW), new TeamSkillsModule(TeamType.OFF), new Color32(255,255,255,255), new Color32(255,255,255,255));locFrenchSecondLeague.Add(locTeamStadeBrestois29);
		Team locTeamStadeLavallois = new Team("Stade Lavallois", new TeamStatusModule(TeamStatus.LOW), new TeamSkillsModule(TeamType.MID), new Color32(255,255,255,255), new Color32(255,255,255,255));locFrenchSecondLeague.Add(locTeamStadeLavallois);
		Team locTeamToursFC = new Team("Tours FC", new TeamStatusModule(TeamStatus.LOW), new TeamSkillsModule(TeamType.MID), new Color32(255,255,255,255), new Color32(255,255,255,255));locFrenchSecondLeague.Add(locTeamToursFC);
		Team locTeamUSCreteil = new Team("US Creteil", new TeamStatusModule(TeamStatus.LOW), new TeamSkillsModule(TeamType.OFF), new Color32(255,255,255,255), new Color32(255,255,255,255));locFrenchSecondLeague.Add(locTeamUSCreteil);
		Team locTeamValenciennesFC = new Team("Valenciennes FC", new TeamStatusModule(TeamStatus.LOW), new TeamSkillsModule(TeamType.OFF), new Color32(255,255,255,255), new Color32(255,255,255,255));locFrenchSecondLeague.Add(locTeamValenciennesFC);

		return locFrenchSecondLeague;
	}
	
	/**
	 * Function that create a list of team for the German first league
	 * @return the German first league
	 */
	private List<Team> createGermanFirstLeague() {
		List<Team> locGermanFirstLeague = new List<Team>();

		Team locTeamBayerLeverkusen = new Team("Bayer Leverkusen", new TeamStatusModule(TeamStatus.FAV), new TeamSkillsModule(TeamType.OFF), new Color32(226,0,26,255), new Color32(0,0,0,255));locGermanFirstLeague.Add(locTeamBayerLeverkusen);
		Team locTeamBayernMunich = new Team("Bayern Munich", new TeamStatusModule(TeamStatus.FAV), new TeamSkillsModule(TeamType.MID), new Color32(240,0,45,255), new Color32(31,134,236,255));locGermanFirstLeague.Add(locTeamBayernMunich);
		Team locTeamBorussiaDortmund = new Team("Borussia Dortmund", new TeamStatusModule(TeamStatus.FAV), new TeamSkillsModule(TeamType.OFF), new Color32(251,237,0,255), new Color32(0,0,0,255));locGermanFirstLeague.Add(locTeamBorussiaDortmund);
		Team locTeamBorussiaMonchengladbach = new Team("Borussia Monchengladbach", new TeamStatusModule(TeamStatus.OUT), new TeamSkillsModule(TeamType.OFF), new Color32(255,255,255,255), new Color32(49,201,100,255));locGermanFirstLeague.Add(locTeamBorussiaMonchengladbach);
		Team locTeamEintrachtFrancfort = new Team("Eintracht Francfort", new TeamStatusModule(TeamStatus.MID), new TeamSkillsModule(TeamType.MID), new Color32(220,0,0,255), new Color32(0,0,0,255));locGermanFirstLeague.Add(locTeamEintrachtFrancfort);
		Team locTeamFCAugsbourg = new Team("FC Augsbourg", new TeamStatusModule(TeamStatus.OUT), new TeamSkillsModule(TeamType.DEF), new Color32(255,255,255,255), new Color32(193,28,25,255));locGermanFirstLeague.Add(locTeamFCAugsbourg);
		Team locTeamFCCologne = new Team("FC Cologne", new TeamStatusModule(TeamStatus.MID), new TeamSkillsModule(TeamType.MID), new Color32(237,28,35,255), new Color32(255,255,255,255));locGermanFirstLeague.Add(locTeamFCCologne);
		Team locTeamFSVMayence05 = new Team("FSV Mayence 05", new TeamStatusModule(TeamStatus.MID), new TeamSkillsModule(TeamType.DEF), new Color32(255,0,0,255), new Color32(255,255,255,255));locGermanFirstLeague.Add(locTeamFSVMayence05);
		Team locTeamHambourgSV = new Team("Hambourg SV", new TeamStatusModule(TeamStatus.MID), new TeamSkillsModule(TeamType.MID), new Color32(255,255,255,255), new Color32(6,69,142,255));locGermanFirstLeague.Add(locTeamHambourgSV);
		Team locTeamHanovre96 = new Team("Hanovre 96", new TeamStatusModule(TeamStatus.MID), new TeamSkillsModule(TeamType.OFF), new Color32(124,41,54,255), new Color32(5,116,74,255));locGermanFirstLeague.Add(locTeamHanovre96);
		Team locTeamHerthaBSC = new Team("Hertha BSC", new TeamStatusModule(TeamStatus.OUT), new TeamSkillsModule(TeamType.DEF), new Color32(0,103,189,255), new Color32(255,255,255,255));locGermanFirstLeague.Add(locTeamHerthaBSC);
		Team locTeamSCFribourg = new Team("SC Fribourg", new TeamStatusModule(TeamStatus.MID), new TeamSkillsModule(TeamType.DEF), new Color32(225,0,62,255), new Color32(255,255,255,255));locGermanFirstLeague.Add(locTeamSCFribourg);
		Team locTeamSCPaderborn07 = new Team("SC Paderborn 07", new TeamStatusModule(TeamStatus.MID), new TeamSkillsModule(TeamType.MID), new Color32(0,0,0,255), new Color32(0,101,165,255));locGermanFirstLeague.Add(locTeamSCPaderborn07);
		Team locTeamSchalke04 = new Team("Schalke 04", new TeamStatusModule(TeamStatus.FAV), new TeamSkillsModule(TeamType.DEF), new Color32(0,64,183,255), new Color32(255,255,255,255));locGermanFirstLeague.Add(locTeamSchalke04);
		Team locTeamTSG1899Hoffenheim = new Team("TSG 1899 Hoffenheim", new TeamStatusModule(TeamStatus.OUT), new TeamSkillsModule(TeamType.OFF), new Color32(0,85,158,255), new Color32(255,255,255,255));locGermanFirstLeague.Add(locTeamTSG1899Hoffenheim);
		Team locTeamVfBStuttgart = new Team("VfB Stuttgart", new TeamStatusModule(TeamStatus.MID), new TeamSkillsModule(TeamType.MID), new Color32(255,255,255,255), new Color32(253,2,2,255));locGermanFirstLeague.Add(locTeamVfBStuttgart);
		Team locTeamVfLWolfsbourg = new Team("VfL Wolfsbourg", new TeamStatusModule(TeamStatus.FAV), new TeamSkillsModule(TeamType.OFF), new Color32(0,183,70,255), new Color32(255,255,255,255));locGermanFirstLeague.Add(locTeamVfLWolfsbourg);
		Team locTeamWerderBreme = new Team("Werder Breme", new TeamStatusModule(TeamStatus.OUT), new TeamSkillsModule(TeamType.DEF), new Color32(13,170,85,255), new Color32(255,255,255,255));locGermanFirstLeague.Add(locTeamWerderBreme);

		return locGermanFirstLeague; 
	}
	
	/**
	 * Function that create a list of team for the German second league
	 * @return the German second league
	 */
	private List<Team> createGermanSecondLeague() {
		List<Team> locGermanSecondLeague = new List<Team>();

		Team locTeamDSCArminiaBielefeld = new Team("DSC Arminia Bielefeld", new TeamStatusModule(TeamStatus.LOW), new TeamSkillsModule(TeamType.DEF), new Color32(255,255,255,255), new Color32(255,255,255,255));locGermanSecondLeague.Add(locTeamDSCArminiaBielefeld);
		Team locTeamEintrachtBrunswick = new Team("Eintracht Brunswick", new TeamStatusModule(TeamStatus.LOW), new TeamSkillsModule(TeamType.OFF), new Color32(255,255,255,255), new Color32(255,255,255,255));locGermanSecondLeague.Add(locTeamEintrachtBrunswick);
		Team locTeamFCEnergieCottbus = new Team("FC Energie Cottbus", new TeamStatusModule(TeamStatus.LOW), new TeamSkillsModule(TeamType.OFF), new Color32(255,255,255,255), new Color32(255,255,255,255));locGermanSecondLeague.Add(locTeamFCEnergieCottbus);
		Team locTeamFCErzgebirgeAue = new Team("FC Erzgebirge Aue", new TeamStatusModule(TeamStatus.LOW), new TeamSkillsModule(TeamType.DEF), new Color32(255,255,255,255), new Color32(255,255,255,255));locGermanSecondLeague.Add(locTeamFCErzgebirgeAue);
		Team locTeamFCIngolstadt04 = new Team("FC Ingolstadt 04", new TeamStatusModule(TeamStatus.LOW), new TeamSkillsModule(TeamType.DEF), new Color32(255,255,255,255), new Color32(255,255,255,255));locGermanSecondLeague.Add(locTeamFCIngolstadt04);
		Team locTeamFCKaiserslautern = new Team("FC Kaiserslautern", new TeamStatusModule(TeamStatus.LOW), new TeamSkillsModule(TeamType.MID), new Color32(255,255,255,255), new Color32(255,255,255,255));locGermanSecondLeague.Add(locTeamFCKaiserslautern);
		Team locTeamFCNuremberg = new Team("FC Nuremberg", new TeamStatusModule(TeamStatus.LOW), new TeamSkillsModule(TeamType.DEF), new Color32(255,255,255,255), new Color32(255,255,255,255));locGermanSecondLeague.Add(locTeamFCNuremberg);
		Team locTeamFCSanktPauli = new Team("FC Sankt Pauli", new TeamStatusModule(TeamStatus.LOW), new TeamSkillsModule(TeamType.OFF), new Color32(255,255,255,255), new Color32(255,255,255,255));locGermanSecondLeague.Add(locTeamFCSanktPauli);
		Team locTeamFCUnionBerlin = new Team("FC Union Berlin", new TeamStatusModule(TeamStatus.LOW), new TeamSkillsModule(TeamType.MID), new Color32(255,255,255,255), new Color32(255,255,255,255));locGermanSecondLeague.Add(locTeamFCUnionBerlin);
		Team locTeamFortunaDusseldorf = new Team("Fortuna Dusseldorf", new TeamStatusModule(TeamStatus.LOW), new TeamSkillsModule(TeamType.MID), new Color32(255,255,255,255), new Color32(255,255,255,255));locGermanSecondLeague.Add(locTeamFortunaDusseldorf);
		Team locTeamFSVFrancfort = new Team("FSV Francfort", new TeamStatusModule(TeamStatus.LOW), new TeamSkillsModule(TeamType.MID), new Color32(255,255,255,255), new Color32(255,255,255,255));locGermanSecondLeague.Add(locTeamFSVFrancfort);
		Team locTeamKarlsruherSC = new Team("Karlsruher SC", new TeamStatusModule(TeamStatus.LOW), new TeamSkillsModule(TeamType.MID), new Color32(255,255,255,255), new Color32(255,255,255,255));locGermanSecondLeague.Add(locTeamKarlsruherSC);
		Team locTeamSGDynamoDresde = new Team("SG Dynamo Dresde", new TeamStatusModule(TeamStatus.LOW), new TeamSkillsModule(TeamType.DEF), new Color32(255,255,255,255), new Color32(255,255,255,255));locGermanSecondLeague.Add(locTeamSGDynamoDresde);
		Team locTeamSpVggGreutherFurth = new Team("SpVgg Greuther Furth", new TeamStatusModule(TeamStatus.LOW), new TeamSkillsModule(TeamType.MID), new Color32(255,255,255,255), new Color32(255,255,255,255));locGermanSecondLeague.Add(locTeamSpVggGreutherFurth);
		Team locTeamSVSandhausen = new Team("SV Sandhausen", new TeamStatusModule(TeamStatus.LOW), new TeamSkillsModule(TeamType.MID), new Color32(255,255,255,255), new Color32(255,255,255,255));locGermanSecondLeague.Add(locTeamSVSandhausen);
		Team locTeamTSVMunich1860 = new Team("TSV Munich 1860", new TeamStatusModule(TeamStatus.LOW), new TeamSkillsModule(TeamType.OFF), new Color32(255,255,255,255), new Color32(255,255,255,255));locGermanSecondLeague.Add(locTeamTSVMunich1860);
		Team locTeamVfLBochum = new Team("VfL Bochum", new TeamStatusModule(TeamStatus.LOW), new TeamSkillsModule(TeamType.OFF), new Color32(255,255,255,255), new Color32(255,255,255,255));locGermanSecondLeague.Add(locTeamVfLBochum);
		Team locTeamVfRAalen = new Team("VfR Aalen", new TeamStatusModule(TeamStatus.LOW), new TeamSkillsModule(TeamType.DEF), new Color32(255,255,255,255), new Color32(255,255,255,255));locGermanSecondLeague.Add(locTeamVfRAalen);

		return locGermanSecondLeague; 
	}
	
	/**
	 * Function that create a list of team for the Italian first league
	 * @return the Italian first league
	 */
	private List<Team> createItalianFirstLeague() {
		List<Team> locItalianFirstLeague = new List<Team>();

		Team locTeamACMilan = new Team("AC Milan", new TeamStatusModule(TeamStatus.OUT), new TeamSkillsModule(TeamType.OFF), new Color32(240,0,27,255), new Color32(0,0,0,255));locItalianFirstLeague.Add(locTeamACMilan);
		Team locTeamASRome = new Team("AS Rome", new TeamStatusModule(TeamStatus.FAV), new TeamSkillsModule(TeamType.OFF), new Color32(179,8,56,255), new Color32(248,151,40,255));locItalianFirstLeague.Add(locTeamASRome);
		Team locTeamAtalanta = new Team("Atalanta", new TeamStatusModule(TeamStatus.MID), new TeamSkillsModule(TeamType.DEF), new Color32(28,140,208,255), new Color32(0,0,0,255));locItalianFirstLeague.Add(locTeamAtalanta);
		Team locTeamBologne = new Team("Bologne", new TeamStatusModule(TeamStatus.MID), new TeamSkillsModule(TeamType.DEF), new Color32(255,255,255,255), new Color32(255,255,255,255));locItalianFirstLeague.Add(locTeamBologne);
		Team locTeamCagliari = new Team("Cagliari", new TeamStatusModule(TeamStatus.MID), new TeamSkillsModule(TeamType.MID), new Color32(181,15,36,255), new Color32(16,30,50,255));locItalianFirstLeague.Add(locTeamCagliari);
		Team locTeamCatane = new Team("Catane", new TeamStatusModule(TeamStatus.MID), new TeamSkillsModule(TeamType.OFF), new Color32(255,255,255,255), new Color32(255,255,255,255));locItalianFirstLeague.Add(locTeamCatane);
		Team locTeamChievo = new Team("Chievo", new TeamStatusModule(TeamStatus.MID), new TeamSkillsModule(TeamType.MID), new Color32(252,227,38,255), new Color32(49,49,145,255));locItalianFirstLeague.Add(locTeamChievo);
		Team locTeamFiorentina = new Team("Fiorentina", new TeamStatusModule(TeamStatus.FAV), new TeamSkillsModule(TeamType.MID), new Color32(105,11,103,255), new Color32(255,255,255,255));locItalianFirstLeague.Add(locTeamFiorentina);
		Team locTeamGenoa = new Team("Genoa", new TeamStatusModule(TeamStatus.MID), new TeamSkillsModule(TeamType.MID), new Color32(223,45,47,255), new Color32(0,30,87,255));locItalianFirstLeague.Add(locTeamGenoa);
		Team locTeamHellas = new Team("Hellas", new TeamStatusModule(TeamStatus.MID), new TeamSkillsModule(TeamType.OFF), new Color32(22,40,131,255), new Color32(255,237,0,255));locItalianFirstLeague.Add(locTeamHellas);
		Team locTeamInter = new Team("Inter", new TeamStatusModule(TeamStatus.FAV), new TeamSkillsModule(TeamType.DEF), new Color32(0,0,0,255), new Color32(0,84,160,255));locItalianFirstLeague.Add(locTeamInter);
		Team locTeamJuventus = new Team("Juventus", new TeamStatusModule(TeamStatus.FAV), new TeamSkillsModule(TeamType.MID), new Color32(255,255,255,255), new Color32(0,0,0,255));locItalianFirstLeague.Add(locTeamJuventus);
		Team locTeamLazio = new Team("Lazio", new TeamStatusModule(TeamStatus.OUT), new TeamSkillsModule(TeamType.DEF), new Color32(0,173,238,255), new Color32(255,255,255,255));locItalianFirstLeague.Add(locTeamLazio);
		Team locTeamLivourne = new Team("Livourne", new TeamStatusModule(TeamStatus.MID), new TeamSkillsModule(TeamType.DEF), new Color32(255,255,255,255), new Color32(255,255,255,255));locItalianFirstLeague.Add(locTeamLivourne);
		Team locTeamNaples = new Team("Naples", new TeamStatusModule(TeamStatus.FAV), new TeamSkillsModule(TeamType.OFF), new Color32(88,216,254,255), new Color32(255,255,255,255));locItalianFirstLeague.Add(locTeamNaples);
		Team locTeamParme = new Team("Parme", new TeamStatusModule(TeamStatus.OUT), new TeamSkillsModule(TeamType.MID), new Color32(0,0,0,255), new Color32(255,211,0,255));locItalianFirstLeague.Add(locTeamParme);
		Team locTeamSampdoria = new Team("Sampdoria", new TeamStatusModule(TeamStatus.OUT), new TeamSkillsModule(TeamType.MID), new Color32(0,80,187,255), new Color32(255,255,255,255));locItalianFirstLeague.Add(locTeamSampdoria);
		Team locTeamSassuolo = new Team("Sassuolo", new TeamStatusModule(TeamStatus.MID), new TeamSkillsModule(TeamType.DEF), new Color32(22,114,48,255), new Color32(0,0,0,255));locItalianFirstLeague.Add(locTeamSassuolo);
		Team locTeamTorino = new Team("Torino", new TeamStatusModule(TeamStatus.OUT), new TeamSkillsModule(TeamType.OFF), new Color32(123,30,32,255), new Color32(255,255,255,255));locItalianFirstLeague.Add(locTeamTorino);
		Team locTeamACCesena = new Team("AC Cesena", new TeamStatusModule(TeamStatus.MID), new TeamSkillsModule(TeamType.MID), new Color32(0,0,0,255), new Color32(255,255,255,255));locItalianFirstLeague.Add(locTeamACCesena);

		return locItalianFirstLeague; 
	}
	
	/**
	 * Function that create a list of team for the Italian second league
	 * @return the Italian second league
	 */
	private List<Team> createItalianSecondLeague() {
		List<Team> locItalianSecondLeague = new List<Team>();

		Team locTeamACSienne = new Team("AC Sienne", new TeamStatusModule(TeamStatus.LOW), new TeamSkillsModule(TeamType.MID), new Color32(255,255,255,255), new Color32(255,255,255,255));locItalianSecondLeague.Add(locTeamACSienne);
		Team locTeamASAvellino = new Team("AS Avellino", new TeamStatusModule(TeamStatus.LOW), new TeamSkillsModule(TeamType.MID), new Color32(255,255,255,255), new Color32(255,255,255,255));locItalianSecondLeague.Add(locTeamASAvellino);
		Team locTeamASBari = new Team("AS Bari", new TeamStatusModule(TeamStatus.LOW), new TeamSkillsModule(TeamType.DEF), new Color32(255,255,255,255), new Color32(255,255,255,255));locItalianSecondLeague.Add(locTeamASBari);
		Team locTeamASCittadella = new Team("AS Cittadella", new TeamStatusModule(TeamStatus.LOW), new TeamSkillsModule(TeamType.DEF), new Color32(255,255,255,255), new Color32(255,255,255,255));locItalianSecondLeague.Add(locTeamASCittadella);
		Team locTeamASVarese = new Team("AS Varese", new TeamStatusModule(TeamStatus.LOW), new TeamSkillsModule(TeamType.MID), new Color32(255,255,255,255), new Color32(255,255,255,255));locItalianSecondLeague.Add(locTeamASVarese);
		Team locTeamASDTrapani = new Team("ASD Trapani", new TeamStatusModule(TeamStatus.LOW), new TeamSkillsModule(TeamType.OFF), new Color32(255,255,255,255), new Color32(255,255,255,255));locItalianSecondLeague.Add(locTeamASDTrapani);
		Team locTeamBrescia = new Team("Brescia", new TeamStatusModule(TeamStatus.LOW), new TeamSkillsModule(TeamType.OFF), new Color32(255,255,255,255), new Color32(255,255,255,255));locItalianSecondLeague.Add(locTeamBrescia);
		Team locTeamCarpiFC = new Team("Carpi FC", new TeamStatusModule(TeamStatus.LOW), new TeamSkillsModule(TeamType.MID), new Color32(255,255,255,255), new Color32(255,255,255,255));locItalianSecondLeague.Add(locTeamCarpiFC);
		Team locTeamDelfinoPescara = new Team("Delfino Pescara", new TeamStatusModule(TeamStatus.LOW), new TeamSkillsModule(TeamType.MID), new Color32(255,255,255,255), new Color32(255,255,255,255));locItalianSecondLeague.Add(locTeamDelfinoPescara);
		Team locTeamEmpoliFC = new Team("Empoli FC", new TeamStatusModule(TeamStatus.LOW), new TeamSkillsModule(TeamType.OFF), new Color32(0,87,156,255), new Color32(255,255,255,255));locItalianSecondLeague.Add(locTeamEmpoliFC);
		Team locTeamFCCrotone = new Team("FC Crotone", new TeamStatusModule(TeamStatus.LOW), new TeamSkillsModule(TeamType.OFF), new Color32(255,255,255,255), new Color32(255,255,255,255));locItalianSecondLeague.Add(locTeamFCCrotone);
		Team locTeamModeneFC = new Team("Modene FC", new TeamStatusModule(TeamStatus.LOW), new TeamSkillsModule(TeamType.MID), new Color32(255,255,255,255), new Color32(255,255,255,255));locItalianSecondLeague.Add(locTeamModeneFC);
		Team locTeamNovare = new Team("Novare", new TeamStatusModule(TeamStatus.LOW), new TeamSkillsModule(TeamType.DEF), new Color32(255,255,255,255), new Color32(255,255,255,255));locItalianSecondLeague.Add(locTeamNovare);
		Team locTeamPadoue = new Team("Padoue", new TeamStatusModule(TeamStatus.LOW), new TeamSkillsModule(TeamType.OFF), new Color32(255,255,255,255), new Color32(255,255,255,255));locItalianSecondLeague.Add(locTeamPadoue);
		Team locTeamReggina = new Team("Reggina", new TeamStatusModule(TeamStatus.LOW), new TeamSkillsModule(TeamType.DEF), new Color32(255,255,255,255), new Color32(255,255,255,255));locItalianSecondLeague.Add(locTeamReggina);
		Team locTeamSpezia = new Team("Spezia", new TeamStatusModule(TeamStatus.LOW), new TeamSkillsModule(TeamType.DEF), new Color32(255,255,255,255), new Color32(255,255,255,255));locItalianSecondLeague.Add(locTeamSpezia);
		Team locTeamSSJuveStabia = new Team("SS Juve Stabia", new TeamStatusModule(TeamStatus.LOW), new TeamSkillsModule(TeamType.MID), new Color32(255,255,255,255), new Color32(255,255,255,255));locItalianSecondLeague.Add(locTeamSSJuveStabia);
		Team locTeamSSVirtusLanciano = new Team("SS Virtus Lanciano", new TeamStatusModule(TeamStatus.LOW), new TeamSkillsModule(TeamType.DEF), new Color32(255,255,255,255), new Color32(255,255,255,255));locItalianSecondLeague.Add(locTeamSSVirtusLanciano);
		Team locTeamTernana = new Team("Ternana", new TeamStatusModule(TeamStatus.LOW), new TeamSkillsModule(TeamType.MID), new Color32(255,255,255,255), new Color32(255,255,255,255));locItalianSecondLeague.Add(locTeamTernana);
		Team locTeamUdinese = new Team("Udinese", new TeamStatusModule(TeamStatus.LOW), new TeamSkillsModule(TeamType.OFF), new Color32(0,0,0,255), new Color32(255,255,255,255));locItalianSecondLeague.Add(locTeamUdinese);
		Team locTeamUSLatina = new Team("US Latina", new TeamStatusModule(TeamStatus.LOW), new TeamSkillsModule(TeamType.DEF), new Color32(255,255,255,255), new Color32(255,255,255,255));locItalianSecondLeague.Add(locTeamUSLatina);
		Team locTeamUSPalerme = new Team("US Palerme", new TeamStatusModule(TeamStatus.LOW), new TeamSkillsModule(TeamType.OFF), new Color32(248,166,209,255), new Color32(0,0,0,255));locItalianSecondLeague.Add(locTeamUSPalerme);

		return locItalianSecondLeague; 
	}
	
	/**
	 * Function that create a list of team for the Spanish first league
	 * @return the Spanish first league
	 */
	private List<Team> createSpanishFirstLeague() {
		List<Team> locSpanishFirstLeague = new List<Team>();

		Team locTeamAthleticBilbao = new Team("Athletic Bilbao", new TeamStatusModule(TeamStatus.FAV), new TeamSkillsModule(TeamType.DEF), new Color32(226,0,26,255), new Color32(255,255,255,255));locSpanishFirstLeague.Add(locTeamAthleticBilbao);
		Team locTeamAtleticoMadrid = new Team("Atletico Madrid", new TeamStatusModule(TeamStatus.FAV), new TeamSkillsModule(TeamType.MID), new Color32(212,46,18,255), new Color32(23,23,150,255));locSpanishFirstLeague.Add(locTeamAtleticoMadrid);
		Team locTeamBetisSeville = new Team("Betis Seville", new TeamStatusModule(TeamStatus.MID), new TeamSkillsModule(TeamType.DEF), new Color32(255,255,255,255), new Color32(255,255,255,255));locSpanishFirstLeague.Add(locTeamBetisSeville);
		Team locTeamCAOsasuna = new Team("CA Osasuna", new TeamStatusModule(TeamStatus.MID), new TeamSkillsModule(TeamType.MID), new Color32(255,255,255,255), new Color32(255,255,255,255));locSpanishFirstLeague.Add(locTeamCAOsasuna);
		Team locTeamCeltaVigo = new Team("Celta Vigo", new TeamStatusModule(TeamStatus.MID), new TeamSkillsModule(TeamType.OFF), new Color32(0,120,186,255), new Color32(218,42,78,255));locSpanishFirstLeague.Add(locTeamCeltaVigo);
		Team locTeamElcheCF = new Team("Elche CF", new TeamStatusModule(TeamStatus.MID), new TeamSkillsModule(TeamType.DEF), new Color32(255,255,255,255), new Color32(33,100,33,255));locSpanishFirstLeague.Add(locTeamElcheCF);
		Team locTeamEspanyolBarcelone = new Team("Espanyol Barcelone", new TeamStatusModule(TeamStatus.OUT), new TeamSkillsModule(TeamType.OFF), new Color32(0,80,245,255), new Color32(255,255,255,255));locSpanishFirstLeague.Add(locTeamEspanyolBarcelone);
		Team locTeamFCBarcelone = new Team("FC Barcelone", new TeamStatusModule(TeamStatus.FAV), new TeamSkillsModule(TeamType.OFF), new Color32(0,49,211,255), new Color32(210,0,51,255));locSpanishFirstLeague.Add(locTeamFCBarcelone);
		Team locTeamGetafeCF = new Team("Getafe CF", new TeamStatusModule(TeamStatus.MID), new TeamSkillsModule(TeamType.MID), new Color32(0,85,157,255), new Color32(0,85,157,255));locSpanishFirstLeague.Add(locTeamGetafeCF);
		Team locTeamGrenadeCF = new Team("Grenade CF", new TeamStatusModule(TeamStatus.MID), new TeamSkillsModule(TeamType.DEF), new Color32(255,0,0,255), new Color32(255,255,255,255));locSpanishFirstLeague.Add(locTeamGrenadeCF);
		Team locTeamLevante = new Team("Levante", new TeamStatusModule(TeamStatus.MID), new TeamSkillsModule(TeamType.MID), new Color32(12,68,230,255), new Color32(250,0,30,255));locSpanishFirstLeague.Add(locTeamLevante);
		Team locTeamMalagaCF = new Team("Malaga CF", new TeamStatusModule(TeamStatus.OUT), new TeamSkillsModule(TeamType.OFF), new Color32(20,110,175,255), new Color32(255,255,255,255));locSpanishFirstLeague.Add(locTeamMalagaCF);
		Team locTeamRayoVallecano = new Team("Rayo Vallecano", new TeamStatusModule(TeamStatus.MID), new TeamSkillsModule(TeamType.OFF), new Color32(255,255,255,255), new Color32(226,32,23,255));locSpanishFirstLeague.Add(locTeamRayoVallecano);
		Team locTeamRealMadrid = new Team("Real Madrid", new TeamStatusModule(TeamStatus.FAV), new TeamSkillsModule(TeamType.MID), new Color32(255,255,255,255), new Color32(10,40,103,255));locSpanishFirstLeague.Add(locTeamRealMadrid);
		Team locTeamRealSociedad = new Team("Real Sociedad", new TeamStatusModule(TeamStatus.OUT), new TeamSkillsModule(TeamType.OFF), new Color32(0,93,230,255), new Color32(255,255,255,255));locSpanishFirstLeague.Add(locTeamRealSociedad);
		Team locTeamRealValladolid = new Team("Real Valladolid", new TeamStatusModule(TeamStatus.MID), new TeamSkillsModule(TeamType.DEF), new Color32(255,255,255,255), new Color32(255,255,255,255));locSpanishFirstLeague.Add(locTeamRealValladolid);
		Team locTeamSevilleFC = new Team("Seville FC", new TeamStatusModule(TeamStatus.FAV), new TeamSkillsModule(TeamType.DEF), new Color32(255,255,255,255), new Color32(255,18,0,255));locSpanishFirstLeague.Add(locTeamSevilleFC);
		Team locTeamUDAlmeria = new Team("UD Almeria", new TeamStatusModule(TeamStatus.MID), new TeamSkillsModule(TeamType.MID), new Color32(250,4,4,255), new Color32(255,255,255,255));locSpanishFirstLeague.Add(locTeamUDAlmeria);
		Team locTeamValenceCF = new Team("Valence CF", new TeamStatusModule(TeamStatus.OUT), new TeamSkillsModule(TeamType.DEF), new Color32(255,255,255,255), new Color32(0,0,0,255));locSpanishFirstLeague.Add(locTeamValenceCF);
		Team locTeamVillarrealCF = new Team("Villarreal CF", new TeamStatusModule(TeamStatus.LOW), new TeamSkillsModule(TeamType.OFF), new Color32(254,240,0,255), new Color32(0,125,223,255));locSpanishFirstLeague.Add(locTeamVillarrealCF);

		return locSpanishFirstLeague; 
	}
	
	/**
	 * Function that create a list of team for the Spanish second league
	 * @return the Spanish second league
	 */
	private List<Team> createSpanishSecondLeague() {
		List<Team> locSpanishSecondLeague = new List<Team>();

		Team locTeamADAlcorcon = new Team("AD Alcorcon", new TeamStatusModule(TeamStatus.LOW), new TeamSkillsModule(TeamType.MID), new Color32(255,255,255,255), new Color32(255,255,255,255));locSpanishSecondLeague.Add(locTeamADAlcorcon);
		Team locTeamCDLugo = new Team("CD Lugo", new TeamStatusModule(TeamStatus.LOW), new TeamSkillsModule(TeamType.DEF), new Color32(255,255,255,255), new Color32(255,255,255,255));locSpanishSecondLeague.Add(locTeamCDLugo);
		Team locTeamCDMirandes = new Team("CD Mirandes", new TeamStatusModule(TeamStatus.LOW), new TeamSkillsModule(TeamType.MID), new Color32(255,255,255,255), new Color32(255,255,255,255));locSpanishSecondLeague.Add(locTeamCDMirandes);
		Team locTeamCDNumancia = new Team("CD Numancia", new TeamStatusModule(TeamStatus.LOW), new TeamSkillsModule(TeamType.OFF), new Color32(255,255,255,255), new Color32(255,255,255,255));locSpanishSecondLeague.Add(locTeamCDNumancia);
		Team locTeamCDTenerife = new Team("CD Tenerife", new TeamStatusModule(TeamStatus.LOW), new TeamSkillsModule(TeamType.MID), new Color32(255,255,255,255), new Color32(255,255,255,255));locSpanishSecondLeague.Add(locTeamCDTenerife);
		Team locTeamCESabadell = new Team("CE Sabadell", new TeamStatusModule(TeamStatus.LOW), new TeamSkillsModule(TeamType.DEF), new Color32(255,255,255,255), new Color32(255,255,255,255));locSpanishSecondLeague.Add(locTeamCESabadell);
		Team locTeamCordobaCF = new Team("Cordoba CF", new TeamStatusModule(TeamStatus.LOW), new TeamSkillsModule(TeamType.MID), new Color32(90,171,37,255), new Color32(255,255,255,255));locSpanishSecondLeague.Add(locTeamCordobaCF);
		Team locTeamDeportivoAlaves = new Team("Deportivo Alaves", new TeamStatusModule(TeamStatus.LOW), new TeamSkillsModule(TeamType.OFF), new Color32(255,255,255,255), new Color32(255,255,255,255));locSpanishSecondLeague.Add(locTeamDeportivoAlaves);
		Team locTeamDeportivoLaCorogne = new Team("Deportivo La Corogne", new TeamStatusModule(TeamStatus.LOW), new TeamSkillsModule(TeamType.MID), new Color32(132,58,132,255), new Color32(255,255,255,255));locSpanishSecondLeague.Add(locTeamDeportivoLaCorogne);
		Team locTeamGironaFC = new Team("Girona FC", new TeamStatusModule(TeamStatus.LOW), new TeamSkillsModule(TeamType.DEF), new Color32(255,255,255,255), new Color32(255,255,255,255));locSpanishSecondLeague.Add(locTeamGironaFC);
		Team locTeamHerculesCF = new Team("Hercules CF", new TeamStatusModule(TeamStatus.LOW), new TeamSkillsModule(TeamType.MID), new Color32(255,255,255,255), new Color32(255,255,255,255));locSpanishSecondLeague.Add(locTeamHerculesCF);
		Team locTeamRCDMajorque = new Team("RCD Majorque", new TeamStatusModule(TeamStatus.LOW), new TeamSkillsModule(TeamType.OFF), new Color32(255,255,255,255), new Color32(255,255,255,255));locSpanishSecondLeague.Add(locTeamRCDMajorque);
		Team locTeamRealJaen = new Team("Real Jaen", new TeamStatusModule(TeamStatus.LOW), new TeamSkillsModule(TeamType.MID), new Color32(255,255,255,255), new Color32(255,255,255,255));locSpanishSecondLeague.Add(locTeamRealJaen);
		Team locTeamRealMurcie = new Team("Real Murcie", new TeamStatusModule(TeamStatus.LOW), new TeamSkillsModule(TeamType.DEF), new Color32(255,255,255,255), new Color32(255,255,255,255));locSpanishSecondLeague.Add(locTeamRealMurcie);
		Team locTeamRealSaragosse = new Team("Real Saragosse", new TeamStatusModule(TeamStatus.LOW), new TeamSkillsModule(TeamType.MID), new Color32(255,255,255,255), new Color32(255,255,255,255));locSpanishSecondLeague.Add(locTeamRealSaragosse);
		Team locTeamRecreativo = new Team("Recreativo", new TeamStatusModule(TeamStatus.LOW), new TeamSkillsModule(TeamType.OFF), new Color32(255,255,255,255), new Color32(255,255,255,255));locSpanishSecondLeague.Add(locTeamRecreativo);
		Team locTeamSDEibar = new Team("SD Eibar", new TeamStatusModule(TeamStatus.LOW), new TeamSkillsModule(TeamType.MID), new Color32(227,34,25,255), new Color32(0,94,168,255));locSpanishSecondLeague.Add(locTeamSDEibar);
		Team locTeamSDPonferradina = new Team("SD Ponferradina", new TeamStatusModule(TeamStatus.LOW), new TeamSkillsModule(TeamType.DEF), new Color32(255,255,255,255), new Color32(255,255,255,255));locSpanishSecondLeague.Add(locTeamSDPonferradina);
		Team locTeamSportingGijon = new Team("Sporting Gijon", new TeamStatusModule(TeamStatus.LOW), new TeamSkillsModule(TeamType.MID), new Color32(255,255,255,255), new Color32(255,255,255,255));locSpanishSecondLeague.Add(locTeamSportingGijon);
		Team locTeamUDLasPalmas = new Team("UD Las Palmas", new TeamStatusModule(TeamStatus.LOW), new TeamSkillsModule(TeamType.OFF), new Color32(255,255,255,255), new Color32(255,255,255,255));locSpanishSecondLeague.Add(locTeamUDLasPalmas);

		return locSpanishSecondLeague; 
	}
}
