﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public class CalendarManager {

    /** Instance unique pre-initialisee */
    private static CalendarManager _instance = new CalendarManager();

    /** Constructeur prive */
    private CalendarManager() {

    }

    public static CalendarManager getInstance() {
        return _instance;
    }

    /**
	 * Function to create all journeys of a championship
	 * @param parTabTeam List of teams for the target championship
	 */
    public List<Journey> createChampionshipJourney(List<Team> parTabTeam) {
        int i = 0;
        bool locAlternanceValue = false;    // bool pour l'alternance des match interieur/exterieur
        int locNbDays = parTabTeam.Count - 1;
        List<Journey> locTabJourney = new List<Journey>();
        Journey locTmpJourney;
        for (i = 0; i < locNbDays; i++) {
            locTmpJourney = createJourney(parTabTeam, locAlternanceValue);
            locTabJourney.Add(locTmpJourney);
            locAlternanceValue = !locAlternanceValue;
            roundTabTeam(parTabTeam);
        }
        getReturnJourneys(locTabJourney); // Inversion des matchs aller pour obtenir les matchs retour
        return locTabJourney;
    }

    /**
	 * Fonction qui permet de creer une journee de championnat
	 * @param parTabTeam Liste des equipes du championnat
	 * @param parInvert bool qui indique s'il faut inverser l'ordre des equipes pour l'alternance match a domicile/exterieur 
	 * @return La liste des matchs de la journee de championnat
	 */
    private Journey createJourney(List<Team> parTabTeam, bool parInvert) {
		int i = 0;
		int n = parTabTeam.Count;
		int h = n / 2;
		Match locMatch;
		Journey locJourney = new Journey();
		for (i = 0; i < h; i++) {
			if (parInvert) {
				locMatch = new Match(parTabTeam[i]._id, parTabTeam[n - 1 - i]._id);
			}
			else {
				locMatch = new Match(parTabTeam[n - 1 - i]._id, parTabTeam[i]._id);
			}
			locJourney.addMatch(locMatch);
		}
		return locJourney;
	}

	/**
	 * Fonction qui permet d'obtenir les journees retours a partir d'un championnat de journees aller
	 * @param parTabJourney Les journees des matchs aller
	 */
	private void getReturnJourneys(List<Journey> parTabJourney) {
		Journey locTmpJourney = new Journey();
		int locNbJourney = parTabJourney.Count;
		for (int i = 0; i < locNbJourney; i++) {
			locTmpJourney = parTabJourney[i].getReturnJourney();
			parTabJourney.Add(locTmpJourney);
		}
	}
	
	/**
	 * Fonction qui modifie l'ordre des equipes du championnat afin de faire tourner les matchs generes pour une journee de championnat
	 * @param parTabTeam La liste des equipes
	 */
	private void roundTabTeam(List<Team> parTabTeam) {
		int i = 1; 										// On fixe la premiere equipe (début de la boucle à 1)
		int n = parTabTeam.Count - 1; 					// Le nombre de tours à faire (le nombre d'équipe moins la première)
		Team locTmpTeam = parTabTeam[1]; 				// On sauvegarde la 2ème équipe
		for (i = 1; i < n; i++) {
			parTabTeam[i] = parTabTeam[i + 1]; 			// On fait tourner toutes les autres equipes (équipe i+1 à l'emplacement i)
		}
		parTabTeam[n] = locTmpTeam;						// On remet la 2ème équipe en fin de liste
	}
}
