﻿using System;
using UnityEngine;
using System.Collections.Generic;
using Assets.Scripts.Models;

public class GameManager : MonoBehaviour {

	// Managers
	public static GameManager _instance = null;

    public Game Game { get; set; }

	void Awake() {
		if (_instance == null)
			_instance = this;
		else if (_instance != this)
			Destroy (gameObject);
		DontDestroyOnLoad(gameObject);
	}

	public void createEnglishChampionship() {
		ChampionshipManager.getInstance().createEnglishChampionship ();
        Game.Championship = ChampionshipManager.getInstance()._championship;
	}

	public void createFrenchChampionship() {
		ChampionshipManager.getInstance().createFrenchChampionship ();
        Game.Championship = ChampionshipManager.getInstance()._championship;
    }

	public void createGermanChampionship() {
		ChampionshipManager.getInstance().createGermanChampionship ();
        Game.Championship = ChampionshipManager.getInstance()._championship;
    }

	public void createItalianChampionship() {
		ChampionshipManager.getInstance().createItalianChampionship ();
        Game.Championship = ChampionshipManager.getInstance()._championship;
    }

	public void createSpanishChampionship() {
		ChampionshipManager.getInstance().createSpanishChampionship ();
        Game.Championship = ChampionshipManager.getInstance()._championship;
    }

    public Match GetCurrentPlayerTeamMatch()
    {
        Team locTeam = ChampionshipManager.getInstance().GetTeamById(Game.Player._team);

        return ChampionshipManager.getInstance().GetCurrentTeamMatch(locTeam, Game.JourneyIndex);
    }

    public void SaveNewGame()
    {

        var locDbMgr = DIContainer.DbManager;
        locDbMgr.OpenConnection();

        DIContainer.GameService.AddGame(Game);

        locDbMgr.CloseConnection();
    }

    public void SaveGame()
    {
        var locDbMgr = DIContainer.DbManager;
        locDbMgr.OpenConnection();

        DIContainer.GameService.UpdateGame(Game);

        locDbMgr.CloseConnection();
    }

    public void SetChampionship(Championship parChampionship)
    {
        Game.Championship = parChampionship;

        var locDbMgr = DIContainer.DbManager;
        locDbMgr.OpenConnection();

        foreach(Team locTeam in parChampionship._tabFirstLeagueTeams)
        {
            DIContainer.TeamService.AddTeam(locTeam);
        }

        locDbMgr.CloseConnection();
    }
}
