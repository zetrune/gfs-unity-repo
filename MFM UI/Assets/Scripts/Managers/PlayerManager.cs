﻿using UnityEngine;
using System.Collections;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class PlayerManager {

	/** Instance unique pre-initialisee */
	private static PlayerManager _instance = new PlayerManager();
	
	/** Constructeur prive */
	private PlayerManager() {
		
	}
	
	/** Point d'acces pour l'instance unique du singleton */
	public static PlayerManager getInstance() {
		return _instance;
	}
	
	public void setTeam(Player parPlayer, Team parTeam) {
		parPlayer._team = parTeam._id;
	}
	
	
	public void updateAfterMatch(Player parPlayer, Match parMatch) {
        MatchResult locMatchResult = parMatch.getMatchResult(parPlayer._team);
        updateGamePoint(parPlayer, locMatchResult);

        parPlayer._historyModule.update(parMatch);
		parPlayer._statsModule.update(ChampionshipManager.getInstance()._championship, parPlayer._team, parMatch);
	}
	
	public void updateAfterChampionship(Player parPlayer, Championship parChampionship) {
        Team locPlayerTeam = ChampionshipManager.getInstance().GetTeamById(parPlayer._team);

        ChampionshipResult locChampionshipResult = new ChampionshipResult(parChampionship, locPlayerTeam);
		parPlayer._palmaresModule.update(locChampionshipResult);
		parPlayer._statsModule.updateAfterChampionship(parChampionship.isFinished(GameManager._instance.Game.JourneyIndex));
		resetTeam(parPlayer);
		resetBoardTrust(parPlayer);
	}
	
	/**
	 * Fonction qui permet d'augmenter la confiance des dirigeants
	 */
	public void upgradeBoardTrust(Player parPlayer) {
		if (parPlayer._boardTrust < 2)
			parPlayer._boardTrust++;
	}
	
	/**
	 * Fonction qui permet de diminuer la confiance des dirigeants
	 */
	public void downgradeBoardTrust(Player parPlayer) {
		if (parPlayer._boardTrust > 0)
			parPlayer._boardTrust--;
	}
	
	private void resetTeam(Player parPlayer) {
		parPlayer._team = 0;
	}
	
	private void resetBoardTrust(Player parPlayer) {
		parPlayer._boardTrust = 1;
	}

    private void updateGamePoint(Player parPlayer, MatchResult parMatchResult)
    {
        int locResult;

        switch (parMatchResult)
        {
            case MatchResult.VICTORY:
                locResult = 3;
                break;
            case MatchResult.DRAW:
                locResult = 1;
                break;
            default:
                locResult = 0;
                break;
        }

        parPlayer._gamePoints += locResult;
    }
}
