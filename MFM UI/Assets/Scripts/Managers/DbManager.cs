﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Mono.Data.Sqlite;
using System.Data;

namespace Assets.Scripts.Managers
{
    public interface IDbManager
    {
        void CloseConnection();
        IDbCommand CreateDbCommand();
        void OpenConnection();
    }

    public class DbManager : IDbManager
    {

        IDbConnection _dbConn;

        public DbManager()
        {
            _dbConn = new SqliteConnection(ConstManager.DB_CONNECTION);
        }

        public void OpenConnection()
        {
            _dbConn.Open();
        }

        public void CloseConnection()
        {
            _dbConn.Close();
        }

        public IDbCommand CreateDbCommand()
        {
            return _dbConn.CreateCommand();
        }
    }
}
