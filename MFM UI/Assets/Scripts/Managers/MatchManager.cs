﻿using UnityEngine;
using System;
using System.Collections;
using Random = UnityEngine.Random;
using Assets.Scripts.DAO.Services;

public class MatchManager {

	/** Instance unique pre-initialisee */
	private static MatchManager _instance = new MatchManager();
	
	/** Constructeur prive */
	private MatchManager() {
		
	}
	
	/** Point d'acces pour l'instance unique du singleton */
	public static MatchManager getInstance() {
		return _instance;
	}

    public void playMatchActions(Match parMatch)
    {
        for (int i = 0; i < ConstManager.MAX_ACTIONS_PER_HALF * 2; i++)
        {
            CreateAction(parMatch);
            ComputeAction(parMatch);
        }
        updateMatchScore(parMatch);
    }

    public void CreateAction(Match parMatch)
    {
        float locSideRng = (float)Math.Round(Random.value, 2);
        ActionSide locActionSide = ActionSide.VISITOR;

        if (locSideRng >= 0.5f)
        {
            locActionSide = ActionSide.LOCAL;
        }

        Action locAction = new Action(locActionSide, parMatch._actions.Count);
        parMatch._actions.Add(locAction);
    }

    public void ComputeAction(Match parMatch)
    {
        initActionScore(parMatch);
        ComputeActionDef(parMatch);
        ComputeActionAtk(parMatch);
        ComputeActionAtk(parMatch);
        ComputeActionDef(parMatch);
    }

    public void ComputeActionDef(Match parMatch)
    {
        Action locAction = parMatch.LastAction();

        if (locAction == null || locAction.Resolved())
        {
            return;
        }

        float locScoreDef = ComputeDefScore(parMatch, locAction);
        float locTeamRandomScoreDef = (float)Math.Round(Random.value, 2);
        bool locTeamHasRescue = locTeamRandomScoreDef > 1 - locScoreDef;

        if (locTeamHasRescue)
        {
            if (locAction.Defense1 != ActionState.NONE)
            {
                locAction.Defense2 = ActionState.WIN;
            }
            else
            {
                locAction.Defense1 = ActionState.WIN;
            }
            
        }
        else
        {
            if (locAction.Defense1 != ActionState.NONE)
            {
                locAction.Defense2 = ActionState.LOSE;
                addGoal(parMatch, locAction);
            }
            else
            {
                locAction.Defense1 = ActionState.LOSE;
            }
            
        }
    }

    public void ComputeActionAtk(Match parMatch)
    {
        Action locAction = parMatch.LastAction();

        if (locAction == null || locAction.Resolved())
        {
            return;
        }

        float locScoreAtk = ComputeAtkScore(parMatch, locAction);
        float locTeamRandomScoreAtk = (float)Math.Round(Random.value, 2);
        bool locTeamHasOpportunity = locTeamRandomScoreAtk > 1 - locScoreAtk;

        if (locTeamHasOpportunity)
        {
            if (locAction.Attack1 != ActionState.NONE)
            {
                locAction.Attack2 = ActionState.WIN;

            }
            else
            {
                locAction.Attack1 = ActionState.WIN;
            }
        }
        else
        {
            if (locAction.Attack1 != ActionState.NONE)
            {
                locAction.Attack2 = ActionState.LOSE;
            }
            else
            {
                locAction.Attack1 = ActionState.LOSE;
            }
            
        }
    }

    private float ComputeDefScore(Match parMatch, Action parAction)
    {
        float locScoreDef = 0f;
        Team locLocalTeam = ChampionshipManager.getInstance().GetTeamById(parMatch._localTeam);
        Team locVisitorTeam = ChampionshipManager.getInstance().GetTeamById(parMatch._visitorTeam);
        Team locPlayerTeam = ChampionshipManager.getInstance().GetTeamById(GameManager._instance.Game.Player._team);

        if (parAction.Side == ActionSide.LOCAL)
        {
            locScoreDef = locVisitorTeam._teamSkills._teamScoreDef;

            if (parAction.Bonus == ActionBonus.YES && parMatch.isVisitor(locPlayerTeam))
            {
                locScoreDef += ConstManager.BONUS_VALUE;
            }
        }
        else
        {
            locScoreDef = locLocalTeam._teamSkills._teamScoreDef;

            if (parAction.Bonus == ActionBonus.YES && parMatch.isLocal(locPlayerTeam))
            {
                locScoreDef += ConstManager.BONUS_VALUE;
            }
        }

        return locScoreDef;
    }

    private float ComputeAtkScore(Match parMatch, Action parAction)
    {
        float locScoreAtk = 0f;
        Team locLocalTeam = ChampionshipManager.getInstance().GetTeamById(parMatch._localTeam);
        Team locVisitorTeam = ChampionshipManager.getInstance().GetTeamById(parMatch._visitorTeam);
        Team locPlayerTeam = ChampionshipManager.getInstance().GetTeamById(GameManager._instance.Game.Player._team);

        if (parAction.Side == ActionSide.LOCAL)
        {
            locScoreAtk = locLocalTeam._teamSkills._teamScoreAtk;

            if (parAction.Bonus == ActionBonus.YES && parMatch.isLocal(locPlayerTeam))
            {
                locScoreAtk += ConstManager.BONUS_VALUE;
            }
        }
        else
        {
            locScoreAtk = locVisitorTeam._teamSkills._teamScoreAtk;

            if (parAction.Bonus == ActionBonus.YES && parMatch.isVisitor(locPlayerTeam))
            {
                locScoreAtk += ConstManager.BONUS_VALUE;
            }
        }

        return locScoreAtk;
    }

    private void addGoal(Match parMatch, Action parAction)
    {
        if (parAction.Side == ActionSide.LOCAL)
        {
            parMatch._localScore += 1;
        }
        else
        {
            parMatch._visitorScore += 1;
        }
    }

    public void updateMatchScore(Match parMatch) {
		parMatch._isPlayed = true;
		updateTeamStatsModule(parMatch);
		Player locPlayer = GameManager._instance.Game.Player;
		if ((parMatch._localTeam == locPlayer._team) || (parMatch._visitorTeam == locPlayer._team)) {
			PlayerManager.getInstance().updateAfterMatch(locPlayer, parMatch);
		}

        var locDbMgr = DIContainer.DbManager;
        locDbMgr.OpenConnection();

        DIContainer.MatchService.UpdateMatch(parMatch);

        locDbMgr.CloseConnection();
    }
	
	private void initActionScore(Match parMatch) {
        Team locLocalTeam = ChampionshipManager.getInstance().GetTeamById(parMatch._localTeam);
        Team locVisitorTeam = ChampionshipManager.getInstance().GetTeamById(parMatch._visitorTeam);

        locLocalTeam._teamSkills.resetTeamScore();
        locLocalTeam._teamSkills.computeTeamScoreAtk();
        locLocalTeam._teamSkills.computeTeamScoreDef();
        locVisitorTeam._teamSkills.resetTeamScore();
        locVisitorTeam._teamSkills.computeTeamScoreAtk();
        locVisitorTeam._teamSkills.computeTeamScoreDef();
	}
	
	/**
	 * Fonction qui permet de mettre a jour le score d'un match
	 * @param parMatch Match a mettre a jour
	 */
	private void updateTeamStatsModule(Match parMatch) {
        Team locLocalTeam = ChampionshipManager.getInstance().GetTeamById(parMatch._localTeam);
        Team locVisitorTeam = ChampionshipManager.getInstance().GetTeamById(parMatch._visitorTeam);

        if (parMatch._localScore == parMatch._visitorScore) {
            locLocalTeam.updateAllTeamStatsModule(MatchResult.DRAW, parMatch._localScore, parMatch._visitorScore, true);
            locVisitorTeam.updateAllTeamStatsModule(MatchResult.DRAW,parMatch._visitorScore, parMatch._localScore, false);
		} else {
			if (parMatch._localScore > parMatch._visitorScore) {
                locLocalTeam.updateAllTeamStatsModule(MatchResult.VICTORY, parMatch._localScore, parMatch._visitorScore, true);
                locVisitorTeam.updateAllTeamStatsModule(MatchResult.LOSE,parMatch._visitorScore, parMatch._localScore, false);
			} else {
                locLocalTeam.updateAllTeamStatsModule(MatchResult.LOSE, parMatch._localScore, parMatch._visitorScore, true);
                locVisitorTeam.updateAllTeamStatsModule(MatchResult.VICTORY,parMatch._visitorScore, parMatch._localScore, false);
			}
		}
	}
	
	private void setMatchTactics(Match parMatch) {
        Team locLocalTeam = ChampionshipManager.getInstance().GetTeamById(parMatch._localTeam);
        Team locVisitorTeam = ChampionshipManager.getInstance().GetTeamById(parMatch._visitorTeam);

        locLocalTeam.setMatchTactic(locVisitorTeam, true);
        locVisitorTeam.setMatchTactic(locLocalTeam, false);
	}
	
	public Team getOpponent(Match parMatch, Team parCurTeam) {
        Team locLocalTeam = ChampionshipManager.getInstance().GetTeamById(parMatch._localTeam);
        Team locVisitorTeam = ChampionshipManager.getInstance().GetTeamById(parMatch._visitorTeam);

        if (locLocalTeam == parCurTeam) {
			return locVisitorTeam;
		} else {
			return locLocalTeam;
		}
	}
	
	public MatchResult getPronostic(Match parMatch, Team parCurTeam) {
        Team locLocalTeam = ChampionshipManager.getInstance().GetTeamById(parMatch._localTeam);
        Team locVisitorTeam = ChampionshipManager.getInstance().GetTeamById(parMatch._visitorTeam);

        bool locIsLocal = locLocalTeam == parCurTeam;
		
		if (locIsLocal) {
			return locLocalTeam.getPronostic(locVisitorTeam, true);
		} else {
			return locVisitorTeam.getPronostic(locLocalTeam, false);	
		}
	}

    public MatchResult getObjectif(Match parMatch, Team parCurTeam)
    {
        var locProno = getPronostic(parMatch, parCurTeam);

        if (locProno == MatchResult.LOSE)
        {
            return MatchResult.DRAW;
        }

        return locProno;
    }

    public Tactic getAdviseTactic(Match parMatch, Team parCurTeam) {
        Team locLocalTeam = ChampionshipManager.getInstance().GetTeamById(parMatch._localTeam);
        Team locVisitorTeam = ChampionshipManager.getInstance().GetTeamById(parMatch._visitorTeam);

        bool locIsLocal = locLocalTeam == parCurTeam;
		
		if (locIsLocal) {
			return locLocalTeam.getMatchTactic(locVisitorTeam, true);
		} else {
			return locVisitorTeam.getMatchTactic(locLocalTeam, false);	
		}
	}
}
