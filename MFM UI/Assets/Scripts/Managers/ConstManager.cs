﻿using UnityEngine;
using System.Collections.Generic;

public class ConstManager {

	// Grade Const
	public readonly static float[] TAB_GRADE_RATE = {0.95f, 0.90f, 0.85f, 0.80f};
	// TeamStatsModule Const
	private static readonly int LOSE_POINTS = 0;
	private static readonly int DRAW_POINTS = 1;
	private static readonly int VICTORY_POINTS = 3;
	public static readonly int[] TAB_MATCH_RESULT_POINT = {LOSE_POINTS, DRAW_POINTS, VICTORY_POINTS};
	public static readonly string[] TAB_MATCH_RESULT_NAME = {"Défaite", "Match nul", "Victoire"};
	// TeamStatusModule Const
	public static readonly int[] TAB_TEAM_STATUS_RANK_HIGH = {16, 11, 6, 1};
	public static readonly int[] TAB_TEAM_STATUS_RANK_LOW = {20, 15, 10, 5};
	public static readonly int[] TAB_TEAM_STATUS_BOARD_OBJECTIVE = {17, 15, 7, 3};
	public static readonly int STATUS_UPDATE_DAY = 11;
	// PlayerHistoryManager Const
	public static readonly int MAX_MATCH = 20;
    // MatchManager Const
    public static readonly float BONUS_VALUE = 0.05f;
    public static readonly int MAX_ACTIONS_PER_HALF = 7;
    // UI Const
    public static readonly int SWIPE_SPEED = 6;
    public static readonly float MATCH_ACTION_DELAY = 0.5f;
    public static readonly Color32 WHITE_COLOR = new Color32(255, 255, 255, 255);
    public static readonly Color32 RED_LIGHT_COLOR = new Color32(255, 80, 80, 255);
    public static readonly Color32 RED_BASE_COLOR = new Color32(152, 1, 1, 255);
    public static readonly Color32 ORANGE_LIGHT_COLOR = new Color32(250, 150, 70, 255);
    public static readonly Color32 ORANGE_BASE_COLOR = new Color32(251, 139, 0, 255);
    public static readonly Color32 GREEN_BUTTON_COLOR = new Color32(51, 153, 0, 255);
    public static readonly Color32 BLUE_LIGHT_COLOR = new Color32(80, 130, 255, 255);
    // DB Const
    public static readonly string DB_CONNECTION = "URI=file:" + Application.dataPath + "/DB/mfmdb.sqlite3";
}
