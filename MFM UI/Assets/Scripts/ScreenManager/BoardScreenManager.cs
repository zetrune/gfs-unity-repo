﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using System;

class BoardScreenManager : ScreenManager
{

    public GameObject _boardScreen = null;
    public GameObject _matchOverview = null;

    public Text _winTxt = null;
    public Text _drawTxt = null;
    public Text _loseTxt = null;
    public Text _gaTxt = null;
    public Text _gfTxt = null;
    public Text _pronosticTxt = null;
    public Text _objectifTxt = null;
    public Text _tacticTxt = null;
    public Text _rankTxt = null;
    public Image _trustImg = null;
    public Text _rewardTxt = null;

    public Button _applyBtn = null;

    public override void OnShow()
    {

        SetupMainHeader();
        SetupSubHeader();
        SetupFooter();
        SetupBoard();
    }

    public override void OnHide()
    {
        _boardScreen.SetActive(false);
    }

    public override void OnNext()
    {

    }

    public override void OnBack()
    {
        SceneManager._instance.PreviousScreen();
    }

    protected override void SetupMainHeader()
    {
        SetupMainHeader(true, false, false, "Board");
    }

    protected override void SetupSubHeader()
    {
        _subHeader.SetActive(true);

        // Disable left and right text and image
        _subHeader.transform.GetChild(0).gameObject.SetActive(false);
        _subHeader.transform.GetChild(1).gameObject.SetActive(false);
        _subHeader.transform.GetChild(3).gameObject.SetActive(false);
        _subHeader.transform.GetChild(4).gameObject.SetActive(false);

        // Enable subheader title and set the text
        Transform locSubHeaderTxt = _subHeader.transform.GetChild(2);
        locSubHeaderTxt.gameObject.SetActive(false);
    }

    protected override void SetupFooter()
    {
        _footer.SetActive(true);

        // Enable back button
        _footer.transform.GetChild(0).gameObject.SetActive(true);
        _footer.transform.GetChild(0).GetComponent<Button>().onClick.RemoveAllListeners();
        _footer.transform.GetChild(0).GetComponent<Button>().onClick.AddListener(() => OnBack());

        // Disable validate button.
        _footer.transform.GetChild(1).gameObject.SetActive(false);
    }

    public void OnApplyTactic()
    {
        var locPlayerTeam = ChampionshipManager.getInstance().GetTeamById(GameManager._instance.Game.Player._team);
        var locMatch = GameManager._instance.GetCurrentPlayerTeamMatch();
        var locTactic = MatchManager.getInstance().getAdviseTactic(locMatch, locPlayerTeam);

        locPlayerTeam._teamSkills.setTeamTactic(locTactic);
        _applyBtn.gameObject.SetActive(false);
    }

    private void SetupBoard()
    {
        _boardScreen.SetActive(true);
        _matchOverview.SetActive(true);
        _applyBtn.gameObject.SetActive(true);

        _applyBtn.onClick.RemoveAllListeners();
        _applyBtn.onClick.AddListener(() => OnApplyTactic());

        var locPlayerTeam = ChampionshipManager.getInstance().GetTeamById(GameManager._instance.Game.Player._team);
        var locMatch = GameManager._instance.GetCurrentPlayerTeamMatch();
        var locOpponentTeam = MatchManager.getInstance().getOpponent(locMatch, locPlayerTeam);

        var locStatModule = locOpponentTeam._teamGlobalStats;
       
        _winTxt.text = GetPercentStr(locStatModule._rateVictory);
        _drawTxt.text = GetPercentStr(locStatModule._rateDraw);
        _loseTxt.text = GetPercentStr(locStatModule._rateLose);

        _gaTxt.text = GetAverageStr(locStatModule._averageGoalConceded);
        _gfTxt.text = GetAverageStr(locStatModule._averageGoalScored);

        var locMatchResult = MatchManager.getInstance().getPronostic(locMatch, locPlayerTeam);
        _pronosticTxt.text = GetMatchResultStr(locMatchResult);
        _pronosticTxt.color = GetColorTxt((int)locMatchResult);

        var locTactic = MatchManager.getInstance().getAdviseTactic(locMatch, locPlayerTeam);
        _tacticTxt.text = GetTacticStr(locTactic);
        _tacticTxt.color = GetTacticColor(locTactic);

        var locObjectif = MatchManager.getInstance().getObjectif(locMatch, locPlayerTeam);
        _objectifTxt.text = GetMatchResultStr(locObjectif);

        _rankTxt.text = locPlayerTeam._teamStatus.getBoardObjective().ToString();

        var locBoardTrust = GameManager._instance.Game.Player._boardTrust;
        _trustImg.color = GetColorTxt(locBoardTrust);
    }

    private string GetTacticStr(Tactic parTactic)
    {
        string result = "";

        switch (parTactic)
        {
            case Tactic.DEF:
                result = "Defensive";
                break;
            case Tactic.NEUTRAL:
                result = "Neutral";
                break;
            case Tactic.OFF:
                result = "Offensive";
                break;
        }

        return result;
    }

    private string GetMatchResultStr(MatchResult parResult)
    {
        string result = "";
        switch (parResult)
        {
            case MatchResult.LOSE:
                result = "Lose";
                break;
            case MatchResult.VICTORY:
                result = "Win";
                break;
            case MatchResult.DRAW:
                result = "Draw";
                break;
        }

        return result;
    }

    private string GetPercentStr(float parRate)
    {
        return parRate != 0f ? parRate.ToString("#%") : "0%";
    }

    private string GetAverageStr(float parAvg)
    {
        return parAvg != 0f ? parAvg.ToString("#.#") : "0";
    }

    private Color32 GetColorTxt(int parValue)
    {
        Color32 locColor = ConstManager.WHITE_COLOR;

        switch (parValue)
        {
            case 0:
                locColor = ConstManager.RED_LIGHT_COLOR;
                break;
            case 1:
                locColor = ConstManager.ORANGE_BASE_COLOR;
                break;
            case 2:
                locColor = ConstManager.GREEN_BUTTON_COLOR;
                break;
        }

        return locColor;
    }

    private Color32 GetTacticColor(Tactic parTactic)
    {
        Color32 locColor = ConstManager.WHITE_COLOR;

        switch (parTactic)
        {
            case Tactic.DEF:
                locColor = ConstManager.BLUE_LIGHT_COLOR;
                break;
            case Tactic.NEUTRAL:
                locColor = ConstManager.ORANGE_BASE_COLOR;
                break;
            case Tactic.OFF:
                locColor = ConstManager.RED_BASE_COLOR;
                break;
        }

        return locColor;
    }

}

