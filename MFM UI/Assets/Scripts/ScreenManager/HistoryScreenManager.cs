﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class HistoryScreenManager : ScreenManager {

	public GameObject _historyListScrollView;
	public GameObject _matchItemPrefab;
	
	public override void OnShow() {
		SetupMainHeader ();
		SetupSubHeader ();
		SetupFooter ();
		SetupListScrollView ();
	}
	
	public override void OnHide () {
		_historyListScrollView.SetActive (false);
	}
	
	public override void OnNext () {
		
	}
	
	public override void OnBack() {
        SceneManager._instance.PreviousScreen();
    }
	
	
	protected override void SetupMainHeader() {
        SetupMainHeader(true, false, false, "History");
	}
	
	protected override void SetupSubHeader() {
		_subHeader.SetActive (true);
		
		// Disable left and right text and image
		_subHeader.transform.GetChild (0).gameObject.SetActive (false);
		_subHeader.transform.GetChild (1).gameObject.SetActive (false);
		_subHeader.transform.GetChild (3).gameObject.SetActive (false);
		_subHeader.transform.GetChild (4).gameObject.SetActive (false);
		
		// Enable subheader title and set the text
		Transform locSubHeaderTxt = _subHeader.transform.GetChild (2);
		locSubHeaderTxt.gameObject.SetActive (true);
		locSubHeaderTxt.GetComponent<Text> ().text = "Player history";
	}
	
	protected override void SetupFooter() {
		_footer.SetActive (true);

        // Enable back button
        _footer.transform.GetChild(0).gameObject.SetActive (true);
        _footer.transform.GetChild(0).GetComponent<Button>().onClick.RemoveAllListeners();
        _footer.transform.GetChild(0).GetComponent<Button>().onClick.AddListener(() => OnBack());
        // Disable next buttons
        _footer.transform.GetChild(1).gameObject.SetActive (false);
	}
	
	private void SetupListScrollView() {
		_historyListScrollView.SetActive (true);
		
		
		// Cleanup the list
		Transform locList = _historyListScrollView.transform.GetChild (0);
		
		foreach (Transform locTmpChild in locList) {
			GameObject.Destroy(locTmpChild.gameObject);
		}
		
		// Get the history of match for the player
		List<Match> locPlayerMatchHistoryTab = GameManager._instance.Game.Player._historyModule._matchHistory;
		
		
		// Fill the list with the match in the player history
		foreach (Match locTmpMatch in locPlayerMatchHistoryTab) {
			GameObject locMatchItem = (GameObject) Instantiate(_matchItemPrefab);
			locMatchItem.transform.SetParent(_historyListScrollView.transform.GetChild(0));
			locMatchItem.transform.localScale = new Vector3(1, 1, 1);
			
			CalendarMatchItem locCalendarItem = locMatchItem.GetComponent<CalendarMatchItem>();
			locCalendarItem.SetMatchInfos(locTmpMatch);
		}
	}
}
