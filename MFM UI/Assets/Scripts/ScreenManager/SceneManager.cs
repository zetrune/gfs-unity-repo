﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SceneManager : MonoBehaviour {

	private ScreenManager _screenManager;
	private ScreenManager _homeScreen;
	private ScreenManager _settingsScreen;

	// Managers
	public static SceneManager _instance = null;

	private ScreenManager _previousScreen = null;

	void Awake() {
		if (_instance == null)
			_instance = this;
		else if (_instance != this)
			Destroy (gameObject);
	}

	public void SetScreenManager(ScreenManager screenManager) {
		if (_screenManager != null) {
			_screenManager.OnHide();
            _previousScreen = _screenManager;
        }

		_screenManager = screenManager;

		if (_screenManager != null) {
			_screenManager.OnShow ();
		}
	}

	public void SetHome(ScreenManager parHome) {
		_homeScreen = parHome;
	}

	public void SetSettings(ScreenManager parSettings) {
		_settingsScreen = parSettings;
	}

	public void PreviousScreen() {
        if (_previousScreen != null)
        {
            SetScreenManager(_previousScreen);
        }
	}

	public void GoHome() {
            SetScreenManager(_homeScreen);
    }

	public void GoSettings() {
		SetScreenManager (_settingsScreen);
	}
}
