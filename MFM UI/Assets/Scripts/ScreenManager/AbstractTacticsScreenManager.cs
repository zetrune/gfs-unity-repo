﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public abstract class AbstractTacticsScreenManager : ScreenManager {

	public GameObject _tacticView;
	public GameObject _tacticPrefab;
	public NavIndicator _navIndicator;
	public StatsOverview _statsOverview;
	
	void Awake() {
	}
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	public override void OnShow() {
		
		SetupMainHeader ();
		SetupSubHeader ();
		SetupFooter ();
		SetupTacticView ();
	}

    public override void OnHide () {
		_tacticView.SetActive (false);
		
		_footer.transform.GetChild (0).GetComponent<Button> ().onClick.RemoveAllListeners ();
	}
	
	public override void OnNext () {
		
	}
	
	public override void OnBack() {
		SceneManager._instance.PreviousScreen ();
	}
	
	protected override void SetupSubHeader() {
		_subHeader.SetActive (true);
		
		// Disable left and right text and image
		_subHeader.transform.GetChild (0).gameObject.SetActive (false);
		_subHeader.transform.GetChild (1).gameObject.SetActive (false);
		_subHeader.transform.GetChild (3).gameObject.SetActive (false);
		_subHeader.transform.GetChild (4).gameObject.SetActive (false);
		
		// Enable subheader title and set the text
		UpdateSubHeaderTxt ();
	}
	
	protected override void SetupFooter() {
		_footer.SetActive (true);
		
		// Enable back button
		_footer.transform.GetChild (0).gameObject.SetActive (true);
        _footer.transform.GetChild(0).GetComponent<Button>().onClick.RemoveAllListeners();
        _footer.transform.GetChild (0).GetComponent<Button> ().onClick.AddListener (() => OnBack ());
		
		// Disable validate button
		_footer.transform.GetChild (1).gameObject.SetActive (false);
	}
	
	public void SwipeUpdate(GameObject parNewObject, int parPosition) {
		_navIndicator.SetPosition (parPosition);
		
		Team locTeam = ChampionshipManager.getInstance().GetTeamById(GameManager._instance.Game.Player._team);
		TeamSkillsModule locTeamSkillModule = locTeam._teamSkills;

		SetIndexPosition (parPosition);

		locTeamSkillModule.computeTeamScoreAtk ();
		locTeamSkillModule.computeTeamScoreDef ();

		SetupTacticsImg(parNewObject, GetTeamSkill());
		UpdateSubHeaderTxt ();
		_statsOverview.setGauge (locTeam);
		
	}

	private void UpdateSubHeaderTxt() {
		Transform locSubHeaderTxt = _subHeader.transform.GetChild (2);
		locSubHeaderTxt.gameObject.SetActive (true);
		locSubHeaderTxt.GetComponent<Text> ().text = GetTeamSkill().tostring();
	}
	
	protected void SetupTacticView(){
		_tacticView.SetActive(true);
		
		Transform locCarousel = _tacticView.transform.GetChild(0);
		
		foreach (Transform locTmpChild in locCarousel) {
			GameObject.Destroy(locTmpChild.gameObject);
		}

        Team locTeam = ChampionshipManager.getInstance().GetTeamById(GameManager._instance.Game.Player._team);
        TeamSkill locTeamSkill = GetTeamSkill();
		
		_navIndicator.SetNbItem (GetCptSkills());
		_navIndicator.SetPosition (GetSkillIndex());
		
		Swipe locSwipe = locCarousel.GetComponent<Swipe>();
		locSwipe._swipeHandler = SwipeUpdate;
		locSwipe._itemCpt = GetCptSkills();
		locSwipe._currentPosition = GetSkillIndex();
		locSwipe._itemPrefab = _tacticPrefab;
        locSwipe._swipeEnable = true;

        GameObject locObject = locSwipe.CreateObject (Side.Center, GetSkillIndex());
        SetupTacticsImg(locObject, locTeamSkill);
		_statsOverview.setGauge (locTeam);
	}
	
	private void SetupTacticsImg(GameObject parNewObject, TeamSkill parTeamSkill) {
		string locTextureName = parTeamSkill.getTextureName ();
		
		parNewObject.GetComponent<Image>().sprite =  Resources.Load<Sprite>("Tactics/" + locTextureName);
	}


	// ABSTRACT METHODS

	protected abstract void SetIndexPosition (int parPosition);
	
	protected abstract TeamSkill GetTeamSkill ();

	protected abstract int GetCptSkills ();
	
	protected abstract int GetSkillIndex ();
}
