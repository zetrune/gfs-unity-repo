﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class PassScreenManager : AbstractTacticsScreenManager
{

    void Awake()
    {
    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    protected override void SetupMainHeader()
    {
        SetupMainHeader(true, false, false, "Pass");
    }

    protected override void SetIndexPosition(int parPosition)
    {
        Team locTeam = ChampionshipManager.getInstance().GetTeamById(GameManager._instance.Game.Player._team);
        TeamSkillsModule locTeamSkillModule = locTeam._teamSkills;
        locTeamSkillModule._teamPassIndex = parPosition;
    }

    protected override int GetSkillIndex()
    {
        Team locTeam = ChampionshipManager.getInstance().GetTeamById(GameManager._instance.Game.Player._team);
        TeamSkillsModule locTeamSkillModule = locTeam._teamSkills;
        return locTeamSkillModule._teamPassIndex;
    }

    protected override TeamSkill GetTeamSkill()
    {
        Team locTeam = ChampionshipManager.getInstance().GetTeamById(GameManager._instance.Game.Player._team);
        TeamSkillsModule locTeamSkillModule = locTeam._teamSkills;
        return locTeamSkillModule._tabTeamPass[GetSkillIndex()];
    }

    protected override int GetCptSkills()
    {
        Team locTeam = ChampionshipManager.getInstance().GetTeamById(GameManager._instance.Game.Player._team);
        TeamSkillsModule locTeamSkillModule = locTeam._teamSkills;
        return locTeamSkillModule._tabTeamPass.Count;
    }
}
