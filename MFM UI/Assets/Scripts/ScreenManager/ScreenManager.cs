﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public abstract class ScreenManager : MonoBehaviour {

	public GameObject _mainHeader;
	public GameObject _subHeader;
	public GameObject _footer;

	// Use this for initialization
	void Start () {
	
	}

	public abstract void OnShow();

	public abstract void OnHide ();

	public abstract void OnNext ();
	
	public abstract void OnBack();

	public void OnHome () {
		SceneManager._instance.GoHome ();
	}

	public void OnSettings () {
		SceneManager._instance.GoSettings ();
	}

    protected abstract void SetupMainHeader();

    protected void SetupMainHeader(bool parEnableHeader, bool parEnableHome, bool parEnableSetting, string parHeaderText)
    {
        if (parEnableHeader)
        {
            _mainHeader.SetActive(true);

            // Enable header title and set the text
            Transform locHeaderTxt = _mainHeader.transform.GetChild(1);
            locHeaderTxt.gameObject.SetActive(true);
            locHeaderTxt.GetComponent<Text>().text = parHeaderText;

            if (parEnableHome)
            {
                // Enable Home
                _mainHeader.transform.GetChild(0).gameObject.SetActive(true);
                _mainHeader.transform.GetChild(0).GetComponent<Button>().onClick.RemoveAllListeners();
                _mainHeader.transform.GetChild(0).GetComponent<Button>().onClick.AddListener(() => OnHome());
            } else {
                // Disable Home
                _mainHeader.transform.GetChild(0).gameObject.SetActive(false);
            }

            if (parEnableSetting)
            {
                // Enable Setting
                _mainHeader.transform.GetChild(2).gameObject.SetActive(true);
                _mainHeader.transform.GetChild(2).GetComponent<Button>().onClick.RemoveAllListeners();
                _mainHeader.transform.GetChild(2).GetComponent<Button>().onClick.AddListener(() => OnSettings());
            }
            else {
                // Disable Setting
                _mainHeader.transform.GetChild(2).gameObject.SetActive(false);
            }

        } else {
            _mainHeader.SetActive(false);
        }

    }

	protected abstract void SetupSubHeader();

	protected abstract void SetupFooter();


}
