﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class DashboardScreenManager : ScreenManager {
	
	public GameObject _matchOverview;
	public GameObject _dashboardButtons;
	public GameObject _tacticsButtons;
	public GameObject _statsOverview;

	public ScreenManager _calendarScreenManager;
	public ScreenManager _rankingScreenManager;
	public ScreenManager _historyScreenManager;
	public ScreenManager _formationScreenManager;
    public ScreenManager _positionScreenManager;
    public ScreenManager _playScreenManager;
    public ScreenManager _passScreenManager;
    public ScreenManager _pressingScreenManager;
    public ScreenManager _markingScreenManager;
    public ScreenManager _matchScreenManager;
    public ScreenManager _boardScreenManager;

    // Use this for initialization
    void Start () {
		SceneManager._instance.SetScreenManager (this);
		SceneManager._instance.SetHome (this);
	}
	
	public override void OnShow() {
        SetupMainHeader();
        SetupSubHeader ();
		SetupMatchOverview ();
		SetupDashboardButtons ();
		SetupTacticsButtons ();
		SetupStatsOverview ();
		SetupFooter ();
	}
	
	public override void OnHide () {
		_matchOverview.SetActive (false);
		_dashboardButtons.SetActive (false);
		_tacticsButtons.SetActive (false);
		_statsOverview.SetActive (false);
	}
	
	public override void OnNext () {
        OnPlayMatch();
	}
	
	public override void OnBack() {
		
	}

	public void OnCalendar() {
		SceneManager._instance.SetScreenManager (_calendarScreenManager);
	}

	public void OnRanking() {
		SceneManager._instance.SetScreenManager (_rankingScreenManager);
	}

    public void OnBoard()
    {
        SceneManager._instance.SetScreenManager(_boardScreenManager);
    }

	public void OnFormation() {
		SceneManager._instance.SetScreenManager (_formationScreenManager);
	}

    public void OnPosition()
    {
        SceneManager._instance.SetScreenManager(_positionScreenManager);
    }

    public void OnPlay()
    {
        SceneManager._instance.SetScreenManager(_playScreenManager);
    }

    public void OnPass()
    {
        SceneManager._instance.SetScreenManager(_passScreenManager);
    }

    public void OnPressing()
    {
        SceneManager._instance.SetScreenManager(_pressingScreenManager);
    }

    public void OnMarking()
    {
        SceneManager._instance.SetScreenManager(_markingScreenManager);
    }

    public void OnHistory() {
		SceneManager._instance.SetScreenManager (_historyScreenManager);
	}

    public void OnPlayMatch() {
        SceneManager._instance.SetScreenManager(_matchScreenManager);
    }

    protected override void SetupMainHeader()
    {
        SetupMainHeader(true, false, false, "Dashboard");
    }

    protected override void SetupSubHeader() {
		_subHeader.SetActive (true);
		
		// Disable right text and image
		_subHeader.transform.GetChild (3).gameObject.SetActive (false);
		_subHeader.transform.GetChild (4).gameObject.SetActive (false);

        // Enable subheader player gamepoint
        _subHeader.transform.GetChild(0).gameObject.SetActive(true); // GamePoint image
        Transform locSubHeaderGamePointTxt = _subHeader.transform.GetChild(1);
        locSubHeaderGamePointTxt.gameObject.SetActive(true);
        locSubHeaderGamePointTxt.GetComponent<Text>().text = "" + (GameManager._instance.Game.Player._gamePoints);

        // Enable subheader title and set the text
        Transform locSubHeaderTxt = _subHeader.transform.GetChild (2);
		locSubHeaderTxt.gameObject.SetActive (true);
		locSubHeaderTxt.GetComponent<Text> ().text = "Day " + (GameManager._instance.Game.JourneyIndex + 1);
	}
	
	protected override void SetupFooter() {
		_footer.SetActive (true);
		
		// Disable cancel button
		_footer.transform.GetChild (0).gameObject.SetActive (false);

        // Enable validate button
		_footer.transform.GetChild (1).gameObject.SetActive (true);
        _footer.transform.GetChild(1).transform.GetChild(0).GetComponent<Text>().text = "Play match";
        _footer.transform.GetChild(1).GetComponent<Button>().onClick.RemoveAllListeners();
        _footer.transform.GetChild(1).GetComponent<Button>().onClick.AddListener(() => OnNext());
    }

	
	private void SetupMatchOverview() {
		_matchOverview.SetActive (true);
		Match locCurrentMatch = GameManager._instance.GetCurrentPlayerTeamMatch();
		_matchOverview.GetComponent<MatchOverview> ().SetMatchOverview (locCurrentMatch);
	}

	private void SetupDashboardButtons() {
		_dashboardButtons.SetActive (true);
		_dashboardButtons.GetComponent<DashboardButtons> ().UpdateDashboardButtons();
	}

	private void SetupTacticsButtons() {
		_tacticsButtons.SetActive (true);
	}

	private void SetupStatsOverview() {
		_statsOverview.SetActive (true);
        Team locTeam = ChampionshipManager.getInstance().GetTeamById(GameManager._instance.Game.Player._team);
		_statsOverview.GetComponent<StatsOverview> ().setGauge (locTeam);
	}
}
