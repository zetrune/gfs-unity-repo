﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class ChampionshipSelectionScreenManager : ScreenManager {
	
	public GameObject _gridScrollView;
	public ScreenManager _teamSelectionScreen;

	private ChampionshipType _champType;

	void Awake() {
	}

	// Use this for initialization
	void Start () {
		_champType = ChampionshipType.NONE;

		SceneManager._instance.SetScreenManager (this);
	}

	public override void OnShow() {
        SetupMainHeader();
        SetupSubHeader ();
		SetupFooter ();
		SetupGridScrollView ();

	}
	
	public override void OnHide () {
		_gridScrollView.SetActive (false);
	}
	
	public override void OnNext () {
		switch (_champType)
		{
		case ChampionshipType.NONE:
			return;
		case ChampionshipType.ENGLISH:
			GameManager._instance.createEnglishChampionship();
			break;
		case ChampionshipType.FRENCH:
			GameManager._instance.createFrenchChampionship();
			break;
		case ChampionshipType.GERMAN:
			GameManager._instance.createGermanChampionship();
			break;
		case ChampionshipType.ITALIAN:
			GameManager._instance.createItalianChampionship();
			break;
		case ChampionshipType.SPANISH:
			GameManager._instance.createSpanishChampionship();
			break;
		default:
			return;
		}

        GameManager._instance.SetChampionship(GameManager._instance.Game.Championship);
        SceneManager._instance.SetScreenManager (_teamSelectionScreen);
	}
	
	public override void OnBack() {
	}

	public void SetChampionship(int parType) {
		_champType = (ChampionshipType) parType;
		
		// Enable Valid btn
		if (_champType != ChampionshipType.NONE) {

			_footer.transform.GetChild (1).gameObject.SetActive (true);
		}
	}

    protected override void SetupMainHeader()
    {
        SetupMainHeader(true, false, false, "Championship Selection");
    }

    protected override void SetupSubHeader() {
		_subHeader.SetActive (true);

		// Disable left and right text and image
		_subHeader.transform.GetChild (0).gameObject.SetActive (false);
		_subHeader.transform.GetChild (1).gameObject.SetActive (false);
		_subHeader.transform.GetChild (3).gameObject.SetActive (false);
		_subHeader.transform.GetChild (4).gameObject.SetActive (false);

		// Enable subheader title and set the text
		Transform locSubHeaderTxt = _subHeader.transform.GetChild (2);
		locSubHeaderTxt.gameObject.SetActive (true);
		locSubHeaderTxt.GetComponent<Text> ().text = "Select a championship";
	}
	
	protected override void SetupFooter() {
		_footer.SetActive (true);

		// Disable both buttons.
		_footer.transform.GetChild (0).gameObject.SetActive (false);
		_footer.transform.GetChild (1).gameObject.SetActive (false);
	}

	private void SetupGridScrollView() {
		_gridScrollView.SetActive (true);
	}
}
