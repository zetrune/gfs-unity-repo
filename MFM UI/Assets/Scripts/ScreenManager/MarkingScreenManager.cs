﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class MarkingScreenManager : AbstractTacticsScreenManager
{

    void Awake()
    {
    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    protected override void SetupMainHeader()
    {
        SetupMainHeader(true, false, false, "Marking");
    }

    protected override void SetIndexPosition(int parPosition)
    {
        Team locTeam = ChampionshipManager.getInstance().GetTeamById(GameManager._instance.Game.Player._team);
        TeamSkillsModule locTeamSkillModule = locTeam._teamSkills;
        locTeamSkillModule._teamMarkingIndex = parPosition;
    }

    protected override int GetSkillIndex()
    {
        Team locTeam = ChampionshipManager.getInstance().GetTeamById(GameManager._instance.Game.Player._team);
        TeamSkillsModule locTeamSkillModule = locTeam._teamSkills;
        return locTeamSkillModule._teamMarkingIndex;
    }

    protected override TeamSkill GetTeamSkill()
    {
        Team locTeam = ChampionshipManager.getInstance().GetTeamById(GameManager._instance.Game.Player._team);
        TeamSkillsModule locTeamSkillModule = locTeam._teamSkills;
        return locTeamSkillModule._tabTeamMarking[GetSkillIndex()];
    }

    protected override int GetCptSkills()
    {
        Team locTeam = ChampionshipManager.getInstance().GetTeamById(GameManager._instance.Game.Player._team);
        TeamSkillsModule locTeamSkillModule = locTeam._teamSkills;
        return locTeamSkillModule._tabTeamMarking.Count;
    }
}
