﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class MatchScreenManager : ScreenManager {

    public GameObject _matchOverview;
    public GameObject _matchActionList;
    public GameObject _actionsList;
    public GameObject _leftActionPrefab;
    public GameObject _rightActionPrefab;
    public GameObject _actionSeparatorPrefab;
    public GameObject _bonusSelector;
    
    private Match _match;

    public override void OnShow()
    {
        _match = GameManager._instance.GetCurrentPlayerTeamMatch();

        SetupMainHeader();
        SetupSubHeader();
        SetupFooter();
        SetupMatchOverview();
        SetupBonusSelector();
        SetupActionList();
    }


    public override void OnHide()
    {
        _footer.transform.GetChild(1).GetComponent<Button>().onClick.RemoveAllListeners();
        _matchActionList.SetActive(false);
        DisableBonusSelector();
    }

    private void DisableBonusSelector()
    {
        var locAddBtn = _bonusSelector.transform.GetChild(0);
        var locRemoveBtn = _bonusSelector.transform.GetChild(1);

        locAddBtn.GetComponent<Button>().onClick.RemoveAllListeners();
        locRemoveBtn.GetComponent<Button>().onClick.RemoveAllListeners();

        _bonusSelector.SetActive(false);
    }

    public override void OnNext()
    {
        // Play all other match of the day goes to next journey
        ChampionshipManager.getInstance().endJourney(GameManager._instance.Game.JourneyIndex);
        GameManager._instance.Game.JourneyIndex += 1;
        ChampionshipManager.getInstance().initJourney(GameManager._instance.Game.JourneyIndex);
        GameManager._instance.SaveGame();
        // Return to dashboard
        SceneManager._instance.GoHome();
    }

    public override void OnBack()
    {
        SceneManager._instance.PreviousScreen();
    }

    protected override void SetupMainHeader()
    {
        SetupMainHeader(true, false, false, "Match");
    }

    protected override void SetupSubHeader()
    {
        _subHeader.SetActive(true);

        // Disable right text and image
        _subHeader.transform.GetChild(3).gameObject.SetActive(false);
        _subHeader.transform.GetChild(4).gameObject.SetActive(false);

        // Enable subheader player gamepoint
        _subHeader.transform.GetChild(0).gameObject.SetActive(true); // GamePoint image
        Transform locSubHeaderGamePointTxt = _subHeader.transform.GetChild(1);
        locSubHeaderGamePointTxt.gameObject.SetActive(true);
        locSubHeaderGamePointTxt.GetComponent<Text>().text = "" + (GameManager._instance.Game.Player._gamePoints);

        // Enable subheader title and set the text
        Transform locSubHeaderTxt = _subHeader.transform.GetChild(2);
        locSubHeaderTxt.gameObject.SetActive(true);

        if (_match._actions.Count <= ConstManager.MAX_ACTIONS_PER_HALF)
        {
            locSubHeaderTxt.GetComponent<Text>().text = "First Half";
        }
        else if (_match._actions.Count > ConstManager.MAX_ACTIONS_PER_HALF && _match._actions.Count < ConstManager.MAX_ACTIONS_PER_HALF * 2)
        {
            locSubHeaderTxt.GetComponent<Text>().text = "Second Half";
        }
        else if (_match._actions.Count == ConstManager.MAX_ACTIONS_PER_HALF * 2)
        {
            locSubHeaderTxt.GetComponent<Text>().text = "Match Finished";
        }

    }

    protected override void SetupFooter()
    {
        _footer.SetActive(true);

        // Enable back button
        _footer.transform.GetChild(0).gameObject.SetActive(true);
        _footer.transform.GetChild(0).GetComponent<Button>().onClick.RemoveAllListeners();
        _footer.transform.GetChild(0).GetComponent<Button>().onClick.AddListener(() => OnBack());

        // Enable validate bbtton
        _footer.transform.GetChild(1).gameObject.SetActive(true);

        _footer.transform.GetChild(1).GetComponent<Button>().onClick.RemoveAllListeners();

        if (_match._actions.Count <= ConstManager.MAX_ACTIONS_PER_HALF)
        {
            _footer.transform.GetChild(1).transform.GetChild(0).GetComponent<Text>().text = "Play";
            _footer.transform.GetChild(1).GetComponent<Button>().onClick.AddListener(() => OnStartFirstHalf());
        }
        else if (_match._actions.Count > ConstManager.MAX_ACTIONS_PER_HALF && _match._actions.Count < ConstManager.MAX_ACTIONS_PER_HALF * 2)
        {
            _footer.transform.GetChild(1).transform.GetChild(0).GetComponent<Text>().text = "Play";
            _footer.transform.GetChild(1).GetComponent<Button>().onClick.AddListener(() => OnStartSecondHalf());
        }
        else if (_match._actions.Count == ConstManager.MAX_ACTIONS_PER_HALF * 2)
        {
            _footer.transform.GetChild(1).transform.GetChild(0).GetComponent<Text>().text = "Next";
            _footer.transform.GetChild(1).GetComponent<Button>().onClick.AddListener(() => OnNext());
        }
    }

    private void DisableButton()
    {
        _footer.transform.GetChild(0).gameObject.SetActive(false);
        _footer.transform.GetChild(1).gameObject.SetActive(false);
        _bonusSelector.gameObject.GetComponent<BonusSelector>().DisableButton();

        var locDbMgr = DIContainer.DbManager;
        locDbMgr.OpenConnection();

        DIContainer.PlayerService.UpdatePlayer(GameManager._instance.Game.Player);

        locDbMgr.CloseConnection();
    }

    private void SetupHalfMatch()
    {
        DisableButton();

        if (!_match._firstHalfIsPlayed && _match._actions.Count < ConstManager.MAX_ACTIONS_PER_HALF
            || _match._firstHalfIsPlayed && _match._actions.Count < ConstManager.MAX_ACTIONS_PER_HALF * 2)
        {
            DelayAction();
        }

        SetupSubHeader();
        SetupMatchOverview();
    }

    private void DelayAction()
    {
        AddActionSeparator(false);

        MatchManager.getInstance().CreateAction(_match);
        var locAction = _match.LastAction();
        var locLocalTeam = ChampionshipManager.getInstance().GetTeamById(_match._localTeam);
        var locVisitorTeam = ChampionshipManager.getInstance().GetTeamById(_match._visitorTeam);

        GameObject locMatchItem = null;

        if (locAction.Side == ActionSide.LOCAL)
        {
            locMatchItem = (GameObject)Instantiate(_leftActionPrefab);
            locMatchItem.GetComponent<ActionItem>().Setup(locAction, locLocalTeam);
        }
        else
        {
            locMatchItem = (GameObject)Instantiate(_rightActionPrefab);
            locMatchItem.GetComponent<ActionItem>().Setup(locAction, locVisitorTeam);
        }

        var locBonus = locMatchItem.transform.GetChild(1);

        if (_match._availableBonus - _match._usedBonus > 0)
        {
            var locBonusBtn = locBonus.GetComponent<Button>();
            locBonusBtn.onClick.AddListener(() => OnClickBonus());
        }
        else
        {
            locBonus.gameObject.SetActive(false);
        }
        

        locMatchItem.transform.SetParent(_actionsList.transform);
        locMatchItem.transform.localScale = new Vector3(1, 1, 1);
        locMatchItem.transform.SetAsFirstSibling();


        Invoke("ComputeBonus", ConstManager.MATCH_ACTION_DELAY);
    }

    private void ComputeBonus()
    {
        var locMatchItem = _actionsList.transform.GetChild(0).gameObject;
        var locBonus = locMatchItem.transform.GetChild(1);
        var locActionBonusAnimation = locBonus.GetComponent<ActionBonusAnimation>();

        locActionBonusAnimation.EnableAnimation();

        Invoke("ComputeActionDef1", ConstManager.MATCH_ACTION_DELAY);
    }

    public void OnClickBonus()
    {
        var locAction = _match.LastAction();
        locAction.Bonus = ActionBonus.YES;

        var locMatchItem = _actionsList.transform.GetChild(0).gameObject;
        var locBonus = locMatchItem.transform.GetChild(1);
        var locBonusMask = locBonus.GetChild(0);

        var locBonusBtn = locBonus.GetComponent<Button>();
        locBonusBtn.onClick.RemoveAllListeners();


        locBonusMask.gameObject.SetActive(false);
        _bonusSelector.GetComponent<BonusSelector>().OnUseBonus();
        _match._usedBonus++;
    }

    private void ComputeActionDef1()
    {
        var locAction = _match.LastAction();
        var locMatchItem = _actionsList.transform.GetChild(0).gameObject;

        if (locAction.Bonus == ActionBonus.UNKNOWN)
        {
            locAction.Bonus = ActionBonus.NO;
            var locBonus = locMatchItem.transform.GetChild(1);
            locBonus.gameObject.SetActive(false);
        }

        MatchManager.getInstance().ComputeActionDef(_match);
        locMatchItem.GetComponent<ActionItem>().UpdateAction(locAction);

        Invoke("ComputeActionAtk1", ConstManager.MATCH_ACTION_DELAY);
    }

    private void ComputeActionAtk1()
    {
        var locAction = _match.LastAction();
        var locMatchItem = _actionsList.transform.GetChild(0).gameObject;

        MatchManager.getInstance().ComputeActionAtk(_match);
        locMatchItem.GetComponent<ActionItem>().UpdateAction(locAction);

        Invoke("ComputeActionAtk2", ConstManager.MATCH_ACTION_DELAY);
    }

    private void ComputeActionAtk2()
    {
        var locAction = _match.LastAction();
        var locMatchItem = _actionsList.transform.GetChild(0).gameObject;

        locMatchItem.GetComponent<ActionItem>().UpdateAction(locAction);
        MatchManager.getInstance().ComputeActionAtk(_match);

        Invoke("ComputeActionDef2", ConstManager.MATCH_ACTION_DELAY);
    }

    private void ComputeActionDef2()
    {
        var locAction = _match.LastAction();
        var locMatchItem = _actionsList.transform.GetChild(0).gameObject;

        MatchManager.getInstance().ComputeActionDef(_match);
        locMatchItem.GetComponent<ActionItem>().UpdateAction(locAction);

        Invoke("ComputeActionEnd", ConstManager.MATCH_ACTION_DELAY);
    }


    private void ComputeActionEnd()
    {
        if (!_match._firstHalfIsPlayed && _match._actions.Count < ConstManager.MAX_ACTIONS_PER_HALF
            || _match._firstHalfIsPlayed && _match._actions.Count < ConstManager.MAX_ACTIONS_PER_HALF * 2)
        {
            Invoke("DelayAction", ConstManager.MATCH_ACTION_DELAY);
            _matchOverview.GetComponent<MatchOverview>().SetScorePanel(_match);
        }
        else
        {
            if (!_match._firstHalfIsPlayed)
            {
                _matchOverview.GetComponent<MatchOverview>().SetScorePanel(_match);
                _match._firstHalfIsPlayed = true;

                AddActionSeparator(false);
                AddActionSeparator(true);
            }
            else
            {
                _matchOverview.GetComponent<MatchOverview>().SetScorePanel(_match);
                _match._isPlayed = true;

                AddActionSeparator(false);
                AddActionSeparator(true);
            }

            SetupFooter();
        }

        var locDbMgr = DIContainer.DbManager;
        locDbMgr.OpenConnection();

        DIContainer.MatchService.UpdateMatch(_match);

        locDbMgr.CloseConnection();
    }

    private void OnStartFirstHalf()
    {
        SetupHalfMatch();
    }

    private void OnStartSecondHalf()
    {
        SetupHalfMatch();
    }

    private void SetupMatchOverview()
    {
        if (_match._actions.Count == 0)
        {
            foreach (Transform locTmpChild in _actionsList.transform)
            {
                GameObject.Destroy(locTmpChild.gameObject);
            }
        }

        _matchOverview.SetActive(true);
        _matchOverview.GetComponent<MatchOverview>().SetMatchOverview(_match);
        _matchActionList.SetActive(true);
    }

    private void SetSeparatorImage(GameObject parActionSeparator, string parPath)
    {
        parActionSeparator.transform.GetChild(0).transform.GetChild(0).GetComponent<Image>().overrideSprite = Resources.Load<Sprite>(parPath);
    }

    private void AddActionSeparator(bool parWithClock)
    {
        GameObject locActionSeparator = null;

        if (_match._actions.Count != 0)
        {
            locActionSeparator = (GameObject)Instantiate(_actionSeparatorPrefab);
            locActionSeparator.transform.SetParent(_actionsList.transform);
            locActionSeparator.transform.localScale = new Vector3(1, 1, 1);
            locActionSeparator.transform.SetAsFirstSibling();

            if (parWithClock)
            {
                locActionSeparator.transform.GetChild(0).transform.GetChild(0).gameObject.SetActive(true);
            } else
            {
                locActionSeparator.transform.GetChild(0).transform.GetChild(1).gameObject.SetActive(true);
            }
        }
    }

    private void SetupBonusSelector()
    {
        _bonusSelector.SetActive(true);
        var locBonusSelector = _bonusSelector.GetComponent<BonusSelector>();
        
        locBonusSelector.Init(_match._availableBonus, _match._usedBonus);

        if (_match._firstHalfIsPlayed)
        {
            locBonusSelector.DisableButton();
        }
        else
        {
            locBonusSelector.EnableButton();

            var locAddBtn = _bonusSelector.transform.GetChild(0);
            var locRemoveBtn = _bonusSelector.transform.GetChild(1);

            locAddBtn.GetComponent<Button>().onClick.AddListener(() => AddBonus());
            locRemoveBtn.GetComponent<Button>().onClick.AddListener(() => RemoveBonus());
        }
        
    }

    private void AddBonus()
    {
        Player locPlayer = GameManager._instance.Game.Player;

        if (locPlayer._gamePoints > 0)
        {
            _bonusSelector.GetComponent<BonusSelector>().OnAddBonus();
            _match._availableBonus += 1;
            locPlayer._gamePoints -= 1;
        }

        SetupSubHeader();
    }

    private void RemoveBonus()
    {
        Player locPlayer = GameManager._instance.Game.Player;

        if (_match._availableBonus > 0)
        {
            _bonusSelector.GetComponent<BonusSelector>().OnRemoveBonus();
            _match._availableBonus -= 1;
            locPlayer._gamePoints += 1;
        }

        SetupSubHeader();
    }

    private void SetupActionList()
    {
        int i;
        Action locAction;

        for (i = 0; i < _match._actions.Count; i++ )
        {
            locAction = _match._actions[i];

            var locLocalTeam = ChampionshipManager.getInstance().GetTeamById(_match._localTeam);
            var locVisitorTeam = ChampionshipManager.getInstance().GetTeamById(_match._visitorTeam);

            GameObject locMatchItem = null;

            if (locAction.Side == ActionSide.LOCAL)
            {
                locMatchItem = (GameObject)Instantiate(_leftActionPrefab);
                locMatchItem.GetComponent<ActionItem>().Setup(locAction, locLocalTeam);
            }
            else
            {
                locMatchItem = (GameObject)Instantiate(_rightActionPrefab);
                locMatchItem.GetComponent<ActionItem>().Setup(locAction, locVisitorTeam);
            }

            locMatchItem.transform.SetParent(_actionsList.transform);
            locMatchItem.transform.localScale = new Vector3(1, 1, 1);
            locMatchItem.transform.SetAsFirstSibling();

            var locBonus = locMatchItem.transform.GetChild(1);

            switch (locAction.Bonus)
            {
                case ActionBonus.NO:
                    locBonus.gameObject.SetActive(false);
                    break;
                case ActionBonus.YES:
                    locBonus.gameObject.SetActive(false);
                    break;
            }

            locMatchItem.GetComponent<ActionItem>().UpdateAction(locAction);

            if (i < _match._actions.Count - 1)
            {
                AddActionSeparator(false);
            }
        }
    }
}
