﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using System;

public class CalendarScreenManager : ScreenManager {

	public GameObject _calendarTabPrefab;
	public GameObject _matchItemPrefab;
	public GameObject _carousel;
	public GameObject _calendar;
	
	public override void OnShow() {

        SetupMainHeader();
        SetupSubHeader ();
		SetupFooter ();
		SetupCarousel ();
	}
	
	public override void OnHide () {
		_calendar.SetActive (false);

		_footer.transform.GetChild (0).GetComponent<Button> ().onClick.RemoveAllListeners ();
	}
	
	public override void OnNext () {

	}

	public override void OnBack() {
		SceneManager._instance.PreviousScreen ();
	}

    protected override void SetupMainHeader() {
        SetupMainHeader(true, false, false, "Calendar");
    }

    protected override void SetupSubHeader() {
		_subHeader.SetActive (true);
		
		// Disable left and right text and image
		_subHeader.transform.GetChild (0).gameObject.SetActive (false);
		_subHeader.transform.GetChild (1).gameObject.SetActive (false);
		_subHeader.transform.GetChild (3).gameObject.SetActive (false);
		_subHeader.transform.GetChild (4).gameObject.SetActive (false);
		
		// Enable subheader title and set the text
		Transform locSubHeaderTxt = _subHeader.transform.GetChild (2);
		locSubHeaderTxt.gameObject.SetActive (true);
		locSubHeaderTxt.GetComponent<Text> ().text = "Day 1";
	}
	
	protected override void SetupFooter() {
		_footer.SetActive (true);

        // Enable back button
        _footer.transform.GetChild (0).gameObject.SetActive (true);
        _footer.transform.GetChild(0).GetComponent<Button>().onClick.RemoveAllListeners();
        _footer.transform.GetChild (0).GetComponent<Button> ().onClick.AddListener (() => OnBack ());

        // Disable validate button.
        _footer.transform.GetChild (1).gameObject.SetActive (false);
	}

	public void SwipeUpdate(GameObject parNewObject, int parPosition) {

		SetupJourneyList (parPosition, parNewObject);

	}

	private void SetupCarousel() {
		_calendar.SetActive (true);
		
		
		// Cleanup the list
		foreach (Transform locTmpChild in _carousel.transform) {
			GameObject.Destroy(locTmpChild.gameObject);
		}

		int locNbJourney = ChampionshipManager.getInstance ()._championship._tabJourneysFirstLeague.Count;
		int locCurrentJourney = GameManager._instance.Game.JourneyIndex;

		Swipe locSwipe = _carousel.GetComponent<Swipe>();
		locSwipe._swipeHandler = SwipeUpdate;
		locSwipe._itemCpt = locNbJourney;
		locSwipe._currentPosition = locCurrentJourney;
		locSwipe._itemPrefab = _calendarTabPrefab;
        locSwipe._swipeEnable = true;

        GameObject locNewCalendarTab = locSwipe.CreateObject (Side.Center, locCurrentJourney);

		SetupJourneyList(locCurrentJourney, locNewCalendarTab);
    }

	private void SetupJourneyList(int parJourneyIndex, GameObject parCalendarTab) {

		// Get the calendar
		Journey locJourney = ChampionshipManager.getInstance ()._championship._tabJourneysFirstLeague [parJourneyIndex];
		
		
		// Fill the list with the team in championship
		foreach (Match locTmpMatch in locJourney._tabMatchs) {
			GameObject locMatchItem = (GameObject) Instantiate(_matchItemPrefab);
			locMatchItem.transform.SetParent(parCalendarTab.transform.GetChild(0));
			locMatchItem.transform.localScale = new Vector3(1, 1, 1);
			
			CalendarMatchItem locCalendarItem = locMatchItem.GetComponent<CalendarMatchItem>();
			locCalendarItem.SetMatchInfos(locTmpMatch);
		}

        Transform locSubHeaderTxt = _subHeader.transform.GetChild(2);
        locSubHeaderTxt.GetComponent<Text>().text = "Day " + (parJourneyIndex + 1);
    }
}
