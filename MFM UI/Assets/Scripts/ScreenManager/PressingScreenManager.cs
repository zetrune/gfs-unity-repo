﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class PressingScreenManager : AbstractTacticsScreenManager
{

    void Awake()
    {
    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    protected override void SetupMainHeader()
    {
        SetupMainHeader(true, false, false, "Pressing");
    }

    protected override void SetIndexPosition(int parPosition)
    {
        Team locTeam = ChampionshipManager.getInstance().GetTeamById(GameManager._instance.Game.Player._team);
        TeamSkillsModule locTeamSkillModule = locTeam._teamSkills;
        locTeamSkillModule._teamPressingIndex = parPosition;
    }

    protected override TeamSkill GetTeamSkill()
    {
        Team locTeam = ChampionshipManager.getInstance().GetTeamById(GameManager._instance.Game.Player._team);
        TeamSkillsModule locTeamSkillModule = locTeam._teamSkills;
        return locTeamSkillModule._tabTeamPressing[GetSkillIndex()];
    }

    protected override int GetCptSkills()
    {
        Team locTeam = ChampionshipManager.getInstance().GetTeamById(GameManager._instance.Game.Player._team);
        TeamSkillsModule locTeamSkillModule = locTeam._teamSkills;
        return locTeamSkillModule._tabTeamPressing.Count;
    }

    protected override int GetSkillIndex()
    {
        Team locTeam = ChampionshipManager.getInstance().GetTeamById(GameManager._instance.Game.Player._team);
        TeamSkillsModule locTeamSkillModule = locTeam._teamSkills;
        return locTeamSkillModule._teamPressingIndex;
    }
}
