﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using System.IO;

public class TeamSelectionScreenManager : ScreenManager {

	public GameObject _listScrollView;
	public GameObject _teamItemPrefab;

	private Team _selectedTeam;

	// Use this for initialization
	void Start () {

	}
	
	public override void OnShow() {
		SetupMainHeader ();
		SetupSubHeader ();
		SetupFooter ();
		SetupListScrollView ();
		
	}
	
	public override void OnHide () {
		
	}
	
	public override void OnNext () {
		if (_selectedTeam != null) {
			PlayerManager.getInstance ().setTeam (GameManager._instance.Game.Player, _selectedTeam);
            ChampionshipManager.getInstance().initJourney(GameManager._instance.Game.JourneyIndex);

            GameManager._instance.SaveNewGame();
            // Load the dashboard      
            Application.LoadLevel("DashboardScene");
		}
	}
	
	public override void OnBack() {
		
	}

	public void OnTeamItemClick(int parIndex) {

		// Enable validate button
		_footer.transform.GetChild (1).gameObject.SetActive (true);

		// Store the selected team
		_selectedTeam = ChampionshipManager.getInstance()._championship._tabFirstLeagueTeams[parIndex];
	}
	
	protected override void SetupMainHeader() {
        SetupMainHeader(true, false, false, "Team Selection");
	}
	
	protected override void SetupSubHeader() {
		_subHeader.SetActive (true);
		
		// Disable left and right text and image
		_subHeader.transform.GetChild (0).gameObject.SetActive (false);
		_subHeader.transform.GetChild (1).gameObject.SetActive (false);
		_subHeader.transform.GetChild (3).gameObject.SetActive (false);
		_subHeader.transform.GetChild (4).gameObject.SetActive (false);
		
		// Enable subheader title and set the text
		Transform locSubHeaderTxt = _subHeader.transform.GetChild (2);
		locSubHeaderTxt.gameObject.SetActive (true);
		locSubHeaderTxt.GetComponent<Text> ().text = "Select a team";
	}
	
	protected override void SetupFooter() {
		_footer.SetActive (true);
		
		// Disable both buttons.
		_footer.transform.GetChild (0).gameObject.SetActive (false);
		_footer.transform.GetChild (1).gameObject.SetActive (false);
	}
	
	private void SetupListScrollView() {
		_listScrollView.SetActive (true);


		// Cleanup the list
		Transform locList = _listScrollView.transform.GetChild (0);

		foreach (Transform locTmpChild in locList) {
			GameObject.Destroy(locTmpChild.gameObject);
		}

		// Get the championship
		List<Team> locListTeam = ChampionshipManager.getInstance()._championship._tabFirstLeagueTeams;
		int locCurrentIndex = 0;


		// Fill the list with the team in championship
		foreach (Team locTmpTeam in locListTeam) {
			GameObject locTmpTeamItem = (GameObject) Instantiate(_teamItemPrefab);
			locTmpTeamItem.transform.SetParent(_listScrollView.transform.GetChild(0));
			locTmpTeamItem.transform.localScale = new Vector3(1, 1, 1);

			Transform locTeamInfo = locTmpTeamItem.transform.GetChild(0);
			Transform locBlaze = locTeamInfo.transform.GetChild(0);

			//Set Primary and secondary color for the team
			locBlaze.GetComponent<BlazeColor>().SetTeam(locTmpTeam);

			// Fill team info panel
			setupTeamItem(locTeamInfo, locTmpTeam);

			// Set the button callback to call OnTeamItemClick with the right team
			Button locButton = locTmpTeamItem.GetComponent<Button>();
			int locTeamIndex = locCurrentIndex;
			locButton.onClick.AddListener(() => OnTeamItemClick(locTeamIndex));

			locCurrentIndex++;
		}
	}

	private void setupTeamItem(Transform parTeamInfo, Team parTeam) {
		// Set the text to the team name
		parTeamInfo.GetChild(1).GetComponent<Text>().text = parTeam._teamName;

		Transform locTeamTag = parTeamInfo.GetChild (2);

		//Set the color of image depending of team type
		locTeamTag.GetComponent<TagColor> ().SetTeam(parTeam);
	}
}
