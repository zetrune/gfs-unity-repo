﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class RankingScreenManager : ScreenManager {

	public GameObject _rankingView;

	public GameObject _rankItemPrefab;
	public GameObject _rankingTabPrefab;

	public NavIndicator _navIndicator;


	enum RankingType {
		Global,
		Local,
		Visitor
	};
	
	void Awake() {
	}
	
	// Use this for initialization
	void Start () {
		// Set the navInficator to 3 for each ranking type.
		_navIndicator.SetNbItem (3);
	}
	
	public override void OnShow() {
		
		SetupMainHeader ();
		SetupSubHeader ();
		SetupFooter ();
		SetupRankingView ();
	}
	
	public override void OnHide () {
		_rankingView.SetActive (false);
		_footer.transform.GetChild (0).GetComponent<Button> ().onClick.RemoveAllListeners ();
	}
	
	public override void OnNext () {
		
	}
	
	public override void OnBack() {
		SceneManager._instance.PreviousScreen ();
	}
	
	
	protected override void SetupMainHeader() {
        SetupMainHeader(true, false, false, "Ranking");
    }
	
	protected override void SetupSubHeader() {
		_subHeader.SetActive (true);
		
		// Disable left and right text and image
		_subHeader.transform.GetChild (0).gameObject.SetActive (false);
		_subHeader.transform.GetChild (1).gameObject.SetActive (false);
		_subHeader.transform.GetChild (3).gameObject.SetActive (false);
		_subHeader.transform.GetChild (4).gameObject.SetActive (false);
		
		// Enable subheader title and set the text
		Transform locSubHeaderTxt = _subHeader.transform.GetChild (2);
		locSubHeaderTxt.gameObject.SetActive (true);
		locSubHeaderTxt.GetComponent<Text> ().text = "";
	}
	
	protected override void SetupFooter() {
		_footer.SetActive (true);

        // Enable back button
        _footer.transform.GetChild (0).gameObject.SetActive (true);
        _footer.transform.GetChild(0).GetComponent<Button>().onClick.RemoveAllListeners();
        _footer.transform.GetChild (0).GetComponent<Button> ().onClick.AddListener (() => OnBack ());

        // Disable validate button
        _footer.transform.GetChild (1).gameObject.SetActive (false);
	}

	public void SwipeUpdate(GameObject parNewObject, int position) {
		_navIndicator.SetPosition (position);

		List<Team> locTeams = ChampionshipManager.getInstance()._championship._tabFirstLeagueTeams;

        var subHeaderTxt =  _subHeader.transform.GetChild(2);

        switch ((RankingType) position) {
		case RankingType.Global:
                ChampionshipManager.getInstance()._championship.sortGlobalRanking(locTeams);
                subHeaderTxt.GetComponent<Text>().text = "Global";
                break;
		case RankingType.Local:
			    ChampionshipManager.getInstance()._championship.sortLocalRanking(locTeams);
                subHeaderTxt.GetComponent<Text>().text = "Local";
                break;
		case RankingType.Visitor:
			    ChampionshipManager.getInstance()._championship.sortVisitorRanking(locTeams);
                subHeaderTxt.GetComponent<Text>().text = "Visitor";
			    break;
		default:
			    break;
		}

		SetupListScrollView(parNewObject.transform.GetChild(1), locTeams, (RankingType) position);
	}
	
	private void SetupRankingView() {
		_rankingView.SetActive(true);
		
		Transform locCarousel = _rankingView.transform.GetChild(0);

		foreach (Transform locTmpChild in locCarousel) {
			GameObject.Destroy(locTmpChild.gameObject);
		}

		Swipe locSwipe = locCarousel.GetComponent<Swipe>();
		locSwipe._swipeHandler = SwipeUpdate;
		locSwipe._itemCpt = 3;
		locSwipe._currentPosition = 0;
		locSwipe._itemPrefab = _rankingTabPrefab;
        locSwipe._swipeEnable = true;

        List<Team> locTeams = ChampionshipManager.getInstance()._championship._tabFirstLeagueTeams;

		GameObject locObject = locSwipe.CreateObject (Side.Center, 0);
		ChampionshipManager.getInstance()._championship.sortGlobalRanking(locTeams);
		SetupListScrollView(locObject.transform.GetChild(1), locTeams, RankingType.Global);

        var subHeaderTxt = _subHeader.transform.GetChild(2);
        subHeaderTxt.GetComponent<Text>().text = "Global";

        _navIndicator.SetPosition(locSwipe._currentPosition);
    }
	
	private void SetupListScrollView(Transform parScrollList, List<Team> parTeams, RankingType parRankType) {
		
		// Fill the list with the team in championship
		foreach (Team locTmpTeam in parTeams) {
			GameObject locMatchItem = (GameObject) Instantiate(_rankItemPrefab);
			locMatchItem.transform.SetParent(parScrollList.GetChild(0));
			locMatchItem.transform.localScale = new Vector3(1, 1, 1);
			
			RankItem locCalendarItem = locMatchItem.GetComponent<RankItem>();
			
			switch (parRankType) {
				case RankingType.Global:
					locCalendarItem.setRankItem(locTmpTeam, locTmpTeam._teamGlobalStats);
					break;
				case RankingType.Local:
					locCalendarItem.setRankItem(locTmpTeam, locTmpTeam._teamLocalStats);
					break;
				case RankingType.Visitor:
					locCalendarItem.setRankItem(locTmpTeam, locTmpTeam._teamVisitorStats);
					break;
				
			}
			
		}
	}
}
