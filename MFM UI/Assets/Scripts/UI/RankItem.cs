﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class RankItem : MonoBehaviour {

	public Transform _rank;
	public Transform _blaze;
	public Transform _teamName;
	public Transform _points;
	public Transform _played;
	public Transform _win;
	public Transform _draw;
	public Transform _lose;
	public Transform _goalFor;
	public Transform _goalAgainst;
	public Transform _goalAverage;

	public void setRankItem(Team parTeam, TeamStatsModule parTeamStatsModule) {
		// Set Team rank
		_rank.GetComponent<Text> ().text = parTeamStatsModule._currentRank.ToString();
		// Set Primary and secondary color for the local team
		_blaze.GetComponent<BlazeColor>().SetTeam(parTeam);
		// Set Team name
		_teamName.GetComponent<Text> ().text = parTeam._teamName;
		// Set points
		_points.GetComponent<Text> ().text = parTeamStatsModule._nbPoints.ToString();
		// Set played
		_played.GetComponent<Text> ().text = parTeamStatsModule._nbMatchs.ToString();
		// Set win
		_win.GetComponent<Text> ().text = parTeamStatsModule._nbVictory.ToString();
		// Set draw
		_draw.GetComponent<Text> ().text = parTeamStatsModule._nbDraw.ToString();
		// Set lose
		_lose.GetComponent<Text> ().text = parTeamStatsModule._nbLose.ToString();
		// Set goal for
		_goalFor.GetComponent<Text> ().text = parTeamStatsModule._nbGoalScored.ToString();
		// Set goal against
		_goalAgainst.GetComponent<Text> ().text = parTeamStatsModule._nbGoalConceded.ToString();
		// Set goal average
		_goalAverage.GetComponent<Text> ().text = parTeamStatsModule._goalAverage.ToString();
	}
}
