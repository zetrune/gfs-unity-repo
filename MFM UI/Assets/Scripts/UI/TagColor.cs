﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TagColor : MonoBehaviour {

	public Transform _teamStatus;
	public Transform _teamType;

	public void SetTeam(Team parTeam) {
		setTeamType (parTeam);
	}
	
	private Sprite setTeamStatus(Team parTeam, Color32 parFullColor, Color32 parAlphaColor) {
		switch (parTeam._teamStatus._status) {
		case TeamStatus.FAV:
			_teamStatus.GetChild(0).GetComponent<Image>().color = parFullColor;
			_teamStatus.GetChild(1).GetComponent<Image>().color = parFullColor;
			_teamStatus.GetChild(2).GetComponent<Image>().color = parFullColor;
			_teamStatus.GetChild(3).GetComponent<Image>().color = parFullColor;
			break;
		case TeamStatus.OUT:
			_teamStatus.GetChild(0).GetComponent<Image>().color = parFullColor;
			_teamStatus.GetChild(1).GetComponent<Image>().color = parFullColor;
			_teamStatus.GetChild(2).GetComponent<Image>().color = parFullColor;
			_teamStatus.GetChild(3).GetComponent<Image>().color = parAlphaColor;
			break;
		case TeamStatus.MID:
			_teamStatus.GetChild(0).GetComponent<Image>().color = parFullColor;
			_teamStatus.GetChild(1).GetComponent<Image>().color = parFullColor;
			_teamStatus.GetChild(2).GetComponent<Image>().color = parAlphaColor;
			_teamStatus.GetChild(3).GetComponent<Image>().color = parAlphaColor;
			break;
		case TeamStatus.LOW:
			_teamStatus.GetChild(0).GetComponent<Image>().color = parFullColor;
			_teamStatus.GetChild(1).GetComponent<Image>().color = parAlphaColor;
			_teamStatus.GetChild(2).GetComponent<Image>().color = parAlphaColor;
			_teamStatus.GetChild(3).GetComponent<Image>().color = parAlphaColor;
			break;
		}
		return null;
	}
	
	private void setTeamType(Team parTeam) {
		Color32 locFullColor = new Color();
		Color32 locAlphaColor = new Color();
		
		switch (parTeam._teamSkills._teamType) {
		case TeamType.OFF:
			// Color Red base
			locFullColor = new Color32(255, 80, 80, 255);
			locAlphaColor = new Color32(255, 80, 80, 80);
			_teamType.GetChild (0).GetComponent<Image> ().color = locFullColor;
			_teamType.GetChild (1).GetComponent<Image> ().color = locAlphaColor;
			_teamType.GetChild (2).GetComponent<Image> ().color = locAlphaColor;
			break;
		case TeamType.MID:
			// Color Orange base
			locFullColor = new Color32(250, 150, 70, 255);
			locAlphaColor = new Color32(250, 150, 70, 80);
			_teamType.GetChild (0).GetComponent<Image> ().color = locAlphaColor;
			_teamType.GetChild (1).GetComponent<Image> ().color = locFullColor;
			_teamType.GetChild (2).GetComponent<Image> ().color = locAlphaColor;
			break;
		case TeamType.DEF:
			// Color Blue base
			locFullColor = new Color32(80, 130, 255, 255);
			locAlphaColor = new Color32(80, 130, 255, 80);
			_teamType.GetChild (0).GetComponent<Image> ().color = locAlphaColor;
			_teamType.GetChild (1).GetComponent<Image> ().color = locAlphaColor;
			_teamType.GetChild (2).GetComponent<Image> ().color = locFullColor;
			break;
		}
		
		setTeamStatus (parTeam, locFullColor, locAlphaColor);
	}

}
