﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DashboardButtons : MonoBehaviour {

	public Transform _calendarButton;
	public Transform _rankingButton;
	public Transform _boardButton;


	public void UpdateDashboardButtons() {
		UpdateCalendar ();
		UpdateRanking ();
		UpdateBoard ();
	}

	private void UpdateCalendar() {
		int locSeason = GameManager._instance.Game.Season;
		int locJourneyIndex = GameManager._instance.Game.JourneyIndex + 1;
		int locJourneyNb = ChampionshipManager.getInstance()._championship._tabJourneysFirstLeague.Count;
		string locSeasonLabel = locSeason.ToString () + "-" + (locSeason + 1).ToString () + " ("
							+ locJourneyIndex.ToString() + "/" + locJourneyNb.ToString() + ")";

		_calendarButton.GetChild (1).GetChild (0).GetComponent<Text> ().text = locSeasonLabel;
	}

	private void UpdateRanking() {
        var locPlayerTeam = ChampionshipManager.getInstance().GetTeamById(GameManager._instance.Game.Player._team);

		int locRank = locPlayerTeam._teamGlobalStats._currentRank;

		_rankingButton.GetChild (1).GetChild (0).GetComponent<Text> ().text = locRank.ToString();
	}

	private void UpdateBoard() {
        Color32 locBoardTrustColor = ConstManager.WHITE_COLOR;

		switch (GameManager._instance.Game.Player._boardTrust) {
		case 0:
            locBoardTrustColor = ConstManager.RED_LIGHT_COLOR;
			_boardButton.GetChild (1).GetComponent<Image> ().color = locBoardTrustColor;
			break;
		case 1:
            locBoardTrustColor = ConstManager.ORANGE_LIGHT_COLOR;
			_boardButton.GetChild (1).GetComponent<Image> ().color = locBoardTrustColor;
			break;
		case 2:
            locBoardTrustColor = ConstManager.GREEN_BUTTON_COLOR;
			_boardButton.GetChild (1).GetComponent<Image> ().color = locBoardTrustColor;
			break;
		}
	}
}
