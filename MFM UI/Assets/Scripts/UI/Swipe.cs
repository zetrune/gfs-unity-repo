﻿using UnityEngine;
using UnityEngine.UI;


public class Swipe : MonoBehaviour {

	public GameObject _itemPrefab;
	public int _padding = 10;
	public int _currentPosition = 2; // Current item position in carrousel
	public int _itemCpt = 5; // Nb element in carrousel
	public float _minSwipeDist = 100;
    public bool _swipeEnable = true;

	private float _swipeDist;
	private Vector3 _startPos;

	public SwipeListener _swipeHandler;

	public delegate void SwipeListener(GameObject parNewObject, int parPosition);

	// Use this for initialization
	void Start () {
		RectTransform objectRectTransform = gameObject.GetComponentInParent<RectTransform> ();
		_swipeDist = objectRectTransform.rect.width;
	}
	
	// Update is called once per frame
	void Update()
	{
        if (_swipeEnable)
        {
            CheckSwipe();
        }
	}
	
	private void CheckSwipe() {
	

		if (Input.GetKeyDown(KeyCode.Q)) {
			SwipeLeft();
		} else if (Input.GetKeyDown(KeyCode.D)) {
			SwipeRight();
		}

		//#if UNITY_ANDROID

		if (Input.touchCount > 0) {
			Touch touch = Input.touches[0];
			
			switch (touch.phase) {

			case TouchPhase.Began:
				_startPos = touch.position;
				break;

			case TouchPhase.Ended:
				float swipeDistVertical = (new Vector3(0, touch.position.y, 0) - new Vector3(0, _startPos.y, 0)).magnitude;
				
				if (swipeDistVertical > _minSwipeDist) {
					float swipeValue = Mathf.Sign(touch.position.y - _startPos.y);
					if (swipeValue > 0) {// Up swipe
						// Action to be performed if up swipe
					}else if (swipeValue < 0) {// Down swipe
						// Action to be performed if down swipe;
					}
				}

				float swipeDistHorizontal = (new Vector3(touch.position.x,0, 0) - new Vector3(_startPos.x, 0, 0)).magnitude;

				if (swipeDistHorizontal > _minSwipeDist) {
					float swipeValue = Mathf.Sign(touch.position.x - _startPos.x);
					if (swipeValue > 0) { // Right swipe
						SwipeRight();
					} else if (swipeValue < 0) { // Left swipe
						SwipeLeft();
					}
				}
				break;
			}
		}
	}
	
	private void SwipeLeft() {
		if (_currentPosition < _itemCpt - 1) {
            _swipeEnable = false;
            _currentPosition++;

			GameObject tmpObject = CreateObject (Side.Right, _currentPosition);
			_swipeHandler(tmpObject, _currentPosition);

			for (int i = 0; i < transform.childCount; i++) {
				Transform locTmpChild = transform.GetChild(i);
				MoveTab locMoveTab = locTmpChild.GetComponent<MoveTab>();
                locMoveTab.OnMoveDone += EnableSwipe;

                if (locMoveTab) {
					locMoveTab.ComputeTarget(-_swipeDist, i < (transform.childCount - 1));
				}
			}
		}
	} 
	
	private void SwipeRight() {
		if (_currentPosition >= 1) {
            _swipeEnable = false;
            _currentPosition--;
			GameObject tmpObject = CreateObject (Side.Left, _currentPosition);
			_swipeHandler(tmpObject, _currentPosition);

			for (int i = 0; i < transform.childCount; i++) {
				Transform locTmpChild = transform.GetChild(i);
				MoveTab locMoveTab = locTmpChild.GetComponent<MoveTab>();
                locMoveTab.OnMoveDone += EnableSwipe;

                if (locMoveTab) {
					locMoveTab.ComputeTarget(_swipeDist, i < (transform.childCount - 1));
				}
			}

		}
        
    }

	public GameObject CreateObject(Side parSide, int parItemIndex) {
		GameObject locObject = (GameObject) Instantiate(_itemPrefab);
		
		locObject.transform.SetParent(transform);
		locObject.transform.localScale = new Vector3(1, 1, 1);
		
		RectTransform rt = (RectTransform)locObject.transform;
		RectTransform rtCarousel = (RectTransform)transform;
		
		rt.SetInsetAndSizeFromParentEdge (RectTransform.Edge.Bottom, _padding, rtCarousel.rect.height - (_padding * 2));
		rt.SetInsetAndSizeFromParentEdge (RectTransform.Edge.Left, _padding, rtCarousel.rect.width - (_padding * 2));
		
		
		
		locObject.transform.position = new Vector3 (locObject.transform.position.x + rtCarousel.rect.width * (int) parSide,
		                                            locObject.transform.position.y,
		                                            locObject.transform.position.z);
		
		MoveTab locMoveTab = locObject.GetComponent<MoveTab>();
		
		if (locMoveTab) {
			locMoveTab.InitTarget();
		}

		return locObject;
	}

    public void EnableSwipe()
    {
        _swipeEnable = true;
    }
}
