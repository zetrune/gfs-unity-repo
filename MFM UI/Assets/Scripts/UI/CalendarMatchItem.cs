﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CalendarMatchItem : MonoBehaviour {

	public Transform _localBlaze;
	public Transform _localTeam;
	public Transform _scoreTxt;
	public Transform _visitorTeam;
	public Transform _visitorBlaze;
	
	public void SetMatchInfos(Match parMatch) {
        var locLocalTeam = ChampionshipManager.getInstance().GetTeamById(parMatch._localTeam);
        var locVisitorTeam = ChampionshipManager.getInstance().GetTeamById(parMatch._visitorTeam);

        // Set Primary and secondary color for the local team
        _localBlaze.GetComponent<BlazeColor>().SetTeam(locLocalTeam);
		// Set Local Team name
		_localTeam.GetComponent<Text> ().text = locLocalTeam._teamName;
		// Set score
		_scoreTxt.GetComponent<Text> ().text = parMatch._localScore + " - " + parMatch._visitorScore; 
		// Set Visitor Team name
		_visitorTeam.GetComponent<Text> ().text = locVisitorTeam._teamName;
		// Set Primary and secondary color for the visitor team
		_visitorBlaze.GetComponent<BlazeColor>().SetTeam(locVisitorTeam);
	}
}
