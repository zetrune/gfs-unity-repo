﻿using UnityEngine;
using System.Collections;

public class MoveTab : MonoBehaviour {

	private Vector3 _target;
	private bool _isMoving = false;
	private bool _toBeDestroy = false;
	private float _epsilon = 1.0f;

    public MoveDoneListener OnMoveDone { get; set; }

    public delegate void MoveDoneListener();

    // Use this for initialization
    void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		MoveItem ();
	}

	private void MoveItem() {
		if (_isMoving) {
			if (Mathf.Abs (transform.position.x - _target.x) < _epsilon) {
				transform.position = _target;
				_isMoving = false;
				if (_toBeDestroy) {
					GameObject.Destroy(gameObject);
				}
                OnMoveDone();
            } else {
				transform.position = Vector3.Lerp (transform.position, _target, Time.deltaTime * ConstManager.SWIPE_SPEED);
			}
		}
	}

	public void InitTarget() {
		_target = transform.position;
	}

    public void ComputeTarget(float parSwipeDist, bool parToBeDestroy)
    {
        _target = new Vector3(_target.x + parSwipeDist, _target.y, _target.z);
        _isMoving = true;
        _toBeDestroy = parToBeDestroy;
    }
}
