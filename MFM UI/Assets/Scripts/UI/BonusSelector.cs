﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BonusSelector : MonoBehaviour
{

    public Color _consumed;
    public Color _available;
    public Color _notAvailable;

    public Transform _listBonus;

    private int _index = 0;

    public void Init(int parAvailable, int parUsed)
    {
        _index = parAvailable - parUsed;

        for (int i = 0; i < _listBonus.childCount; i++)
        {
            var locImg = _listBonus.GetChild(i).GetComponent<Image>();

            if (i < _index)
            {
                locImg.color = _available;
            }
            else if (i < parAvailable)
            {
                locImg.color = _consumed;
            }
            else
            {
                locImg.color = _notAvailable;
            }
        }
    }

    public void EnableButton()
    {
        transform.GetChild(0).gameObject.SetActive(true);
        transform.GetChild(1).gameObject.SetActive(true);
    }

    public void DisableButton()
    {
        transform.GetChild(0).gameObject.SetActive(false);
        transform.GetChild(1).gameObject.SetActive(false);
    }

    public void OnAddBonus()
    {
        if (_index < 8)
        {
            var locBonus = _listBonus.GetChild(_index);
            var locImg = locBonus.GetComponent<Image>();
            locImg.color = _available;

            _index++;
        }
    }

    public void OnRemoveBonus()
    {
        if (_index > 0)
        {
            _index--;

            var locBonus = _listBonus.GetChild(_index);
            var locImg = locBonus.GetComponent<Image>();
            locImg.color = _notAvailable;
        }
    }

    public void OnUseBonus()
    {
        if (_index > 0)
        {
            _index--;

            var locBonus = _listBonus.GetChild(_index);
            var locImg = locBonus.GetComponent<Image>();
            locImg.color = _consumed;
        }
    }
}
