﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BlazeColor : MonoBehaviour {

	public Image _topBlaze;
	public Image _bottomBlaze;
	
	public void SetTeam(Team parTeam) {
		//Set Primary and secondary color for the team
		_topBlaze.color = parTeam._teamPrimaryColor;
		_bottomBlaze.color = parTeam._teamSecondaryColor;
	}

}
