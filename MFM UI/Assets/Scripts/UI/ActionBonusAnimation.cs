﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

class ActionBonusAnimation : MonoBehaviour
{
    public Animator _animator = null;

    public void EnableAnimation()
    {
        _animator.speed = ConstManager.MATCH_ACTION_DELAY;
        _animator.Play("BonusAnimation");
    }
}

