﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MatchOverview : MonoBehaviour {

	public Transform _localPanel;
	public Transform _scorePanel;
	public Transform _visitorPanel;

	public void SetMatchOverview(Match parMatch) {
        var locLocalTeam = ChampionshipManager.getInstance().GetTeamById(parMatch._localTeam);
        var locVisitorTeam = ChampionshipManager.getInstance().GetTeamById(parMatch._visitorTeam);

        SetupPanel (_localPanel, locLocalTeam);
		SetScorePanel (parMatch);
		SetupPanel (_visitorPanel, locVisitorTeam);
	}

	private void SetupPanel(Transform parPanel, Team parTeam) {
		// Retrieve team name
		Transform locTeamName = parPanel.GetChild (2);
		// Retrieve blaze component
		Transform locBlaze = parPanel.GetChild(3);
		// Retrieve Team Tag component
		Transform locTeamTag = parPanel.GetChild(4).GetChild(0);

		// Set Team name
		locTeamName.GetComponent<Text> ().text = parTeam._teamName;
		// Set Primary and secondary color for the team
		locBlaze.GetComponent<BlazeColor>().SetTeam(parTeam);
		// Set Team tag and color for the team
		locTeamTag.GetComponent<TagColor>().SetTeam(parTeam);
	}

	public void SetScorePanel(Match parMatch) {
		Transform locScoreTxt = _scorePanel.GetChild (0);
		locScoreTxt.GetComponent<Text> ().text = parMatch._localScore + " - " + parMatch._visitorScore; 
	}
}
