﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class StatsOverview : MonoBehaviour {

	public Transform _atkGauge;
	public Transform _defGauge;

	public void setGauge(Team parTeam) {
		float locAtk = parTeam._teamSkills._teamScoreAtk * 100f;
		float locDef = parTeam._teamSkills._teamScoreDef * 100f;
		updateGauge (_atkGauge, locAtk);
		updateGauge (_defGauge, locDef);
	}

	private void updateGauge(Transform parGauge, float parValue) {
        var locGaugeCtrl = parGauge.GetComponent<GaugeController>();
        locGaugeCtrl.InitGauge ();
        locGaugeCtrl.UpdateGauge (parValue);
	}
}
