﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GaugeController : MonoBehaviour {

	public Transform _label;
	public RectTransform _foreground;
	public bool _isLeftGauge;
	private float _currentValue;
	private float _minXValue;
	private float _width;

    private bool _isInit = false;
    private Vector3 _initPosition;

	// Use for initialization
	public void InitGauge() {

        if (!_isInit)
        {
            _isInit = true;
            _initPosition = new Vector3(_foreground.localPosition.x, _foreground.localPosition.y, _foreground.localPosition.z);
            _width = _foreground.rect.width;
        }

        _foreground.localPosition = _initPosition;
		_currentValue = 0f;
		if (_isLeftGauge) {
			_minXValue = _foreground.localPosition.x - _width;
		} else {
			_minXValue = _foreground.localPosition.x + _width;
		}
	}

	public void UpdateGauge(float parValue) {

		_currentValue = MapValues (parValue);
		_foreground.localPosition = new Vector3(_currentValue, _initPosition.y, _initPosition.z);
		
		if (_isLeftGauge) {
			_label.GetComponent<Text> ().text = "Attack: " +  Mathf.RoundToInt(parValue);
		} else {
			_label.GetComponent<Text> ().text = "Defense: " + Mathf.RoundToInt(parValue);
		}
	}

	private float MapValues(float parValue) {
		float locDelta = parValue / 100f * _width;
		if (_isLeftGauge) {

			return _minXValue + locDelta;

		} else {

			return (_minXValue - locDelta);
		}
	}
}
