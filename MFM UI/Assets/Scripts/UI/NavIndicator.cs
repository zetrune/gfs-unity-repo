﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class NavIndicator : MonoBehaviour {

	public Color32 _selectedColor;
	public Color32 _unselectedColor;
	public GameObject _indicatorItem;

	private int _nbItem;

	public void SetPosition(int parPosition){

        int i = 0;

		if ((parPosition >= 0) && (parPosition < _nbItem)) {
			// Unselect all item
			foreach (Transform locTmpChild in transform) {
				locTmpChild.GetComponent<Image>().color = _unselectedColor;
                i++;
			}
            // Set selected item to current position
            transform.GetChild(parPosition).GetComponent<Image>().color = _selectedColor;
 
			    
        }
	}

	public void SetNbItem(int parNbItem) {
		_nbItem = parNbItem;

		// Remove all current items
		foreach (Transform locTmpChild in transform) {    
			GameObject.Destroy(locTmpChild.gameObject);
		}

        transform.DetachChildren();

        // Create target nb items
        for (int i = 0; i < _nbItem; i++) {
			InstantiateIndicatorItem();
		}

		// Set current position to the first element
		SetPosition (0);
	}

	private void InstantiateIndicatorItem() {
		GameObject locIndicatorItem = (GameObject) Instantiate(_indicatorItem);
		locIndicatorItem.transform.SetParent(gameObject.transform);
		locIndicatorItem.transform.localScale = new Vector3(1, 1, 1);
	}
}
