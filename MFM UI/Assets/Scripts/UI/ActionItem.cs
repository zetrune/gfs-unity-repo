﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

class ActionItem : MonoBehaviour
{
    public GameObject _mainTxt = null;
    public GameObject _subTxt = null;
    public GameObject _time = null;
    public GameObject _icon = null;
    public GameObject _primaryColor = null;
    public GameObject _secondaryColor = null;


    public void Setup(Action parAction, Team parTeam)
    {
        if (GameManager._instance.Game.Player._team == parTeam._id)
        {
            _mainTxt.GetComponent<Text>().text = "Attack";
        }
        else
        {
            _mainTxt.GetComponent<Text>().text = "Defense";
        }

        SetSubText("In progress");
        _time.GetComponent<Text>().text = parAction.Time + "'";

        var locImg = _primaryColor.GetComponent<Image>();
        locImg.color = parTeam._teamPrimaryColor;

        locImg = _secondaryColor.GetComponent<Image>();
        locImg.color = parTeam._teamSecondaryColor;
    }

    public void UpdateAction(Action parAction)
    {
        if (parAction.Defense1 == ActionState.WIN)
        {
            SetSubText("Defensive take the ball");
            SetIcon("ActionIcon/mfm_icon_rescue_def");
        }
        else if (parAction.Defense1 == ActionState.NONE)
        {
            SetSubText("In progress");
        }
        else
        {
            if (parAction.Attack1 == ActionState.NONE)
            {
                SetSubText("Defensive missed interception");
            }
            else if (parAction.Attack1 == ActionState.LOSE)
            {
                SetSubText("Attacker lose the ball");
                SetIcon("ActionIcon/mfm_icon_rescue_def");
            }
            else
            {
                if (parAction.Attack2 == ActionState.NONE)
                {
                    SetSubText("Attacker dribble past defensive");
                }
                else if (parAction.Attack2 == ActionState.LOSE)
                {
                    SetSubText("Attacker shots off target");
                    SetIcon("ActionIcon/mfm_icon_opportunity");
                }
                else
                {
                    if (parAction.Defense2 == ActionState.NONE)
                    {
                        SetSubText("Attacker shots");
                    }
                    else if (parAction.Defense2 == ActionState.WIN)
                    {
                        SetSubText("Goal keeper catch the ball");
                        SetIcon("ActionIcon/mfm_icon_rescue");

                    }
                    else
                    {
                        SetSubText("Goal !");
                        SetIcon("ActionIcon/mfm_icon_football");
                    }
                }
            }
        }
    }

    private void SetSubText(string parText)
    {
        _subTxt.GetComponent<Text>().text = parText;
    }

    private void SetIcon(string parImgPath)
    {
        _icon.GetComponent<Image>().overrideSprite = Resources.Load<Sprite>(parImgPath);
    }
}
