﻿using UnityEngine;
using System.Collections;

public enum Grade {
	REGIONAL,
	NATIONAL,
	CONTINENTAL,
	INTERNATIONAL
}
