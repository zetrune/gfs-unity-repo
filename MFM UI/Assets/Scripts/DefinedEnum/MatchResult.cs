﻿using UnityEngine;
using System.Collections;

public enum MatchResult : byte {
	LOSE,
	DRAW,
	VICTORY
}
