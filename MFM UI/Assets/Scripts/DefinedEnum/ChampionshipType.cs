﻿using UnityEngine;
using System.Collections;

public enum ChampionshipType {
	NONE,
	ENGLISH,
	FRENCH,
	GERMAN,
	ITALIAN,
	SPANISH
}
