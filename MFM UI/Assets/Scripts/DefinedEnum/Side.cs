﻿using UnityEngine;
using System.Collections;

public enum Side {
	Left = -1,
	Center = 0,
	Right = 1
}
