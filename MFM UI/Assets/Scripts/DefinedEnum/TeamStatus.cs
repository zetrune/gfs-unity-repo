﻿using UnityEngine;
using System.Collections;

public enum TeamStatus : byte {
	LOW,
	MID,
	OUT,
	FAV
}
