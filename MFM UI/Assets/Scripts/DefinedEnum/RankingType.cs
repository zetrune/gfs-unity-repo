﻿using UnityEngine;
using System.Collections;

public enum RankingType {
	GLOBAL,
	LOCAL,
	VISITOR
}