﻿using UnityEngine;
using System.Collections;

public enum TeamType : byte {
	DEF,
	MID,
	OFF
}
