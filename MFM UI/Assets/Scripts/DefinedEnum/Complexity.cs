﻿using UnityEngine;
using System.Collections;

public enum Complexity {
	NORMAL,
	ADVANCED,
	EXPERT
}
