﻿using UnityEngine;
using System.Collections;

public enum Trend {
	DOWN,
	STABLE,
	UP
}
