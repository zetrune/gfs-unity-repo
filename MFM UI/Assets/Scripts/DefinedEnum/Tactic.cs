﻿using UnityEngine;
using System.Collections;

public enum Tactic : byte {
	DEF,
	NEUTRAL,
	OFF
}
